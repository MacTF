//
//  TFDataFormat.h
//  MacTF
//
//  Created by Kalle Olavi Niemitalo on 2007-10-23, based on:
//    TFUSBController.h
//    MacTF
//    Created by Nathan Oates on Sun Aug 01 2004.
//    Copyright (c) 2004-7 Nathan Oates. All rights reserved.
//  May also include parts of:
//    uproar 0.1
//    Copyright (c) 2001 Kasima Tharnpipitchai <me@kasima.org>
//
// This source code is free software; you can redistribute it and/or
// modify it under the terms of the GNU Public License as published
// by the Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This source code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// Please refer to the GNU Public License for more details.
//
// You should have received a copy of the GNU Public License along with
// this source code; if not, write to:
// Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

typedef enum TopfieldUSBCmd {
	USB_CmdNone						= 0x00000000,

	USB_Fail						= 0x00000001,
	USB_Success						= 0x00000002,
	USB_Cancel						= 0x00000003,

	USB_CmdReady					= 0x00000100,
	USB_CmdReset					= 0x00000101,
	USB_CmdTurbo					= 0x00000102,

	// Hdd Cmd
	USB_CmdHddSize					= 0x00001000,
	USB_DataHddSize					= 0x00001001,

	USB_CmdHddDir					= 0x00001002,
	USB_DataHddDir					= 0x00001003,
	USB_DataHddDirEnd				= 0x00001004,

	USB_CmdHddDel					= 0x00001005,
	USB_CmdHddRename				= 0x00001006,
	USB_CmdHddCreateDir				= 0x00001007,

	USB_CmdHddFileSend				= 0x00001008,
	USB_DataHddFileStart			= 0x00001009,
	USB_DataHddFileData				= 0x0000100A,
	USB_DataHddFileEnd				= 0x0000100B,

	// Ensure that random 32-bit values received from the PVR can
	// be converted to enum TopfieldUSBCmd without losing bits.
	TopfieldUSBCmd_pad              = 0xFFFFFFFF
} TopfieldUSBCmd;

// -----------------------------------------------------------------------------
// Usb Error Code
typedef enum TopfieldUSBEcode {
	USB_OK							= 0x00000000,
	USB_Err_Crc						= 0x00000001,
	USB_Err_UnknownCmd				= 0x00000002,
	USB_Err_InvalidCmd				= 0x00000003,
	USB_Err_Unknown					= 0x00000004,
	USB_Err_Size					= 0x00000005,
	USB_Err_RunFail					= 0x00000006,
	USB_Err_Memory					= 0x00000007,

	// Ensure that random 32-bit values received from the PVR can
	// be converted to enum TopfieldUSBCmd without losing bits.
	TopfieldUSBEcode_pad            = 0xFFFFFFFF
} TopfieldUSBEcode;

// -----------------------------------------------------------------------------
// File send direct
#define USB_FileToDevice				0x00
#define USB_FileToHost					0x01

// -----------------------------------------------------------------------------
// File Attribute
#define	USB_FileTypeFolder				0x01
#define	USB_FileTypeNormal				0x02

// -----------------------------------------------------------------------------
#define USB_FileTypeSize				114
#define USB_FileNameSize				 95

@interface TFDataFormat : NSObject

#pragma mark Framing of communication blocks
- (UInt16) findCRC:(const UInt8*)p length:(size_t)n;
- (NSMutableData*) swap:(NSData*)inData;
- (NSData*) prepareCommand:(TopfieldUSBCmd)cmd withData:(NSData*)inData;
- (BOOL) isCommunicationBlockValid:(NSData*)block
	error:(NSError**)errorOut ecode:(TopfieldUSBEcode*)ecodeOut;
- (TopfieldUSBCmd) cmdFromCommunicationBlock:(NSData*)block;

#pragma mark Common ingredients
- (BOOL) appendToData:(NSMutableData*)data fname:(NSString*)fname;
- (BOOL) appendToData:(NSMutableData*)data sizeAndFname:(NSString*)fname;
- (NSString*) fnameForFile:(NSString*)file atPath:(NSString*)path;
- (NSString*) nameOfCmd:(TopfieldUSBCmd)cmd;
- (NSString*) nameOfEcode:(TopfieldUSBEcode)ecode;

#pragma mark Communication blocks for various commands
- (NSData*) prepareFailWithECode:(TopfieldUSBEcode)ecode;
- (TopfieldUSBEcode) ecodeFromFail:(NSData*)block;
- (NSData*) prepareSuccess;
- (NSData*) prepareCancel;
- (NSData*) prepareCmdHddDirWithPath:(NSString*)path;
- (NSData*) prepareCmdHddFileSendWithDirection:(UInt8)direction
	fname:(NSString*)fname offset:(UInt64)offset;
- (SInt64) offsetFromDataHddFileData:(NSData*)block;
- (NSData*) prepareDataHddFileEnd;
- (NSData*) prepareCmdTurboWithMode:(SInt32)mode;
- (NSData*) prepareCmdHddDelWithFname:(NSString*)fname;
- (NSData*) prepareCmdHddRenameFromFname:(NSString*)oldFname
	toFname:(NSString*)newFname;
- (NSData*) prepareCmdHddCreateDirWithFname:(NSString*)fname;

@end

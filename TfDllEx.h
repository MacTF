
#ifndef TFDLLEX_H
#define TFDLLEX_H

// -----------------------------------------------------------------------------
#ifndef	word
	typedef	unsigned short	word;
#endif
#ifndef	dword
	typedef	unsigned long	dword;
#endif
#ifndef	byte
	typedef unsigned char	byte;
#endif

// -----------------------------------------------------------------------------
#define	MAX_UsbSendBlockSize	0x10000  
#define	MAX_UsbBlockSize		0xffff  
#define MAX_UsbFileDataSize		0xfe00 
									   
#define	MAX_UsbDataSize			(MAX_UsbBlockSize-16)

// -----------------------------------------------------------------------------
#define	USB_CmdNone						0x00000000

#define	USB_Fail						0x00000001
#define	USB_Success						0x00000002
#define	USB_Cancel						0x00000003

#define	USB_CmdReady					0x00000100
#define	USB_CmdReset					0x00000101
#define	USB_CmdTurbo					0x00000102

// Hdd Cmd
#define	USB_CmdHddSize					0x00001000
#define	USB_DataHddSize					0x00001001

#define	USB_CmdHddDir					0x00001002
#define	USB_DataHddDir					0x00001003
#define	USB_DataHddDirEnd				0x00001004

#define	USB_CmdHddDel					0x00001005
#define	USB_CmdHddRename				0x00001006
#define	USB_CmdHddCreateDir				0x00001007

#define	USB_CmdHddFileSend				0x00001008
#define	USB_DataHddFileStart			0x00001009
#define	USB_DataHddFileData				0x0000100A
#define	USB_DataHddFileEnd				0x0000100B

// -----------------------------------------------------------------------------
// Usb Error Code
#define	USB_OK							0x00000000
#define	USB_Err_Crc						0x00000001
#define	USB_Err_UnknownCmd				0x00000002
#define	USB_Err_InvalidCmd				0x00000003
#define	USB_Err_Unknown					0x00000004
#define	USB_Err_Size					0x00000005
#define	USB_Err_RunFail					0x00000006
#define	USB_Err_Memory					0x00000007

// -----------------------------------------------------------------------------
// File send direct
#define USB_FileToDevice				0x00
#define USB_FileToHost					0x01

// -----------------------------------------------------------------------------
// File Attribute
#define	USB_FileTypeFolder				0x01
#define	USB_FileTypeNormal				0x02

// -----------------------------------------------------------------------------
#define USB_FileTypeSize				114
#define USB_FileNameSize				 95

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
extern "C" __declspec(dllimport) void	InitDevice( void );
extern "C" __declspec(dllimport) BOOL	DeviceArrival( char *pDeviceName );
extern "C" __declspec(dllimport) BOOL	DeviceRemoval( char *pDeviceName );
extern "C" __declspec(dllimport) BOOL	GetDeviceVidPid( DWORD *pVid, DWORD *pPid );
extern "C" __declspec(dllimport) BOOL	StartPnpNotice( HWND hwnd );
extern "C" __declspec(dllimport) BOOL	TryDeviceOpen( void );

// -----------------------------------------------------------------------------
extern "C" __declspec(dllimport) BOOL	UsbWrite( byte *pBuf, DWORD size );
extern "C" __declspec(dllimport) dword	UsbRead( byte *pBuf, DWORD size );
extern "C" __declspec(dllimport) BOOL	UsbCommandSend( dword cmd, byte *pBuf, dword size );

//------------------------------------------------------------------------------
extern "C" __declspec(dllimport) word	GetCrc16(void *data, unsigned long n);
extern "C" __declspec(dllimport) word	Get16bit(void *p);
extern "C" __declspec(dllimport) void	Put16bit(void *p, word data);
extern "C" __declspec(dllimport) dword	Get24bit(void *p);
extern "C" __declspec(dllimport) void	Put24bit(void *p, dword data);
extern "C" __declspec(dllimport) dword	Get32bit(void *p);
extern "C" __declspec(dllimport) void	Put32bit(void *p, dword data);
extern "C" __declspec(dllimport) byte	ExtractMjd(word mjd, word *year, byte *month, byte *day, byte *weekDay);
extern "C" __declspec(dllimport) word	MakeMjd(word year, byte month, byte day);

//------------------------------------------------------------------------------
extern "C" __declspec(dllimport) BOOL	UsbTaskFileSend( char *pDestName, char *pSrcName, HWND hWnd, WPARAM wParam );
extern "C" __declspec(dllimport) word	UsbTaskGetPercent( void );
extern "C" __declspec(dllimport) BOOL	UsbTaskCancel( void );
extern "C" __declspec(dllimport) BOOL	UsbTaskFileReceive( char *pDestName, char *pSrcName, HWND hWnd, WPARAM wParam );


// -----------------------------------------------------------------------------
#endif	TFDLLEX_H


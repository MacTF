//
//  AOSegmentedControl.m
//  MacTF
//
//  Created by Nathan Oates on 30/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "AOSegmentedControl.h"


@interface NSSegmentedCell ( PrivateMethod )
//- (void)_setSegmentedCellStyle:(int)style;
@end

@implementation AOSegmentedControl

- (void)awakeFromNib
{
    [self setFrameSize:NSMakeSize([self frame].size.width, 26)];
}

- (NSCell *)cell
{
    NSSegmentedCell *cell = [super cell];
 //   [cell _setSegmentedCellStyle:FlatSegmentedCellStyle];
    return cell;
}

@end

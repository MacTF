//
//  TFUSBController.h
//  MacTF
//
//  Created by Nathan Oates on Sun Aug 01 2004.
//  Copyright (c) 2004-7 Nathan Oates. All rights reserved.
//

// Includes code from uproar - see below for License:

/*
 *  uproar 0.1
 *  Copyright (c) 2001 Kasima Tharnpipitchai <me@kasima.org>
 *
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This file was modified by Kalle Olavi Niemitalo on 2007-10-28.  */

#import <Foundation/Foundation.h>
#include <IOKit/usb/IOUSBLib.h>

@class TFDataFormat, DataHandler;

//#define		kBlockSize		0x08
#define		kMaxXferSize	0xFFFF

#define		kUproarSuccess		0
#define    	kUproarTrackNotFound	-1
#define    	kUproarTrackExists	-2
#define		kUproarDeviceErr	-3
#define		kUproarNotEnoughSpace	-4


typedef struct {
    IOUSBDeviceInterface197	**dev;
    IOUSBInterfaceInterface197	**intf;
} USBDeviceContext;


@interface TFUSBController : NSObject {
	NSProgressIndicator* progressBar;
	NSTextField* progressTime;
	NSButton* turboCB;
	USBDeviceContext* myContext;
	NSMutableArray* transferQueue;
	NSMutableArray* priorityTransferQueue;
	NSMutableArray* pausedQueue;
	TFDataFormat* dataFormat;
	DataHandler* dh; //move
	id tableView;// and this?
}

- (void) closeDevice:(USBDeviceContext *)device;

- (USBDeviceContext*) initializeUSB;
- (int) getFile:(NSDictionary*)fileInfo forPath:(NSString*)currentPath
	toSaveTo:(NSString*)savePath beginAtOffset:(unsigned long long)offset
	withLooping:(BOOL)looping existingTime:(NSTimeInterval)existingTime;
- (void) uploadFile:(NSString*)fileToUpload ofSize:(long long)size
	fromPath:(NSString*)curPath withAttributes:(NSData*)typeFile
	atOffset:(unsigned long long)offset
	existingTime:(NSTimeInterval)existingTime;
- (int) getSpeed;
- (void) setDebug:(int)mode;
- (void) setRate:(int)newRate;
- (void) setProgressBar:(NSProgressIndicator*)bar time:(NSTextField*)timeField
	turbo:(NSButton*)turbo;
- (void) deleteFile:(NSDictionary*)fileInfo fromPath:(NSString*)currentPath;
- (void) transfer:(id)sender;
- (void) addPriorityTransfer:(id)newTransfer;
- (void) addTransfer:(id)newTransfer atIndex:(int)front; //-1 for end of array
- (void) setDH:(id)newDH tableView:(id)tv;
- (BOOL) hasCurrentTransfer;
- (id) currentTransferInfo;
- (void) clearQueues;
- (id) firstPausedTransferInfo;

@end

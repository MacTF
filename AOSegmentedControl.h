//
//  AOSegmentedControl.h
//  MacTF
//
//  Created by Nathan Oates on 30/01/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


#define NormalSegmentedCellStyle 1
#define FlatSegmentedCellStyle 2

@interface AOSegmentedControl : NSSegmentedControl
{
}
@end


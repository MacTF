//
//  Scripting_Additions.h
//  MacTF
//
//  Created by Nathan Oates on Fri Dec 31 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSApplication (Scripting_Additions)

- (id)handleUploadFileCommand:(NSScriptCommand *)command ;
- (id)handleDownloadFileCommand:(NSScriptCommand *)command ;
- (id)handleDeleteFileCommand:(NSScriptCommand *)command ;
- (id)handleConnectCommand:(NSScriptCommand *)command ;
- (id)handleDisconnectCommand:(NSScriptCommand *)command ;


@end

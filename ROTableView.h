/* ROTableView */

#import <Cocoa/Cocoa.h>

@interface ROTableView : NSTableView
{
	NSTrackingRectTag trackingTag;
	BOOL mouseOverView;
	int mouseOverRow;
	int lastOverRow;
}
@end

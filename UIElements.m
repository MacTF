// MacTF Copyright 2004 Nathan Oates

/* 
*
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
/* This file was modified by Kalle Olavi Niemitalo on 2007-10-18.  */

#import "UIElements.h"
#include <unistd.h>

@interface UIElements (PrivateMethods)
- (void) setCurrentPath:(NSString*)path;
@end

@implementation UIElements

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:
        (NSApplication *)theApplication {
    return YES;
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)path
{
	if (!connected) 
		[self connect:nil];
	[self uploadPath:path toPath:[NSString stringWithString:currentPath]];
	NSLog(@"DockUpload:%@", path);
	return YES;
}

-(void) awakeFromNib {
	connected = NO;
	paused = NO;
	highlightImageData = @"<4d4d002a 00000048 800f4f6d a2ca65ca 564b390a 69371941 1ee22622 dc04743b 7c86826e 900fcdb1 d9e5b237 3ab60647 06b0b8d8 d5151a1a 82732348 46616888 4bcd00f9 719f0100 000d0100 00030000 00010001 00000101 00030000 00010012 00000102 00030000 00030000 00ea0103 00030000 00010005 00000106 00030000 00010002 00000111 00040000 00010000 00080115 00030000 00010003 00000116 00040000 00010000 2aaa0117 00040000 00010000 003f011a 00050000 00010000 00f0011b 00050000 00010000 00f8011c 00030000 00010001 00000128 00030000 00010002 00000000 00000008 00080008 000afc80 00002710 000afc80 00002710 >";
	[self setCurrentPath:@"\\"];
	[currentlyField setStringValue:NSLocalizedString(@"IDLE", @"Idle")];
	[currentlyField displayIfNeeded];
	[tableView setDoubleAction: @selector(doubleClick:)];
	[tableView setTarget: self];
	[tableView registerForDraggedTypes: [NSArray arrayWithObjects: @"NSFilenamesPboardType", @"NSFilenamesPboardType", nil]];
	tfusbc = [[TFUSBController alloc] init];
	[tfusbc setDH:dh tableView:tableView]; //send needed items to tfusbc
	sortAscending = TRUE;
	[turboCB setState:0];
	[turboCB setEnabled:NO];
	[versionField setStringValue:[[[NSBundle mainBundle] infoDictionary] valueForKey:@"CFBundleVersion"]]; 
	[pathBar setEnabled:YES];
	[[pathBar cell] setSegmentCount:1];
	[[pathBar cell] setLabel:@"Path:" forSegment:0];
	[[pathBar cell] setWidth:0 forSegment:0];
	[pathBar sizeToFit];
	[[dh fileList] addObject:[NSDictionary dictionaryWithObject:NSLocalizedString(@"NOT_CONNECTED_ENTRY", @"Not connected") forKey:@"name"]];
	[tableView reloadData];
	prefs = [[NSUserDefaults standardUserDefaults] retain];
	if ([prefs boolForKey:@"auto-connect"]) {
		[self connect:nil];
		[autoCB setState:1];	
	}
}

- (BOOL)validateMenuItem:(NSMenuItem*)anItem {
    if ([[anItem title] isEqualToString:NSLocalizedString(@"DL_MENUITEM", @"Download")] && (1 > [tableView numberOfSelectedRows])) {
        return NO;
    }
	if ([[anItem title] isEqualToString:NSLocalizedString(@"DEL_MENUITEM", @"Delete")] && (1 > [tableView numberOfSelectedRows])) {
        return NO;
    }
	if ([[anItem title] isEqualToString:NSLocalizedString(@"RN_MENUITEM", @"Rename")] && (1 != [tableView numberOfSelectedRows])) {
        return NO;
    }
	if ([[anItem title] isEqualToString:NSLocalizedString(@"UL_MENUITEM", @"Upload")] && (! connected)) {
        return NO;
    }
	if ([[anItem title] isEqualToString:NSLocalizedString(@"NF_MENUITEM", @"NewFolder")] && (! connected)) {
        return NO;
    }
    return YES;
}

- (BOOL)validateToolbarItem:(NSToolbarItem *)toolbarItem {
    if ([[toolbarItem itemIdentifier] isEqual:@"ConnectTBIcon"]) {
		if (connected) {
			[toolbarItem setLabel:NSLocalizedString(@"DC_BUTTON", @"Disconnect")];
		} else {
			[toolbarItem setLabel:NSLocalizedString(@"CON_BUTTON", @"Connect")];
		}
	}
	if ([[toolbarItem itemIdentifier] isEqual:@"UploadTBIcon"] && (! connected)) {
		return NO;
	}
	if ([[toolbarItem itemIdentifier] isEqual:@"NewFolderTBIcon"] && (! connected)) {
		return NO;
	}
	if ([[toolbarItem itemIdentifier] isEqual:@"TurboTBIcon"] && (! connected)) {
		return NO;
	}
	if ([[toolbarItem itemIdentifier] isEqual:@"DownloadTBIcon"] && (1 > [tableView numberOfSelectedRows] || !connected)) {
		return NO;
	}
	if ([[toolbarItem itemIdentifier] isEqual:@"DeleteTBIcon"] && (1 > [tableView numberOfSelectedRows] || !connected)) {
		return NO;
	}
	if ([[toolbarItem itemIdentifier] isEqual:@"RenameTBIcon"] && (1 != [tableView numberOfSelectedRows] || !connected)) {
		return NO;
	}
    return YES;
}


- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	[preview setString:@""];
	if ([previewDrawer state] == NSDrawerClosedState) return; // don't bother getting previews if closed
	if ([tableView numberOfSelectedRows] != 1 || !connected)
		return;
	if ([tfusbc hasCurrentTransfer]) return; //for now, change to a priority transfer later?
	id currentSelectedItem = [[dh displayedList] objectAtIndex:[tableView selectedRow]];
	int type = [[currentSelectedItem objectForKey:@"type"] intValue];
	if (type == 2) { //file
		NSString* nameOnToppy = [currentSelectedItem objectForKey:@"name"]; 
		if (([nameOnToppy hasSuffix:@".ini"] || [nameOnToppy hasSuffix:@".txt"] || [nameOnToppy hasSuffix:@".tgd"] || [nameOnToppy hasSuffix:@".tsv"] || [nameOnToppy hasSuffix:@".dat"] || [nameOnToppy hasSuffix:@".tap"])) {
			[drawerTabView selectTabViewItemAtIndex:0];
			NSString* tempFile = NSTemporaryDirectory(); // download preview to temp folder
			tempFile = [tempFile stringByAppendingPathComponent:@"MacTFTemp"];
			[tfusbc addTransfer:[NSDictionary dictionaryWithObjectsAndKeys:currentSelectedItem,@"filename",currentPath,@"path",tempFile,@"savePath",[NSNumber numberWithUnsignedLongLong:0],@"offset",@"download",@"transferType",[NSNumber numberWithBool:NO],@"looping",[NSNumber numberWithInt:0],@"existingTime",nil] atIndex:-1];
			while ([tfusbc hasCurrentTransfer]) {
				usleep(10);
			}
			[preview setString:[NSString stringWithContentsOfFile:tempFile]];
		} else if ([nameOnToppy hasSuffix:@".rec"]) { //display headers
			[drawerTabView selectTabViewItemAtIndex:1];
			NSString* tempFile = NSTemporaryDirectory(); // download first 64k to temp folder
			tempFile = [tempFile stringByAppendingPathComponent:@"MacTFTemp"];
			[tfusbc addTransfer:[NSDictionary dictionaryWithObjectsAndKeys:currentSelectedItem,@"filename",currentPath,@"path",tempFile,@"savePath",[NSNumber numberWithUnsignedLongLong:0],@"offset",@"download",@"transferType",[NSNumber numberWithBool:NO],@"looping",[NSNumber numberWithInt:0],@"existingTime",nil] atIndex:-1];
			while ([tfusbc hasCurrentTransfer]) {
				usleep(10);
			}
			NSDictionary* recData = [dh extractDataFromRecHeader:tempFile forModel:[prefs objectForKey:@"modelType"]]; //then parse and display headers
			[recStart setObjectValue:[recData objectForKey:@"startTime"]];
			[recDuration setObjectValue:[recData objectForKey:@"duration"]];
			[recDescription setStringValue:[recData objectForKey:@"description"]];
			[recName setStringValue:[recData objectForKey:@"name"]];
			[recExtInfo setStringValue:[recData objectForKey:@"extInfo"]];
			[recChannel setStringValue:[recData objectForKey:@"channel"]];
		}	
		else {
			[drawerTabView selectTabViewItemAtIndex:0];
			return;
		}
	} else if (type == 1) { //folder
		[drawerTabView selectTabViewItemAtIndex:0];
		return;
	} else {
		[statusField setStringValue:NSLocalizedString(@"SEL_ERROR", @"Selection error...")];
		return;
	}
}

- (void) setAvailableSpace:(NSData*)input {
	[availSpaceField setStringValue:[self prepForSizeDisplay:input]];
}

- (void) setFreeSpace:(NSData*) input {
	[freeSpaceField setStringValue:[self prepForSizeDisplay:input]];
}

- (NSString*) prepForSizeDisplay:(NSData*) input {
	UInt32 size_bigendian;
	[input getBytes:&size_bigendian];
	unsigned size = EndianU32_BtoN(size_bigendian);
	NSString *ret = nil;
		if( size == 0. ) ret = NSLocalizedString( @" - ", "no file size" );
		else if( size > 0. && size < 1024. ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.0f KB", "file size measured in kilobytes" ), size];
		else if( size >= 1024. && size < pow( 1024., 2. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.1f MB", "file size measured in megabytes" ), ( size / 1024. )];
		else if( size >= pow( 1024., 2. ) && size < pow( 1024., 3. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.2f GB", "file size measured in gigabytes" ), ( size / pow( 1024., 2. ) )];
		else if( size >= pow( 1024., 3. ) && size < pow( 1024., 4. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.3f TB", "file size measured in terabytes" ), ( size / pow( 1024., 3. ) )];
		else if( size >= pow( 1024., 4. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.4f PB", "file size measured in pentabytes" ), ( size / pow( 1024., 4. ) )];
	return ret;
}

- (IBAction) connect: (id) sender { 
	if (! connected) {
		context = [tfusbc initializeUSB];
		if (context == nil) {
			[statusField setStringValue:NSLocalizedString(@"INIT_PROBLEM", @"Message when initialization fails.")];
			return;
		}
		// add a check here to see if really connected...
		connected = YES;
		[[dh fileList] removeAllObjects];
		[[dh fileList] addObject:[NSDictionary dictionaryWithObject:NSLocalizedString(@"FETCHING", @"Fetching") forKey:@"name"]];
		[tableView reloadData];
		[tfusbc clearQueues];
		paused = NO;
		[turboCB setState:0];
		[turboCB setEnabled:YES];
		[[pathBar cell] setSegmentCount:2];
		[[pathBar cell] setLabel:@"Path:" forSegment:0];
		[[pathBar cell] setLabel:@"\\" forSegment:1];
		[[pathBar cell] setWidth:0 forSegment:0];
		[[pathBar cell] setWidth:0 forSegment:1];
		[pathBar sizeToFit];
		[pathBar setHidden:NO];
		[connectLight setImage:[NSImage imageNamed:@"green.tiff"]];
		[connectButton setTitle:NSLocalizedString(@"DC_BUTTON", @"Disconnect.")];
		[statusField setStringValue:[NSString stringWithFormat:NSLocalizedString(@"CONNECTED_MESSAGE", @"Message giving connection speed."), [tfusbc getSpeed]]];
		[NSThread detachNewThreadSelector:@selector(transfer:) toTarget:tfusbc withObject:nil];
	} else { // already connected
		connected = NO;
		if ([tfusbc hasCurrentTransfer]) { //gives a few seconds for busy thread to notice it should not be connected before cutting the device off
			sleep(3);
		}
		[tfusbc closeDevice:context];
		[connectButton setTitle:NSLocalizedString(@"CON_BUTTON", @"Connect.")];
		[statusField setStringValue:NSLocalizedString(@"NO_CONNECT", @"No connection.")];
		[[dh fileList] removeAllObjects];
		[tableView reloadData];
		[turboCB setState:0];
		[turboCB setEnabled:NO];
		[connectLight setImage:[NSImage imageNamed:@"red.tiff"]];
		[self setCurrentPath:@"\\"];
		return;
	}
	int ret = [self goToPath:@"\\"];
	if (ret == 1) { 
		sleep (5); //error, wait a few secs for HDD to start if need be then try again
		[self goToPath:@"\\"];
	}
	[self setCurrentPath:@"\\"];
}

- (int) goToPath:(NSString*) path {
	[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:path,@"path",@"fileList",@"transferType",nil]];
	// FIXME: The caller would like to know whether the operation succeeded.
	// But it has not even finished yet.  So goToPath should somehow notify
	// the caller on completion.
	return 0;
}

- (void)doubleClick:(id)sender
{
	if ([tableView numberOfSelectedRows] != 1)
		return;
	id currentSelectedItem = [[dh displayedList] objectAtIndex:[tableView selectedRow]];
	int type = [[currentSelectedItem objectForKey:@"type"] intValue];
	if (type == 2) { //file
		[self downloadFileDoubleClickThread:nil];
	} else if (type == 1) { //folder
		NSString* currentName = [currentSelectedItem objectForKey:@"name"];
		if ([currentName isEqualToString:@".. (Parent folder)"]) { // move up
			NSMutableArray* array = [NSMutableArray arrayWithArray:[currentPath componentsSeparatedByString:@"\\"]];
			if ([array count] > 1){ // not going to root
				[array removeLastObject];
				NSString* temp = [NSString stringWithString:[array componentsJoinedByString:@"\\"]];
				if ([self goToPath:temp]) {// if error
					[self goToPath:temp]; // hackish workaround
				}
				int segCount = [pathBar segmentCount]; //remember count starts at 1, not zero!
				NSRect r = [pathBar frame];
				[[pathBar cell] setSegmentCount:segCount-1];
				[pathBar setNeedsDisplayInRect:r];
				[pathBar displayIfNeeded];
				[self setCurrentPath:temp];
				[statusField setStringValue:NSLocalizedString(@"CONNECTED_OK", @"Connection OK")];
			} else {//root
				[self setCurrentPath:@"\\"];
				[[pathBar cell] setSegmentCount:1];
			}
		} else { //move down
			NSMutableString* temp = [NSMutableString stringWithString:currentPath];
			if (![temp isEqualToString:@"\\"])
					[temp appendString:@"\\"];
			[temp appendString:currentName];
			if ([self goToPath:temp]) { // if error, try again
				[self goToPath:temp]; // hackish workaround
	        }
			[self setCurrentPath:temp];
			int segCount = [pathBar segmentCount]; //remember count starts at 1, not zero!
			NSRect r = [pathBar frame];
			[[pathBar cell] setSegmentCount:segCount+1];
			[[pathBar cell] setLabel:currentName forSegment:segCount];
			[[pathBar cell] setWidth:0 forSegment:segCount];
			[pathBar sizeToFit];
			[pathBar displayIfNeededInRect:r];
			[statusField setStringValue:NSLocalizedString(@"CONNECTED_OK", @"Connection OK")];
		}
	} else {
		[statusField setStringValue:NSLocalizedString(@"SEL_ERROR", @"Selection error...")];
		return;	
	}
}

- (void) downloadFileDoubleClickThread:(NSDictionary*)fileInfo {
	if (! connected) return;
		[self downloadSelectedFile:nil];
//	[self updateHDDSize];
}

/*- (void) updateHDDSize {
	if (! connected) return;
	NSData* hddSizeString = [tfusbc getHDDSize:context];
	if (hddSizeString != nil) {
		[self setAvailableSpace:[hddSizeString subdataWithRange:(NSRange) {8,4}]];
		[self setFreeSpace:[hddSizeString subdataWithRange:(NSRange) {12,4}]];
	} 
}*/

- (void)tableView:(NSTableView *)tView didClickTableColumn:(NSTableColumn *)tableColumn
{
	// Either reverse the sort or change the sorting column
	NSString* columnIdentifier = [tableColumn identifier];
	BOOL newSortAscending = !(sortAscending && [tableColumn isEqualTo:selectedColumn]);
	
	NSString* sortKey = nil;
	SEL sortSelector = @selector(compare:);
	if ([columnIdentifier isEqualToString:@"name"]) {
		sortKey = @"name";
		sortSelector = @selector(caseInsensitiveCompare:);
	} else if ([columnIdentifier isEqualToString:@"date"]) {
		sortKey = @"sortdate";
	} else if ([columnIdentifier isEqualToString:@"size"]) {
		sortKey = @"fileSize";
	} else if ([columnIdentifier isEqualToString:@"icon"]) {
		sortKey = @"suffix";
	}
	if (sortKey == nil)
		return;

	NSSortDescriptor* descriptor = [[[NSSortDescriptor alloc]
			initWithKey:sortKey ascending:newSortAscending selector:sortSelector]
		autorelease];
	NSArray* sortedArray = [[dh fileList] sortedArrayUsingDescriptors:
		[NSArray arrayWithObject:descriptor]];
	NSImage* indicatorImage = [NSImage imageNamed:
		newSortAscending ? @"NSAscendingSortIndicator" : @"NSDescendingSortIndicator"];

	sortAscending = newSortAscending;
	selectedColumn = tableColumn;
	[tView setIndicatorImage: indicatorImage inTableColumn: tableColumn];
	// remove other indicators
	NSMutableArray* otherColumns = [NSMutableArray arrayWithArray:[tView tableColumns]];
	[otherColumns  removeObject:tableColumn];
 	NSEnumerator* cols = [otherColumns objectEnumerator];
	id currentCol;
	while (currentCol = [cols nextObject]) {
		[tView setIndicatorImage: nil inTableColumn: currentCol];
	}
	[dh setFileList:sortedArray];
	[dh search:searchField];
	[dh reloadTable];
}


- (IBAction) downloadSelectedFile:(id)sender{
	if (! connected)
		return;
	NSLog(@"Download file starting...");
	NSEnumerator* selected = [tableView selectedRowEnumerator];
	id currentSelected;
	while (currentSelected = [selected nextObject]) {
		id currentSelectedItem = [[dh displayedList] objectAtIndex:[currentSelected intValue]];
		int type = [[currentSelectedItem objectForKey:@"type"] intValue];
		if (type == 1) {[statusField setStringValue:NSLocalizedString(@"NO_FOLDER_DL", @"Folder download not supported yet")];}
		else if (type == 2) {
			NSString* nameOnToppy = [currentSelectedItem objectForKey:@"name"];
			NSSavePanel *nssave = [[NSSavePanel savePanel]retain];
			int retButton = [nssave runModalForDirectory:nil file:nameOnToppy];
			if (retButton) {
				NSString* savePath= [nssave filename]; 
				NSLog(savePath);
				unsigned long long offset = 0;
				NSFileHandle *newFileHandle = [NSFileHandle fileHandleForReadingAtPath:savePath];
				if (!(newFileHandle==nil)) { // there is a file there, ask if want to resume
					NSLog(@"existing file length: %qu", [newFileHandle seekToEndOfFile]);
					//ask to resume, save offset if yes
					//offset = [newFileHandle seekToEndOfFile];
				}
				[tfusbc setProgressBar:progressBar time:progressTime turbo:turboCB];
				[tfusbc addTransfer:[NSDictionary dictionaryWithObjectsAndKeys:currentSelectedItem,@"filename",currentPath,@"path",savePath,@"savePath",[NSNumber numberWithUnsignedLongLong:offset],@"offset",@"download",@"transferType",[NSNumber numberWithBool:YES],@"looping",[NSNumber numberWithInt:0],@"existingTime",nil] atIndex:-1];
			}
		}		
	}
	//		[self updateHDDSize];
	}

- (void) finishTransfer {
	[currentlyField setStringValue:@""];
	[progressBar setDoubleValue:0];
	[[NSSound soundNamed:@"Ping"] play];
	[connectLight setImage:[NSImage imageNamed:@"green.tiff"]];
	[currentlyField setStringValue:NSLocalizedString(@"IDLE", @"Idle")];
	[currentlyField display];
}

- (IBAction) uploadFile: (id) sender {
	if (! connected)
		return;
	NSOpenPanel *nsop = [[NSOpenPanel openPanel]retain];
	[nsop setAllowsMultipleSelection:YES];
	[nsop setCanChooseFiles:YES];
	[nsop setCanChooseDirectories:YES];
	int retButton = [nsop runModal];
	NSArray *returnedNames = [nsop filenames];	
	if (retButton == NSFileHandlingPanelCancelButton) {		
		NSLog(@"Upload cancelled");
		return;
	}
	//get info about the chosen file - later if do folders check here and do recursive all things in folder???
	NSEnumerator *enumerator = [returnedNames objectEnumerator];
	id returnedName;
//	[statusField setStringValue:[NSLocalizedString(@"CONNECTED_OK", @"Connection OK") stringByAppendingString:NSLocalizedString(@"DOWNLOAD", @"Download")]; 
	[connectLight setImage:[NSImage imageNamed:@"blink.tiff"]];
	while (returnedName = [enumerator nextObject]) {
		NSLog(returnedName);
		[self uploadPath:returnedName toPath:[NSString stringWithString:currentPath]];
	}
}



- (void) uploadPath:(NSString*) path toPath:(NSString*) toPath {
	if (! connected)
		return;
	NSFileManager* fm = [NSFileManager defaultManager];
	NSDictionary *fattrs = [fm fileAttributesAtPath:path traverseLink:NO];
	if (!fattrs) {
		NSLog(@"Path of file to upload is incorrect!");
		return; }
	NSString* type = [fattrs fileType];
	if ([type isEqualToString:@"NSFileTypeDirectory"]) {
		// should look to create folder on Toppy, or add stuff if already there (maybe it does this automatically?), and place in subfolders etc
		[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:[path lastPathComponent],@"newName",toPath,@"path",@"newFolder",@"transferType", nil]];
		NSArray* subPaths = [fm directoryContentsAtPath:path];
		NSEnumerator* e = [subPaths objectEnumerator];
		id object;
		while (object = [e nextObject]) {
			NSString* subPath = [NSString stringWithFormat:@"%@/%@", path, object];
			NSString* subToPath = [NSString stringWithFormat:@"%@\\%@", toPath, [path lastPathComponent]];
		//	NSLog(@"subdir: %@, %@", subPath, subToPath);
			if (![object isEqualToString:@".DS_Store"]) {
				[self uploadPath:subPath toPath:subToPath];
			}
		}
		if (connected) { //may have disconnected during threaded upload (although not actually threaded here!)
	//		[self updateHDDSize];
			[self goToPath:currentPath];
		}
	}
	else {
		NSDictionary* fileAttr = [dh extractAttributes:fattrs];		// turn Dict into a new TypeFile dict
		NSMutableArray* array = [NSMutableArray arrayWithArray:[path componentsSeparatedByString:@"/"]]; // cut down name
		NSString* fileName = [NSString stringWithString:[array lastObject]];
		NSData* typeFile = [dh getDataFromTFFile:fileAttr withName:fileName];
		[progressBar setDoubleValue:0];
		[currentlyField setStringValue:[NSLocalizedString(@"UPLOADING", @"Uploading") stringByAppendingString:fileName]];
		[currentlyField displayIfNeeded];
		[tfusbc setProgressBar:progressBar time:progressTime turbo:turboCB];
		[tfusbc addTransfer:[NSDictionary dictionaryWithObjectsAndKeys:path,@"filename",[NSNumber numberWithUnsignedLongLong:[fattrs fileSize]],@"fileSize",toPath,@"path",typeFile,@"attributes",@"upload",@"transferType",[NSNumber numberWithUnsignedLongLong:0],@"offset",[NSNumber numberWithInt:0],@"existingTime",nil] atIndex:-1];
	}
}

-(IBAction)deleteFile:(id)sender
{
	if (! connected)
		return;
	if ( [tableView numberOfSelectedRows] == 0 )
        return;
	NSString *title = NSLocalizedString(@"WARNING", @"Warning!");
    NSString *defaultButton = NSLocalizedString(@"DEL", @"Delete");
    NSString *alternateButton = NSLocalizedString(@"NO_DEL", @"Don't Delete");
    NSString *otherButton = nil;
    NSString *message = NSLocalizedString(@"DEL_MESSAGE", @"Are you sure you want to delete the selected file(s)?");

    NSBeep();
    NSBeginAlertSheet(title, defaultButton, alternateButton, otherButton, mainWindow, self, @selector(sheetDidEnd:returnCode:contextInfo:), nil, nil, message);
}

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    if ( returnCode == NSAlertDefaultReturn ) {
		NSLog(@"Deleting file(s) starting...");
		NSEnumerator* selected = [tableView selectedRowEnumerator];
		id currentSelected;
		while (currentSelected = [selected nextObject]) {
			id currentSelectedItem = [[dh displayedList] objectAtIndex:[currentSelected intValue]];
			[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:currentSelectedItem,@"file",currentPath,@"path",@"delete",@"transferType", nil]];
		}
	//	[self updateHDDSize];
		[self goToPath:currentPath];
		//should check so don't delete root folders?
	}
}

- (IBAction)openPrefsSheet:(id)sender
{
	[NSApp beginSheet:prefsWindow
	   modalForWindow:mainWindow
		modalDelegate:self
	   didEndSelector:@selector(mySheetDidEnd:returnCode:contextInfo:)
          contextInfo: nil];
	return;  // leave without doing anything else
}

- (IBAction) mySheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode
			   contextInfo:(void *)contextInfo {
	
	[sheet orderOut:self];
	if(returnCode == 0)  return;
	// continue with application  
}

- (IBAction)closePrefsSheet:(id)sender
{
	//do changes to prefs here
	//    if (connected) {
	[tfusbc setDebug:[debugCB state]]; 
	//  }
	NSLog(@"auto: %i", [autoCB state]);
	[prefs setBool:[autoCB state] forKey:@"auto-connect"];
//	[prefs setBool:[epgCB state] forKey:@"epgSync"];
	[prefs synchronize];
	[NSApp endSheet:prefsWindow returnCode:1];
}

- (IBAction)openRenameSheet:(id)sender
{
	if ( [tableView numberOfSelectedRows] != 1 )
        return;
	[renameOld setStringValue:[[[dh displayedList] objectAtIndex:[tableView selectedRow]] objectForKey:@"name"]];
	[renameNew setStringValue:[renameOld stringValue]];
//	[renameWindow setTitleBarHeight:0.0];
	[NSApp beginSheet: renameWindow
	   modalForWindow: mainWindow
		modalDelegate: nil
	   didEndSelector: nil
		  contextInfo: nil];
    [NSApp runModalForWindow: renameWindow];
    // Sheet is up here.
}

- (IBAction)cancelRename:(id)sender{
	[NSApp endSheet: renameWindow];
    [renameWindow orderOut: self];
	[NSApp stopModal];
}

- (IBAction)closeRenameSheet:(id)sender
{
	//do changes to rename here
	if (connected) {
		[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:[renameOld stringValue],@"oldName",[renameNew stringValue],@"newName", currentPath,@"path",@"rename",@"transferType",nil]];
		[self goToPath:currentPath];
	}
		//then close up
	[self cancelRename:sender];
}


- (IBAction)openNewFolderSheet:(id)sender
{
	[NSApp beginSheet: newFolderWindow
	   modalForWindow: mainWindow
		modalDelegate: nil
	   didEndSelector: nil
		  contextInfo: nil];
    [NSApp runModalForWindow: newFolderWindow];
    // Sheet is up here.
	
}

- (IBAction)cancelNewFolder:(id)sender{
	[NSApp endSheet: newFolderWindow];
    [newFolderWindow orderOut: self];
	[NSApp stopModal];
}

- (IBAction)closeNewFolderSheet:(id)sender
{
	//do changes to make new folder here
	if (connected) {
		[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:[newFolderNew stringValue],@"newName",currentPath,@"path",@"newFolder",@"transferType", nil]];
		[self goToPath:currentPath];
	}
	//then close up
	[self cancelNewFolder:sender];
}

- (IBAction)pathBarClick:(id)sender {
	int segCount = [pathBar segmentCount]; //remember count starts at 1, not zero!
	int selectedSegment = [sender selectedSegment];	
	if (selectedSegment == segCount - 1) return; //no need to move anywhere
	int i = 1; // remember have "path" there too 
	NSMutableArray* pathArray = [NSMutableArray arrayWithCapacity:selectedSegment];
	while (i<=selectedSegment) {
		[pathArray addObject:[[pathBar cell] labelForSegment:i]];
		i++;
	} 
	NSString* pathString = [pathArray componentsJoinedByString:@"\\"];
	if ([pathString hasPrefix:@"\\\\"]) pathString = [pathString substringFromIndex:1];
	[self goToPath:pathString];
	[self setCurrentPath:pathString];
	NSRect r = [pathBar frame];
	[[pathBar cell] setSegmentCount:selectedSegment+1];
	[pathBar displayIfNeededInRect:r];	
}

- (id)getTfusbc {
	return tfusbc;
}

- (USBDeviceContext*) getContext {
	return context;
}

- (NSNumber*) isConnected {
	return [NSNumber numberWithBool:connected];
}


- (NSString*) currentPath {
	return currentPath;
}

- (IBAction) toggleTurbo:(id)sender {
	if ([tfusbc hasCurrentTransfer]) { //have a current transfer, lets toggle it
		[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:@"turbo",@"transferType",[NSNumber numberWithBool:[turboCB state]],@"turboOn",nil]];
	}
}

- (IBAction) toggleTurboCB:(id)sender {
	if (! connected)
		return;
	if ([turboCB state]) { // currently on, want off
		[turboCB setState:0];
	} else {
		[turboCB setState:1];
	}
	if ([tfusbc hasCurrentTransfer]) { //have a current transfer, lets toggle it
			[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:@"turbo",@"transferType",[NSNumber numberWithBool:[turboCB state]],@"turboOn",nil]];
	}
}

- (IBAction) togglePreview:(id)sender {
		[previewDrawer toggle:sender];
}

/*- (IBAction) snapshot:(id) sender {
	NSArray* snapshotData = [self snapshotOfPath:@"\\"];
	[dh setSnapShot:snapshotData];
	// show snapshot window
	[snapshotWindow makeKeyAndOrderFront:sender];
	[outlineView reloadData];
}

- (NSArray*) snapshotOfPath:(NSString*)path {
	
NSData* hddFileData = [tfusbc getFileList:context forPath:path];
NSData* checkForError = [hddFileData subdataWithRange:(NSRange){4,4}];
const UInt32 check_bigendian = EndianU32_NtoB(USB_Fail);
const UInt32 check2_bigendian = EndianU32_NtoB(USB_DataHddDir);
if ([checkForError isEqualToData:[NSData dataWithBytes:&check_bigendian length:4]]) {
	[statusField setStringValue:@"Connected - error on last command"];
}
if (! [checkForError isEqualToData:[NSData dataWithBytes:&check2_bigendian length:4]]) {
	hddFileData = [tfusbc getFileList:context forPath:path]; //try again
	if ([checkForError isEqualToData:[NSData dataWithBytes:&check_bigendian length:4]]) {
		[statusField setStringValue:@"Connected - error on last command"];
	}
}
hddFileData = [hddFileData subdataWithRange:(NSRange) {8, [hddFileData length]-8}]; // cut off header and cmd 
int i;
NSMutableArray* paths = [[NSMutableArray alloc] init];
for (i=0; i*114 < [hddFileData length]-4; i++) { // 4 is there cause swapping sometimes adds a byte of padding  
	NSData* temp = [hddFileData subdataWithRange:(NSRange) {i*114,114}];
	NSMutableDictionary* tfFile = [dh newTFFileFromSwappedHexData:temp];
	[dh convertRawDataToUseful:tfFile];
	if (!([[tfFile objectForKey:@"name"] isEqualToString:@".."])) {
		if ([[tfFile objectForKey:@"type"] intValue] == 1) { //folder
			NSMutableString* temp = [NSMutableString stringWithString:path];
			if (![temp isEqualToString:@"\\"])
				[temp appendString:@"\\"];
			[temp appendString:[tfFile objectForKey:@"name"]];
			[paths addObject:tfFile];
			[paths addObject:[self snapshotOfPath:temp]];
		} else {
			[paths addObject:tfFile];
		}
	}
	[tfFile release];
}
return paths;
}

*/

/*- (IBAction) epg:(id) sender {
	XMLToEPG *xmlConv = [[XMLToEPG alloc] init];
	NSOpenPanel *nsop = [[NSOpenPanel openPanel]retain];
    NSArray *fileType = [NSArray arrayWithObject:@"xml"];
    [nsop setAllowsMultipleSelection:NO];
    [nsop setCanChooseFiles:YES];
    [nsop setCanChooseDirectories:NO];
    [nsop runModalForTypes:fileType];
    NSMutableArray* importedXML = [xmlConv importXML:[nsop URL]];
	[xmlConv fixData:importedXML];
	[xmlConv exportFixedXML:importedXML toFile:@"/Volumes/Tyr/nathan/Desktop/TV/Guides/test.tgd"];
}
*/


/*	if ([prefs boolForKey:@"epgSync"]) {
			[epgCB setState:1];
			XMLToEPG *xmlConv = [[XMLToEPG alloc] init];
			NSString* path = @"/Volumes/Tyr/nathan/Desktop/TV/Guides/tv.xml";
			NSString* tempFile = @"/Volumes/Tyr/nathan/Desktop/TV/Guides/test.epg";
			NSMutableArray* importedXML = [xmlConv importXML:[NSURL fileURLWithPath:path]];
			[xmlConv fixData:importedXML];
			[xmlConv exportFixedXML:importedXML toFile:tempFile];
			if (! connected)
				return;
			NSFileManager* fm = [NSFileManager defaultManager];
			NSDictionary *fattrs = [fm fileAttributesAtPath:tempFile traverseLink:NO];
			if (!fattrs) {
				NSLog(@"Path of file to upload is incorrect!");
				return;
			}
			NSDictionary* fileAttr = [dh extractAttributes:fattrs];
			NSMutableArray* array = [NSMutableArray arrayWithArray:[tempFile componentsSeparatedByString:@"/"]];
			NSString* fileName = [NSString stringWithString:[array lastObject]];
			NSData* typeFile = [dh getDataFromTFFile:fileAttr withName:fileName];
			[tfusbc uploadFile:tempFile ofSize:[fattrs fileSize] fromPath:@"\\Program Files\\TimerKey\\" withAttributes:typeFile toDevice:context];
			[self goToPath:currentPath];
			[self resetTurbo];
		} */



/*- (void)tableView:(NSTableView *)aTableView willDisplayCell:(id)aCell forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
 {
	if ([prefs objectForKey:@"NoRollover"] != nil) return;
	if ([aTableView mouseOverRow] == rowIndex && ([aTableView selectedRow] != rowIndex))
			if ([aTableView lockFocusIfCanDraw]) {
				NSRect rowRect = [aTableView rectOfRow:rowIndex];
				NSRect columnRect = [aTableView rectOfColumn:[[aTableView tableColumns] indexOfObject:aTableColumn]];

					if (!highlightImage) {
						highlightImage = [[NSImage alloc] initWithData:[highlightImageData propertyList]];
							[highlightImage setDataRetained:YES]; //?
							[highlightImage setCacheMode:NSImageCacheAlways]; //?
					}

				[highlightImage drawInRect:NSIntersectionRect(rowRect, columnRect) fromRect:NSMakeRect(0,0,1,[highlightImage size].height) operation:NSCompositeSourceOver fraction:0.3];
				[aTableView unlockFocus];
			}
 }
*/

- (void)dealloc
{
  if (highlightImage) [highlightImage release];
[super dealloc];
}

- (IBAction)pauseCurrentTransfer:(id)sender {
	if (!paused) {
	[pauseButton setImage:[NSImage imageNamed:@"DownloadResume.tif"]];
	[pauseButton setAlternateImage:[NSImage imageNamed:@"DownloadResumePressed.tif"]];
	//add priority "halt" to queue naming file
	id current = [tfusbc currentTransferInfo];
	[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:@"pause",@"transferType",[current objectForKey:@"filename"],@"filename",nil]];
	paused = YES;
	[currentlyField setStringValue:[NSLocalizedString(@"PAUSED", @"Paused: ") stringByAppendingString:[[current objectForKey:@"filename"]objectForKey:@"name"]]];
	[connectLight setImage:[NSImage imageNamed:@"green.tiff"]];
	[currentlyField displayIfNeeded];
	} else {
	//change image to pause
	//add priority "resume" to queue naming file
	[pauseButton setImage:[NSImage imageNamed:@"DownloadStop.tif"]];
	[pauseButton setAlternateImage:[NSImage imageNamed:@"DownloadStopPressed.tif"]];
	id current = [tfusbc firstPausedTransferInfo];//temp as for now only have 1 paused
	[tfusbc addPriorityTransfer:[NSDictionary dictionaryWithObjectsAndKeys:@"resume",@"transferType",[current objectForKey:@"filename"],@"filename",nil]];
	paused = NO;
	[currentlyField setStringValue:[NSLocalizedString(@"DOWNLOADING", @"Downloading: ") stringByAppendingString:[[current objectForKey:@"filename"]objectForKey:@"name"]]];
	[connectLight setImage:[NSImage imageNamed:@"blink.tiff"]];
	[currentlyField displayIfNeeded];
	}
}

- (id) selectedColumn {
	return selectedColumn;
}
- (id) currentlyField {
	return currentlyField;
}
- (id) connectLight {
	return connectLight;
}

@end

@implementation UIElements (PrivateMethods)

// To ensure proper maintenance of reference counts, all updates of currentPath
// go via this method.
- (void) setCurrentPath:(NSString*)path {
	// Retain first, in case it's the same or a related object.
	[path retain];
	[currentPath release];
	currentPath = path;
}

@end

#! /usr/bin/perl

use strict;
use warnings;

my($in_name, $out_name) = @ARGV;

open my $in_h, "<", $in_name or die "$in_name: $!";
binmode $in_h or die "$in_name: $!";
my $head;
defined(sysread $in_h, $head, 4) or die "$in_name: $!";

my($skip, $charset);
if ($head =~ /^\x00\x00\xFF\xFE/) {
	print "$in_name: Found big-endian UTF-32 BOM\n";
	($skip, $charset) = (0, "UTF-32");
} elsif ($head =~ /^\xFE\xFF\x00\x00/) {
	print "$in_name: Found little-endian UTF-32 BOM\n";
	($skip, $charset) = (0, "UTF-32");
} elsif ($head =~ /^\xFE\xFF/) {
	print "$in_name: Found big-endian UTF-16 BOM\n";
	($skip, $charset) = (0, "UTF-16");
} elsif ($head =~ /^\xFF\xFE/) {
	print "$in_name: Found little-endian UTF-16 BOM\n";
	($skip, $charset) = (0, "UTF-16");
} elsif ($head =~ /^\xEF\xBB\xBF/) {
	print "$in_name: Found UTF-8 BOM\n";
	($skip, $charset) = (3, "UTF-8");
} else {
	print "$in_name: No BOM found -- assuming UTF-8\n";
	($skip, $charset) = (0, "UTF-8");
}

sysseek($in_h, $skip, 0) or die "$in_name: $!";

open STDOUT, ">", $out_name or die "$out_name: $!";
open STDIN, "<&", $in_h or die "$in_name: $!";
exec "iconv", "-f", $charset, "-t", "UTF-16"
	or die "exec iconv: $!";

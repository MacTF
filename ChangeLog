This file was modified by Kalle Olavi Niemitalo on 2007-10-18.

0.1		Initial hack version - talks to Toppy and gets HDD size info

0.2		Added ability to browse around Toppy filesystem.
		Download works for small files.
		Lots of internal parts changed to simplify things.

0.21	Fixed bug in downloading.

0.3		Save Panel added to choose where to save files to.
		Fixed memory problem where it was not cleared during long downloads.

0.35	Cancelling the "Save As..." panel should now work.
		Added Turbo Mode.

0.4		Upload files works (for files <64kB is size).
		Multiple file download. 
		Bugfix in endian logic.
	
0.5		More error checking in download to prevent crashes if strange Toppy response
		Upload of large files
		Debug preference added
		Determinate progress bar added
		Removed extra NSLog() statements to cut down on console.log bloat
		Transfer timer added
		Workaround for navigation errors, so it seems much smoother now
		Delete function added (Cmd-Delete)...
		
0.6     Fix for freeze on second download
	    Connected message now indicates connection speed
		Time display is now 24h time (oops)
		Folders no longer show a date
		Strange (non-ASCII) characters now work
		Added menu for all the functions (and keyboard commands)
		Files are now saved with dates from Toppy
		Rename added
		
0.61	Menus now disabled when not applicable
	    Rename fixed to work on any folder!
		Timezones are now taken into account in dates
		
0.62	Small change to upload that should hopefully make it more reliable (not publically released)

0.63	Fix bug introduced in 0.62 (not publically released)

0.7     Added update checking
	    Added feedback menu item
		Possible fix for various USB 1.1 connection problems
		Cosmetic bug fixed
		
0.71	Fixed a problem with really really big filesizes (not publically released)

0.72	Fixed problem with multiple deletes in turbo mode (not publically released)

0.8		Multiple uploads possible (from same folder)
		Sorting of table columns added

0.9		Turbo checkbox disabled until valid
		Fix for occasional non-turbo first download when turbo should be on
		German localization
		Beep at end of upload/download (might be a pref later)

1.0		Added pref to workaround upload bug	on specific sized files 
	    Slightly less timer updates (hopefully improve turbo speed)
		Transferring file name is shown
		New Folder command
		Upload of folders
		Basic AppleScript support (Connect/Upload/Download/Disconnect)
		
1.0.1	Slight bug in New Folder in German Localization fixed

1.1		Icon changed slightly to look better
		USB transfers now use timeouts
		Upload bug finally fixed
		
1.1.1	10.2 compatability fixed (broke in shift to Xcode 2.1)
		Filenames containing control characters now work (German)
		
1.2		Drag and drop upload
		Close widget now quits
		Double-clicking a file downloads it
		Filetype icons added
		Preview of txt/ini/tgd files (<100kb) in drawer
		
1.3		UI redesigned around toolbar
		Menu options moved around
		Can now drag to Dock icon to upload
		Sort by file extension
		Multi-threaded upload and download (except drag-n-drop)
		Sorting no longer selects column
		Sorting maintained after navigation
		Code cleanup
		Rollover code added
		Names trimmed of extra 0x05 character which sorted wrong
		
1.4		Rec header preview
		
2.0b1	Added queuing system
		Overhaul of UI
		Removed mainmenu.nib localizations for moment
		
2.0b2	Turbo now only enabled during transfers
		Interrupted transfers now place themselves at front of queue
		Transfer times are no longer reset if interrupted
		Filtering using search bar added

2.0b3	(work in progress)
		Universal binary
		Can display more than 574 files per directory
		CRC checking of downloads and directory listings
		Some crashes have been fixed, but others remain
		Plugged several memory leaks
		Finnish translation is back
		Removed iLifeControls

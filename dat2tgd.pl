#!/usr/bin/perl -s

# Convert .dat files to tgd format
# Nigel Whitfield, 2005
# With thanks to the rt grabber code, from XMLTV

# 0.4 added purge of output folder, new channelmap.ini options
# 0.3 added -days switch
# 0.2 added -oldtgd switch
# 0.1 original version

# either specify channels on command line, or use channelmap.ini
# command line channel names use the same names as in channelmap.ini
# eg dat2tgd.pl more4 e4 sky3
# -verbose switch lets you see what's happening
# -oldtgd switch creates older TGD format files
# -days switch limits number of days
# eg dat2tgd.pl -verbose -oldtgd -days 3 more4 e4

# start by purging old files from the output folder
# override this with the -nopurge switch

if ( ! $nopurge ) { 

	@today = localtime(time) ;
	$todaysname = ($today[5] +1900)*10000 + ($today[4]+1)*100 + $today[3] ;
	
	foreach $tgd (<output/*.tgd>) {
	
		if (($tgd =~ /(2[0-9]*).tgd/) &&  ($1 < $todaysname )) {
			$verbose && print "Found stale tgd file $tgd\n" ;
			unlink($tgd) ;
		}	
	}

}

$startup = time() ;

# is the days switch given?
# $limit is the default number of days; use 0 for all available

$limit = 0 ; 

if ( $days ) {
	$limit = $ARGV[0] ;
	shift(@ARGV) ;

	$verbose && print "Limiting to $limit days\n." ;
}

if ( $limit > 0 ) {
	# Now work out when the last date required is
	($le_day, $le_month, $le_year) = &date_limit($limit) ;

	$verbose && print "Last EPG date is $le_day/$le_month/$le_year\n" ;
}

# do the same for the last extended info date, if given
# you can specify this using the -extended N option, eg
# dat2tgd.pl -extended 3

if ( $extended ) {
	$extlimit = $ARGV[0] ;
	shift(@ARGV) ;

	$verbose && print "Limiting extended information to $extlimit days\n" ;

	($lx_day, $lx_month, $lx_year) = &date_limit($extlimit) ;

	$verbose && print "Last date for extended info is $lx_day/$lx_month/$lx_year\n" ;
}


if ( $ARGV[0] eq '' ) {

#
# read channelmap.ini to find out which channels to process
# v0.4 = channels can now have options for the amount of information in each one

	open( MAP, 'channelmap.ini' ) || die("Unable to open channelmap.ini\n") ;

	while(<MAP>) {

		next if /^#/ ;

		($channel, $rtid, $chanopts) = split(/\s+/) ;

		if ( $chanopts ) {

		# process individual channel options

			foreach $chopt (split(',',$chanopts)) {
				($opt,$val) = split('=',$chopt) ;

				$channeloptions{$channel . '-' . $opt} = $val ;
			}
		}

		if ( ! -e "channels/$channel.dat" ) {
			print "Skipping $channel - no dat file\n" ;
		} else {
			push ( @channels, $channel ) ;
		}

	}
	close MAP ;
} else {

# use a list of channels from the command line

	foreach $channel ( @ARGV ) {
		if ( ! -e "channels/$channel.dat" ) {
			print "Skipping $channel - no dat file\n" ;
		} else {
			push ( @channels, $channel ) ;
		}
	}
}

# now the channels array has all the channels we're going to
# build TGD files for

$verbose && print "Building epg for @channels\n" ;

# we'll build up the EPG data in an array
# the key to the array is the date of broadcast

foreach $channel ( @channels ) {

	$verbose && print "Processing dat file for $channel\n" ;

	# check channel specific options

	if ($channeloptions{$channel . '-days'}) {
		( $l_day, $l_month, $l_year ) = &date_limit($channeloptions{$channel . '-days'}) ;
		$verbose && print "Last EPG date $is $l_day/$l_month/$l_year\n" ;
	}

	if ($channeloptions{$channel . '-ext'}) {
		( $x_day, $x_month, $x_year) = &date_limit($channeloptions{$channel . '-ext'}) ;
		$verbose && print "Limiting extended info to $x_day/$x_month/$x_year\n" ;
	}

	if ($channeloptions{$channel . '-genres'}) {
		$verbose && print "Limiting extended info to genres matching $channeloptions{$channel . '-genres'}\n" ;
	}

	if ($channeloptions{$channel . '-except'}) {
		$verbose && print "Omitting extended info for genres matching $channeloptions{$channel . '-except'}\n" ;
	}


	if ($channeloptions{$channel . '-hours'}) {
		$verbose && print "Including only programmes between $channeloptions{$channel . '-hours'}\n" ;
	}

	if ($channeloptions{$channel . '-omit'}) {
		$verbose && print "Omitting programmes between $channeloptions{$channel . '-omit'}\n" ;
	}
	
	open( DATFILE, "channels/$channel.dat" ) || die ("Can't open dat file for $channel\n" ) ;

	while(<DATFILE>) {

		# code shamelessly modelled on the RT grabber code

		s/&#8212;/--/g;
        	s/&#8230;/.../g;
        	tr/\207\211\200\224/\347\311\055\055/; 

		@epgentry = split/\~/ ;

		# a basic sanity check
		next if (@epgentry != 23 ) ;

		my ( $title, $subtitle, $episode, $yearmade, $director, $cast,
		$premier, $film, $repeat, $subtitles, $widescreen, $new,
		$signed, $bw, $rating, $cert, $genre, $description, $choice,
		$date, $starttime, $endtime, $duration ) = @epgentry ;

		# date and time checks and limits

		( $day, $month, $year ) = split('/', $date) ;
		( $sh, $sm ) = split(':', $starttime) ;
		( $eh, $em ) = split(':', $endtime) ;

		# check global limit on number of days
		if ( $limit > 0 ) {
			next if ( $year > $le_year ) ;
			next if (( $month > $le_month ) && ( $year == $le_year )) ;
			next if (( $day > $le_day ) && ( $month == $le_month )) ;
			
		}

		# check per channel limit on number of days
		if ($channeloptions{$channel . '-days'}) {
			next if ( $year > $l_year ) ;
			next if (( $month > $l_month ) && ( $year == $l_year )) ;
			next if (( $day > $l_day ) && ( $month == $l_month )) ;
		}

		# check per channel limit on times to omit or include
		if ($channeloptions{$channel . '-omit'}) {
			( $earliest, $latest ) = split(/-/, $channeloptions{$channel . '-omit'}) ;
			$begin = $sh*100 + $sm ;
			next if (( $begin >= $earliest ) && ( $begin < $latest )) ;
		}

		if ($channeloptions{$channel . '-hours'}) {
			( $earliest, $latest ) = split(/-/, $channeloptions{$channel . '-hours'}) ;
			( $latest == '0000' ) && ( $latest = '2400' ) ;

			$begin = $sh*100 + $sm ;
			next if (( $begin < $earliest ) || ( $begin >= $latest )) ;
		}


		# we're making at least a standard EPG entry
		$events++ ;

		# date and time conversions 

		if ( $eh < $sh ) {
			$eh = $eh + 24 ;
		}

		$begin = 60 * $sh + $sm ; 
		$end = 60 * $eh + $em ;

		$runtime = $end - $begin ; 

		$tgdfile = $year . $month . $day ; 

		if ( $lasttgd ne $tgdfile ) {
			$verbose && print '.' ;
			$lasttgd = $tgdfile ;
		}

		# now decide what goes where

		if ( $episode ne '' ) {
			$shortinfo = $episode ;
		} else {
			$shortinfo = $subtitle ;
		}

		if ( $shortinfo eq '' ) {
			$shortinfo = '[' . $genre . ']' ;
		} else {
			$shortinfo .= ' [' . $genre . ']' ;
		}

		if ( $genre eq 'Film' ) {
			$shortinfo .= " $director, $yearmade." ;
		}

		if ( $new eq 'true' ) { $shortinfo .= ' [New]' ;  }
		if ( $repeat eq 'true' ) { $shortinfo .= ' [Repeat]' ; }

		# now the long description

		$longinfo = $description ; 

		( $yearmade ne '' ) && ( $longinfo .= " $yearmade." ) ; 
		( $director ne '' ) && ( $longinfo .= " Directed by $director." ) ; 
		( $signed eq 'true' ) && ( $longinfo .= ' Signed.' ) ;
		( $bw eq 'true' ) && ( $longinfo .= ' Black & white.' ) ;

		# throw it away if it's not needed

		if ( $extended ) {
		
			if (( $year > $lx_year ) || 
			    (( $month > $lx_month ) && ( $year == $lx_year )) ||
			    (( $day > $lx_day ) && ( $month == $lx_month ))) { 
				$longinfo = '' ;
			}
		}

		# channel specific date check

		if (($channeloptions{$channel . '-ext'}) && ( $longinfo ne '')) {
		
			if (( $year > $x_year ) ||
			    (( $month > $x_month ) && ( $year == $x_year )) ||
			    (( $day > $x_day ) && ( $month == $x_month ))) {
				$longinfo = '' ;

			}
		}

		# channel specific genre checks

		if (($channeloptions{$channel . '-genres'}) && ($longinfo ne '')) {
			if ( $genre !~ /$channeloptions{$channel . '-genres'}/i ) {
				$longinfo = '' ;
			}

		}

		if (($channeloptions{$channel . '-except'}) && ($longinfo ne '')) {
			if ( $genre =~ /$channeloptions{$channel . '-except'}/i ) {
				$longinfo = '' ;
			}

		}

		( $longinfo ne '' ) && $extinfo++ ;

		
		# this is our TGD info line

		if ( $oldtgd ) {
			$tgd = "$channel\t$year-$month-$day $sh:$sm\t$runtime\t$title\t$shortinfo\t$longinfo\t\tN\r\n" ;
		} else {
			$tgd = "$channel\t$year/$month/$day $sh:$sm\t$runtime\t$title\t$shortinfo\t$longinfo\t\t\t\r\n" ;
		}

		$epgdata{$tgdfile} .= $tgd ;

	}

	$verbose && print "$channel complete\n" ;
}

# now all the TGD data is in memory, in the epgdata array
# we can create output files, one per day

foreach $dayfile ( keys %epgdata ) {

	$verbose && print "Generating TGD file $dayfile\n" ;

	open( TGD, "> output/$dayfile.tgd" ) || die("Can't create TGD $dayfile\n") ;

	print TGD $epgdata{$dayfile} ;
	close TGD ;
}

# report totals

$verbose && print "Created $events EPG entries, $extinfo extended information entries\n" ;

exit ;

sub date_limit {

	local ($days) = @_ ;
	local (@lastepg) ;

	@lastepg = localtime($startup+60*60*24*($days-1)) ;
	
	return ( $lastepg[3], $lastepg[4] + 1, $lastepg[5] + 1900 ) ;
}


//
//  USBFunctions.h
//  MacTF
//
//  Created by Nathan Oates on Sun Aug 01 2004.
//  Copyright (c) 2004 Nathan Oates. All rights reserved.
//

// Includes code from uproar - see below for License:

/*
 *  uproar 0.1
 *  Copyright (c) 2001 Kasima Tharnpipitchai <me@kasima.org>
 *
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef	word
typedef	unsigned short	word;
#endif
#ifndef	dword
typedef	unsigned long	dword;
#endif
#ifndef	byte
typedef unsigned char	byte;
#endif

#define	USB_CmdNone						0x00000000

#define	USB_Fail						0x00000001
#define	USB_Success						0x00000002
#define	USB_Cancel						0x00000003

#define	USB_CmdReady					0x00000100
#define	USB_CmdReset					0x00000101
#define	USB_CmdTurbo					0x00000102

// Hdd Cmd
#define	USB_CmdHddSize					0x00001000
#define	USB_DataHddSize					0x00001001

#define	USB_CmdHddDir					0x00001002
#define	USB_DataHddDir					0x00001003
#define	USB_DataHddDirEnd				0x00001004

#define	USB_CmdHddDel					0x00001005
#define	USB_CmdHddRename				0x00001006
#define	USB_CmdHddCreateDir				0x00001007

#define	USB_CmdHddFileSend				0x00001008
#define	USB_DataHddFileStart			0x00001009
#define	USB_DataHddFileData				0x0000100A
#define	USB_DataHddFileEnd				0x0000100B

// -----------------------------------------------------------------------------
// Usb Error Code
#define	USB_OK							0x00000000
#define	USB_Err_Crc						0x00000001
#define	USB_Err_UnknownCmd				0x00000002
#define	USB_Err_InvalidCmd				0x00000003
#define	USB_Err_Unknown					0x00000004
#define	USB_Err_Size					0x00000005
#define	USB_Err_RunFail					0x00000006
#define	USB_Err_Memory					0x00000007

// -----------------------------------------------------------------------------
// File send direct
#define USB_FileToDevice				0x00
#define USB_FileToHost					0x01

// -----------------------------------------------------------------------------
// File Attribute
#define	USB_FileTypeFolder				0x01
#define	USB_FileTypeNormal				0x02

// -----------------------------------------------------------------------------
#define USB_FileTypeSize				114
#define USB_FileNameSize				 95



#import <Foundation/Foundation.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <mach/mach.h>

#include <CoreFoundation/CFNumber.h>
#import "DataHandler.h"
#include <IOKit/IOKitLib.h>
#include <IOKit/IOCFPlugIn.h>
#include <IOKit/usb/IOUSBLib.h>

//#define		kBlockSize		0x08
#define		kMaxXferSize	0xFFFF

#define		kUproarSuccess		0
#define    	kUproarTrackNotFound	-1
#define    	kUproarTrackExists	-2
#define		kUproarDeviceErr	-3
#define		kUproarNotEnoughSpace	-4


typedef struct {
    IOUSBDeviceInterface197	**dev;
    IOUSBInterfaceInterface197	**intf;
} USBDeviceContext;


@interface USBFunctions : NSObject {
	NSProgressIndicator* progressBar;
	NSTextField* progressTime;
	USBDeviceContext* myContext;
}
int debug;
int rate;
int kBlockSize;
int connectedSpeed;

int initDevice(USBDeviceContext *device);

int getDeviceInfo(USBDeviceContext *device,
				  UInt8 *devInfo); 


- (void) closeDevice:(USBDeviceContext *)device;

void hexDump(UInt8 *buf, int len);

- (USBDeviceContext*) initializeUSB;
- (NSData*) getHDDSize:(USBDeviceContext*)device  ;
- (NSData*) getFileList:(USBDeviceContext*)device forPath:(NSString*) path;
- (word) findCRC:(byte*)p length:(dword) n;
- (NSData*) prepareCommand:(unsigned int)cmd withData:(NSData*) inData ;
- (int) getFile:(NSDictionary*)fileInfo forPath:(NSString*) currentPath toSaveTo:(NSString*) savePath withLoop:(BOOL)looping ;
- (int) getFile:(NSDictionary*)fileInfo forPath:(NSString*) currentPath toSaveTo:(NSString*) savePath atOffset:(unsigned long long) offset withLoop:(BOOL)looping ;

- (NSData*) sendCommand:(NSData*) fullyPackagedCommand toDevice:(USBDeviceContext*)device expectResponse:(BOOL) getResponse careForReturn:(BOOL) careFactor;
- (NSMutableData*) swap: (NSData*) inData;
- (void) turnTurboOn:(BOOL) turnOn forDevice:(USBDeviceContext*)device withReset:(BOOL)reset;
- (void) uploadFile:(NSString*) fileToUpload ofSize:(long long)size fromPath:(NSString*)curPath withAttributes:(NSData*) typeFile toDevice:(USBDeviceContext*)dev ;
- (int) getSpeed;
- (void) setDebug:(int)mode;
-(void) setRate:(int) newRate ;
-(void) setProgressBar:(NSProgressIndicator*)bar time:(NSTextField*)timeField;
-(void) updateProgress:(NSDictionary*) inDict;
- (NSString*) elapsedTime:(NSTimeInterval) totalSeconds ;
- (void) deleteFile:(NSDictionary*)fileInfo fromPath:(NSString*)currentPath onDevice:(USBDeviceContext*)device;
- (void) renameFile:(NSString*) oldName withName:(NSString*)newName atPath:(NSString*)currentPath onDevice:(USBDeviceContext*)device;
- (void) makeFolder:(NSString*)newName atPath:(NSString*)currentPath onDevice:(USBDeviceContext*)device;
- (void) checkUSB:(USBDeviceContext*)device ;

@end

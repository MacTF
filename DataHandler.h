// MacTF Copyright 2004 Nathan Oates

/* 
*
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This file was modified by Kalle Olavi Niemitalo on 2007-10-18.  */

#import <Cocoa/Cocoa.h>

@class UIElements;

@interface DataHandler : NSObject
{	
	NSMutableArray * fileList;	
	NSMutableArray * searchList;	
	IBOutlet id tableView;
	IBOutlet UIElements *UIEl;
	BOOL searchIsActive;
}

- (NSMutableArray *) fileList; 
- (NSMutableArray *) displayedList;
- (void) setFileList: (NSArray *)newFileList;
- (void) setSearchList: (NSArray *)newSearchList;
- (void) reloadTable;
- (IBAction) search:(id)sender;
// - (void) setSnapShot: (NSArray *)newFileList;
- (NSMutableDictionary*) newTFFileFromSwappedHexData:(NSData*) input;
- (NSData*) getDataFromTFFile:(NSDictionary*) infile withName:(NSString*)inputName;
- (void) convertRawDataToUseful:(NSMutableDictionary*) input ;
- (NSDictionary*) extractAttributes:(NSDictionary*)fattrs ;
- (NSDictionary*) extractDataFromRecHeader:(NSString*)file forModel:(NSString*)model;

@end

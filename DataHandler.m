// MacTF Copyright 2004 Nathan Oates

/* 
*
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This file was modified by Kalle Olavi Niemitalo on 2007-10-18.  */


#import "DataHandler.h"
#import "UIElements.h"

@implementation DataHandler

#pragma mark -
#pragma mark Startup and Shutdown

- (id) init
{
    if (self = [super init])
    {
        fileList = [[NSMutableArray alloc] init];
		searchList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) dealloc
{
    [fileList release];
    [searchList release];
    [super dealloc];
}



#pragma mark -
#pragma mark Simple Accessors

- (NSMutableArray *) fileList
{
    return fileList;
}

- (NSMutableArray *) displayedList
{
    if (searchIsActive) return searchList;
	else return fileList;
}

- (void) setFileList: (NSArray *)newFileList
{
    if (fileList != newFileList)
    {
        [fileList autorelease];
        fileList = [[NSMutableArray alloc] initWithArray: newFileList];
    }
}

- (void) setSearchList: (NSArray *)newSearchList
{
    if (searchList != newSearchList)
    {
        [searchList autorelease];
        searchList = [[NSMutableArray alloc] initWithArray: newSearchList];
    }
}

-(void)awakeFromNib
{
	searchIsActive = NO;
}



-(id)tableView:(NSTableView *)aTableView
objectValueForTableColumn:(NSTableColumn *)aTableColumn
           row:(int)rowIndex
{
    id theRecord, theValue;
	// FIXME: Because DataHandler is used simultaneously from two threads,
	// it sometimes happens that the NSMutableArray has already been emptied
	// even though it was not empty when numberOfRowsInTableView was called.
	// As a workaround, catch the resulting NSRangeException.
	// This is not really safe and should be fixed by making the TFUSBController
	// send results to the UI thread, which would then update the table data
	// at its leisure.
	@try {
		if (searchIsActive)	theRecord = [searchList objectAtIndex:rowIndex];
		else theRecord = [fileList objectAtIndex:rowIndex];
		theValue = [theRecord objectForKey:[aTableColumn identifier]];
	} @catch (NSException* exception) {
		NSLog(@"DataHandler hit an exception: %@", exception);
		if ([[exception name] isEqualToString:NSRangeException])
			theValue = nil;
		else
			@throw;
	}
    return theValue;
}

// just returns the number of items we have.
- (int)numberOfRowsInTableView:(NSTableView *)aTableView
{
	if (searchIsActive) return [searchList count];
    else return [fileList count];
}

- (void) reloadTable {
    [tableView reloadData];
}

- (IBAction) search:(id)sender {
	NSString *searchString = [sender stringValue];
	if ([searchString isEqualToString:@""]) {
		searchIsActive = NO;
		[self reloadTable];
	} else {
		NSArray *searchTerms = [searchString componentsSeparatedByString:@" "];
		NSEnumerator *enumerator = [searchTerms objectEnumerator];
		id anObject;
		BOOL firstToken = YES;
		NSArray *results = [[NSArray alloc] init];
		while (anObject = [enumerator nextObject]) {
			if (![anObject isEqualTo:@""]) {
				NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", anObject];
				if (firstToken) 
					results = [fileList filteredArrayUsingPredicate:pred];
				else
					results = [results filteredArrayUsingPredicate:pred];
				firstToken = NO;
			}
		}
 		[self setSearchList:results];
		searchIsActive = YES; 
		[self reloadTable];
	}
}


// Drag n Drop methods
/*- (BOOL)tableView:(NSTableView *)aTableView writeRows:(NSArray*)rows toPasteboard:(NSPasteboard*)pboard {
     id list;
	if (searchIsActive) 
		list = searchList;
	else
		list = seqs;
	NSMutableArray *tempArray = [NSMutableArray array];
    movedRows = [[NSMutableArray alloc] init];
    NSNumber *index;
    id tempObject;
    NSEnumerator *enumerator = [rows objectEnumerator];
    while ( (index = [enumerator nextObject]) ) {
        tempObject = [list objectAtIndex:[index intValue]];
        [tempArray addObject:tempObject];
    }
    movedRows = tempArray;
    NSData *data = [NSArchiver archivedDataWithRootObject: tempArray];
    [pboard declareTypes: [NSArray arrayWithObject: @"NSGeneralPboardType"] owner: nil];
    [pboard setData: data forType: @"NSGeneralPboardType"];
    return YES;
}
*/
- (BOOL)tableView:(NSTableView*)aTableView acceptDrop:(id <NSDraggingInfo>)info row:(int)row dropOperation:(NSTableViewDropOperation)operation {
  	NSPasteboard *pboard = [info draggingPasteboard];
    NSArray *types = [NSArray arrayWithObjects:@"NSFilenamesPboardType", nil];
	NSString *desiredType = [pboard availableTypeFromArray:types];
	if (nil == [info draggingSource]) // From other application
    {
		NSData *carriedData = [pboard dataForType:desiredType];
		if (nil == carriedData)
		{
			//the operation failed for some reason
			NSRunAlertPanel(@"Paste Error", @"Sorry, but the paste operation failed", nil, nil, nil);
			return NO;
		} else {
			if ([desiredType isEqualToString:NSFilenamesPboardType]) {
				//we have a list of file names in an NSData object
				NSArray *fileArray = [pboard propertyListForType:@"NSFilenamesPboardType"]; //be caseful since this method returns id. We just happen to know that it will be an array.
				NSEnumerator* selected = [fileArray objectEnumerator];
				id currentSelected;
				while (currentSelected = [selected nextObject]) {
					NSLog(@"Upload:%@ to %@", currentSelected, [NSString stringWithString:[UIEl currentPath]]);
					[UIEl uploadPath:currentSelected toPath:[NSString stringWithString:[UIEl currentPath]]];
				}
			} else {
				//this can't happen
				NSAssert(NO, @"This can't happen");
				return NO;
			}
			return YES;
		}
    } else if (aTableView == [info draggingSource]) { // From self
        return YES;        
    }
    // From other documents
	return YES;    
}

    - (NSDragOperation) tableView: (NSTableView *) view
validateDrop: (id <NSDraggingInfo>) info
proposedRow: (int) row
proposedDropOperation: (NSTableViewDropOperation) operation
    {
	
	if (nil == [info draggingSource]) // From other application
    {
		[view setDropRow: row dropOperation: NSTableViewDropAbove];
        return NSDragOperationCopy;
    }
    else if (tableView == [info draggingSource]) // From self
    {
         return NSDragOperationNone;
    }
	return NSDragOperationNone;
}

// Delegate methods for tableview 

- (NSMutableDictionary*) newTFFileFromSwappedHexData:(NSData*) input {
	
	NSData* mjdHex = [input subdataWithRange:(NSRange) {0,2}];
	NSData* hrHex = [input subdataWithRange:(NSRange) {2,1}];
	NSData* minHex = [input subdataWithRange:(NSRange) {3,1}];
	NSData* secHex = [input subdataWithRange:(NSRange) {4,1}];
	NSData* typeHex = [input subdataWithRange:(NSRange) {5,1}];
	NSData* sizeMSBHex = [input subdataWithRange:(NSRange) {6,4}];
	NSData* sizeLSBHex = [input subdataWithRange:(NSRange) {10,4}];
	NSData* nameHex = [input subdataWithRange:(NSRange) {14,95}];
	// windows attributes here?
	
	unsigned char c;
	UInt16 u16_bigendian;
	SInt32 s32_bigendian;
	NSMutableDictionary* tfFile = [[NSMutableDictionary alloc] init];
	[mjdHex getBytes:&u16_bigendian];
	[tfFile setObject:[NSNumber numberWithInt:EndianU16_BtoN(u16_bigendian)] forKey:@"rawMJD"];
	[hrHex getBytes:&c];
	[tfFile setObject:[NSNumber numberWithChar:c] forKey:@"hour"];
	[minHex getBytes:&c];
	[tfFile setObject:[NSNumber numberWithChar:c] forKey:@"min"];
	[secHex getBytes:&c];
	[tfFile setObject:[NSNumber numberWithChar:c] forKey:@"sec"];
	[typeHex getBytes:&c];
	[tfFile setObject:[NSNumber numberWithChar:c] forKey:@"type"];
	[sizeMSBHex getBytes:&s32_bigendian];
	[tfFile setObject:[NSNumber numberWithLong:EndianS32_BtoN(s32_bigendian)] forKey:@"sizeMSB"];
	[sizeLSBHex getBytes:&s32_bigendian];
	[tfFile setObject:[NSNumber numberWithLong:EndianS32_BtoN(s32_bigendian)] forKey:@"sizeLSB"];
	NSString* name = [[[NSString alloc] initWithData:nameHex encoding:NSISOLatin1StringEncoding] autorelease];
	name = [name substringToIndex:[name rangeOfCharacterFromSet:[NSCharacterSet characterSetWithRange:(NSRange){0,1}]].location]; //trim anything past the 0x00
	name = [name stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithRange:(NSRange){5,1}]]; //remove extraneous 0x05 on some peoples names.
	[tfFile setObject:name forKey:@"name"];
//	NSLog([tfFile description]);
	return tfFile;
}

- (NSDictionary*) extractAttributes:(NSDictionary*)fattrs {
	NSMutableDictionary* tfFile = [[NSMutableDictionary alloc] init];
	//dates
	NSCalendarDate* date = [[NSCalendarDate alloc] initWithTimeInterval:0 sinceDate:[fattrs fileModificationDate]];
	int min = [date minuteOfHour];
	int sec = [date secondOfMinute];
	int hour = [date hourOfDay];
	NSInteger MJDdate;
	NSCalendarDate *mjdref = [[[NSCalendarDate alloc] initWithYear:1858 month:11 day:17 hour:0 minute:0 second:0 timeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]] autorelease];
	[date years:NULL months:NULL days:&MJDdate hours:NULL minutes:NULL seconds:NULL sinceDate:mjdref];
	[tfFile setObject:[NSNumber numberWithInt:MJDdate] forKey:@"rawMJD"];
	[tfFile setObject:[NSNumber numberWithInt:hour] forKey:@"hour"];
	[tfFile setObject:[NSNumber numberWithInt:min] forKey:@"min"];
	[tfFile setObject:[NSNumber numberWithInt:sec] forKey:@"sec"];
	
	//types
	NSString* type = [fattrs fileType];
	if ([type isEqualToString:@"NSFileTypeDirectory"]) 
		[tfFile setObject:[NSNumber numberWithInt:1] forKey:@"type"];
	else
		[tfFile setObject:[NSNumber numberWithInt:2] forKey:@"type"];
	
	//sizes
	long long size = [fattrs fileSize];
	[tfFile setObject:[NSNumber numberWithLong:size/0xFFFFFFFF] forKey:@"sizeMSB"];
	[tfFile setObject:[NSNumber numberWithLong:size%0xFFFFFFFF] forKey:@"sizeLSB"];
	return tfFile;
}

-(NSData*) getDataFromTFFile:(NSDictionary*) infile withName:(NSString*) inputName {
	UInt16 u16_bigendian = EndianU16_NtoB([[infile objectForKey:@"rawMJD"] shortValue]);
	NSMutableData* build = [NSMutableData dataWithBytes:&u16_bigendian length:2];
	char c = [[infile objectForKey:@"hour"]charValue];
	[build appendData:[NSData dataWithBytes:&c length:1]];
	c = [[infile objectForKey:@"min"] charValue];
	[build appendData:[NSData dataWithBytes:&c length:1]];
	c = [[infile objectForKey:@"sec"] charValue];
	[build appendData:[NSData dataWithBytes:&c length:1]];
	c = [[infile objectForKey:@"type"] charValue];
	[build appendData:[NSData dataWithBytes:&c length:1]];
	SInt32 s32_bigendian = EndianS32_NtoB([[infile objectForKey:@"sizeMSB"] longValue]);
	[build appendData:[NSData dataWithBytes:&s32_bigendian length:4]];
	s32_bigendian = EndianS32_NtoB([[infile objectForKey:@"sizeLSB"] longValue]);
	[build appendData:[NSData dataWithBytes:&s32_bigendian length:4]];
	NSData * name = [inputName dataUsingEncoding:NSISOLatin1StringEncoding];
	[build appendData:name];
	if ([name length] < 95)
		[build increaseLengthBy:95-[name length]];
	// other file stuff (5 bytes) - maybe add this later?
	[build increaseLengthBy:5];
	return build;
}

- (void) convertRawDataToUseful:(NSMutableDictionary*) input {
	
	//turn MJD and time into a useful object
	if ([[input objectForKey:@"type"] intValue] == 1) { //folder
		[input setObject:@"" forKey:@"date"];
		[input setObject:[NSDate distantPast] forKey:@"sortdate"];
		[input setObject:[NSImage imageNamed:@"folder.tif"] forKey:@"icon"];
		if ([[input objectForKey:@"name"] isEqualToString:@".."]) 
			[input setObject:@".. (Parent folder)" forKey:@"name"];
		[input setObject:@"0000" forKey:@"suffix"];
	} else {
		if ([[input objectForKey:@"name"] hasSuffix:@".tgd"]) [input setObject:[NSImage imageNamed:@"tgd.tif"] forKey:@"icon"];
		else if ([[input objectForKey:@"name"] hasSuffix:@".tap"]) [input setObject:[NSImage imageNamed:@"package.tif"] forKey:@"icon"];
		else if ([[input objectForKey:@"name"] hasSuffix:@".mp3"]) [input setObject:[NSImage imageNamed:@"mp3.tif"] forKey:@"icon"];
		else if ([[input objectForKey:@"name"] hasSuffix:@".txt"] || [[input objectForKey:@"name"] hasSuffix:@".ini"]) [input setObject:[NSImage imageNamed:@"txt.tif"] forKey:@"icon"];
		else [input setObject:[NSImage imageNamed:@"file.tif"] forKey:@"icon"];
		[input setObject:[[input objectForKey:@"name"] pathExtension] forKey:@"suffix"];
		
		int daysSinceRef = [[input objectForKey:@"rawMJD"] intValue] - 51910; // ref date is 1/1/2001 51911??
		float secsSinceRef = (((daysSinceRef * 24 + [[input objectForKey:@"hour"] intValue])* 60 + [[input objectForKey:@"min"] intValue]) * 60); 
		NSCalendarDate* tempDate = [NSCalendarDate dateWithTimeIntervalSinceReferenceDate:secsSinceRef];
		//now adjust for timezone
		int offset = [[NSTimeZone defaultTimeZone] secondsFromGMTForDate:tempDate];
		//NSLog(@"TZ offset:%i td:%@", offset, [tempDate description]);
		NSCalendarDate* finalDate = [tempDate dateByAddingYears:0 months:0 days:0 hours:0 minutes:0 seconds:-1*offset]; 
		//NSLog(@"TZ td:%@", [finalDate description]);
		[input setObject:finalDate forKey:@"date"];
		[input setObject:finalDate forKey:@"sortdate"];
	}

	//turn size fields into a proper number
	double size;
	double big =  pow( 256., 4. );
	double lsb = [[input objectForKey:@"sizeLSB"] doubleValue];
	double msb = [[input objectForKey:@"sizeMSB"] doubleValue];
	if (lsb < 0) 
		lsb = lsb + pow( 256., 4. ); //rollover
	if (msb < 0) 
		msb = msb + pow( 256., 4. ); //rollover
	size = msb * big + lsb;
	[input setObject:[NSNumber numberWithDouble:size] forKey:@"fileSize"];
	
	NSString* ret = nil;
	if( size == 0. ) ret = NSLocalizedString( @" - ", "no file size" );
	else if( size > 0. && size < 1024. ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.0f B", "file size measured in bytes" ), size];
	else if( size >= 1024. && size < pow( 1024., 2. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.1f KB", "file size measured in kilobytes" ), ( size / 1024. )];
	else if( size >= pow( 1024., 2. ) && size < pow( 1024., 3. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.2f MB", "file size measured in megabytes" ), ( size / pow( 1024., 2. ) )];
	else if( size >= pow( 1024., 3. ) && size < pow( 1024., 4. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.2f GB", "file size measured in gigabytes" ), ( size / pow( 1024., 3. ) )];
	else if( size >= pow( 1024., 4. ) ) ret = [NSString stringWithFormat:NSLocalizedString( @"%.4f TB", "file size measured in terabytes" ), ( size / pow( 1024., 4. ) )];
	else ret = NSLocalizedString( @" ? ", "unknown file size" );
	[input setObject:ret forKey:@"size"];
}

- (NSDictionary*) extractDataFromRecHeader:(NSString*)file forModel:(NSString*)model {
	NSMutableDictionary* extracts = [NSMutableDictionary dictionaryWithCapacity:8];
	NSData* fileData = [NSData dataWithContentsOfFile:file];
	
	int modelOffset = 0;
	if ([model isEqualToString:@"TF5800"]) modelOffset = 4;
	unsigned char c; //1byte
	UInt16 u16_bigendian;
	[[fileData subdataWithRange:(NSRange) {8,2}] getBytes:&u16_bigendian];
	[extracts setObject:[NSNumber numberWithShort:EndianU16_BtoN(u16_bigendian)] forKey:@"duration"];
	NSString* channel = [[NSString alloc] initWithData:[fileData subdataWithRange:(NSRange) {28,24}] encoding:NSISOLatin1StringEncoding];
	NSRange r = [channel rangeOfCharacterFromSet:[NSCharacterSet characterSetWithRange:(NSRange){0,1}]]; //trim anything past the 0x00
	if (r.location != NSNotFound)
		channel = [channel substringToIndex:r.location];	
	channel = [channel stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithRange:(NSRange){5,1}]]; //remove extraneous 0x05 on some peoples names.
	[extracts setObject:channel forKey:@"channel"];
	[[fileData subdataWithRange:(NSRange) {76+modelOffset,2}] getBytes:&u16_bigendian];
	[extracts setObject:[NSNumber numberWithInt:EndianU16_BtoN(u16_bigendian)] forKey:@"rawMJD"];
	int daysSinceRef = [[extracts objectForKey:@"rawMJD"] intValue] - 51910; // ref date is 1/1/2001 51911??
	[[fileData subdataWithRange:(NSRange) {78+modelOffset,1}] getBytes:&c];
	NSNumber* hour = [NSNumber numberWithChar:c];
	[[fileData subdataWithRange:(NSRange) {79+modelOffset,1}] getBytes:&c];
	NSNumber* min = [NSNumber numberWithChar:c];
	float secsSinceRef = (((daysSinceRef * 24 + [hour intValue])* 60 + [min intValue]) * 60); 
	NSCalendarDate* finalDate = [NSCalendarDate dateWithTimeIntervalSinceReferenceDate:secsSinceRef];
	[extracts setObject:finalDate forKey:@"startTime"];
	[[fileData subdataWithRange:(NSRange) {85+modelOffset,1}] getBytes:&c];
	NSString* name = [[NSString alloc] initWithData:[fileData subdataWithRange:(NSRange) {87+modelOffset,c}] encoding:NSISOLatin1StringEncoding];
	[extracts setObject:name forKey:@"name"];
	NSString* desc = [[NSString alloc] initWithData:[fileData subdataWithRange:(NSRange) {87+c+modelOffset,255}] encoding:NSISOLatin1StringEncoding];
	r = [desc rangeOfCharacterFromSet:[NSCharacterSet characterSetWithRange:(NSRange){0,1}]]; //trim anything past the 0x00
	if (r.location != NSNotFound)
		desc = [desc substringToIndex:r.location];
	desc = [desc stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithRange:(NSRange){5,1}]]; //remove extraneous 0x05 on some peoples names.
	[extracts setObject:desc forKey:@"description"];
	[[fileData subdataWithRange:(NSRange) {362+modelOffset,2}] getBytes:&u16_bigendian];
	NSString* extInfo = [[NSString alloc] initWithData:[fileData subdataWithRange:(NSRange) {369+modelOffset,EndianU16_BtoN(u16_bigendian)}] encoding:NSISOLatin1StringEncoding];
	[extracts setObject:extInfo forKey:@"extInfo"];
	//NSData* movieData = [fileData subdataWithRange:(NSRange) {1656,([fileData length]-1656)}];
	//[movieData writeToFile:@"/Volumes/Tyr/cazlar/testMovie.ts" atomically:YES];
	//NSLog([extracts description]);
	return extracts;
}



@end

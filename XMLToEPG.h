//
//  XMLToEPG.h
//  MacTF
//
//  Created by Nathan Oates on Thu Dec 16 2004.
//  Copyright (c) 2004 Nathan Oates. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <XMLTree.h>


@interface XMLToEPG : NSObject {	
	
}

- (NSMutableArray*) importXML: (NSURL*) xmlFile ;
- (void) fixData: (NSMutableArray*) rawData;
- (NSCalendarDate*) stringToDate:(NSString*) inString;
- (void) exportFixedXML:(NSMutableArray*) fixedData toFile:(NSString*) path ;

@end

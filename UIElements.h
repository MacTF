// MacTF Copyright 2004 Nathan Oates

/* 
*
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* This file was modified by Kalle Olavi Niemitalo on 2007-10-18.  */

#import <Cocoa/Cocoa.h>
#import "DataHandler.h"
#import "TFUSBController.h"

@interface UIElements : NSObject
{
    IBOutlet id availSpaceField;
    IBOutlet id freeSpaceField;
	IBOutlet DataHandler* dh; //move
	IBOutlet id tableView;// and this?
	IBOutlet id statusField;
	IBOutlet id progressBar;
	IBOutlet id connectButton;
	IBOutlet id connectLight;
	IBOutlet id turboCB;
	IBOutlet NSTextField* versionField;
	IBOutlet NSWindow* mainWindow;
	IBOutlet id prefsWindow;
	IBOutlet id renameWindow;
	IBOutlet id newFolderWindow;
	IBOutlet id debugCB;
	IBOutlet id altRateSlider;
	IBOutlet id autoCB;
	IBOutlet id preview;
	IBOutlet id previewDrawer;
	IBOutlet id progressTime;
	IBOutlet id renameOld;
	IBOutlet id renameNew;
	IBOutlet id newFolderNew;
	IBOutlet id downloadMenu;
	IBOutlet id uploadMenu;
	IBOutlet id renameMenu;
	IBOutlet id deleteMenu;
	IBOutlet id currentlyField;
	IBOutlet id recName;
	IBOutlet id recDuration;
	IBOutlet id recDescription;
	IBOutlet id recExtInfo;
	IBOutlet id recStart;
	IBOutlet id recChannel;
	IBOutlet id drawerTabView;
	IBOutlet id pathBar;
	IBOutlet id pauseButton;
	IBOutlet id searchField;
	NSTableColumn* selectedColumn;
	BOOL connected;
	BOOL paused;
	TFUSBController* tfusbc;
	USBDeviceContext* context;
	NSString* currentPath;
	NSUserDefaults *prefs;
	BOOL sortAscending;
	NSString *highlightImageData;
	NSImage *highlightImage;
}
- (BOOL)validateMenuItem:(NSMenuItem*)anItem ;
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification;
- (void) setAvailableSpace:(NSData*) input ;
- (void) setFreeSpace: (NSData*) input ;
- (NSString*) prepForSizeDisplay:(NSData*) input;
- (IBAction) connect: (id) sender;
//- (IBAction) tempGoToPath: (id) sender;
- (int) goToPath:(NSString*) path;
- (void)doubleClick:(id)sender;
- (void) downloadFileDoubleClickThread:(NSDictionary*)fileInfo;
//- (void) updateHDDSize ;
- (NSString*) currentPath;
//- (IBAction) snapshot:(id) sender;
- (IBAction) downloadSelectedFile:(id)sender;
- (IBAction) uploadFile: (id) sender;
- (void) uploadPath:(NSString*) path toPath:(NSString*) toPath;
- (IBAction) toggleTurbo:(id)sender;
- (IBAction) toggleTurboCB:(id)sender;
- (IBAction) togglePreview:(id)sender;
//- (IBAction) epg:(id) sender;
- (void) finishTransfer;
- (IBAction)deleteFile:(id)sender;
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo;
- (IBAction)openPrefsSheet:(id)sender;
- (IBAction) mySheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo ;
- (IBAction)closePrefsSheet:(id)sender;
- (IBAction)openRenameSheet:(id)sender;
- (IBAction)closeRenameSheet:(id)sender;
- (IBAction)cancelRename:(id)sender;
- (IBAction)openNewFolderSheet:(id)sender;
- (IBAction)closeNewFolderSheet:(id)sender;
- (IBAction)cancelNewFolder:(id)sender;
- (IBAction)pathBarClick:(id)sender;
- (IBAction)pauseCurrentTransfer:(id)sender;
- (id) getTfusbc;
- (USBDeviceContext*) getContext;
- (NSNumber*) isConnected;
- (id) selectedColumn;
- (id) currentlyField;
- (id) connectLight;

//- (NSArray*) snapshotOfPath:(NSString*)path ;

@end

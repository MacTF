//
//  TFDataFormat.h
//  MacTF
//
//  Created by Kalle Olavi Niemitalo on 2007-10-23, based on:
//    TFUSBController.m
//    MacTF
//    Created by Nathan Oates on Sun Aug 01 2004.
//    Copyright (c) 2004-7 Nathan Oates nathan@noates.com All rights reserved.
//  May also include parts of:
//    uproar 0.1
//    Copyright (c) 2001 Kasima Tharnpipitchai <me@kasima.org>
//
// This source code is free software; you can redistribute it and/or
// modify it under the terms of the GNU Public License as published
// by the Free Software Foundation; either version 2 of the License,
// or (at your option) any later version.
//
// This source code is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// Please refer to the GNU Public License for more details.
//
// You should have received a copy of the GNU Public License along with
// this source code; if not, write to:
// Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#import "TFDataFormat.h"
#import "Errors.h"

@implementation TFDataFormat

// ---------------------------------------------------------------------------
#pragma mark Framing of communication blocks
// ---------------------------------------------------------------------------

- (UInt16) findCRC:(const UInt8*)p length:(size_t)n
{
	UInt16 m_crc16 = 0x0000;
	static const UInt16 crc16Tbl[256] = {
		0x0000, 0xc0c1, 0xc181, 0x0140, 0xc301, 0x03c0, 0x0280, 0xc241,
		0xc601, 0x06c0, 0x0780, 0xc741, 0x0500, 0xc5c1, 0xc481, 0x0440,
		0xcc01, 0x0cc0, 0x0d80, 0xcd41, 0x0f00, 0xcfc1, 0xce81, 0x0e40,
		0x0a00, 0xcac1, 0xcb81, 0x0b40, 0xc901, 0x09c0, 0x0880, 0xc841,
		0xd801, 0x18c0, 0x1980, 0xd941, 0x1b00, 0xdbc1, 0xda81, 0x1a40,
		0x1e00, 0xdec1, 0xdf81, 0x1f40, 0xdd01, 0x1dc0, 0x1c80, 0xdc41,
		0x1400, 0xd4c1, 0xd581, 0x1540, 0xd701, 0x17c0, 0x1680, 0xd641,
		0xd201, 0x12c0, 0x1380, 0xd341, 0x1100, 0xd1c1, 0xd081, 0x1040,
		0xf001, 0x30c0, 0x3180, 0xf141, 0x3300, 0xf3c1, 0xf281, 0x3240,
		0x3600, 0xf6c1, 0xf781, 0x3740, 0xf501, 0x35c0, 0x3480, 0xf441,
		0x3c00, 0xfcc1, 0xfd81, 0x3d40, 0xff01, 0x3fc0, 0x3e80, 0xfe41,
		0xfa01, 0x3ac0, 0x3b80, 0xfb41, 0x3900, 0xf9c1, 0xf881, 0x3840,
		0x2800, 0xe8c1, 0xe981, 0x2940, 0xeb01, 0x2bc0, 0x2a80, 0xea41,
		0xee01, 0x2ec0, 0x2f80, 0xef41, 0x2d00, 0xedc1, 0xec81, 0x2c40,
		0xe401, 0x24c0, 0x2580, 0xe541, 0x2700, 0xe7c1, 0xe681, 0x2640,
		0x2200, 0xe2c1, 0xe381, 0x2340, 0xe101, 0x21c0, 0x2080, 0xe041,
		0xa001, 0x60c0, 0x6180, 0xa141, 0x6300, 0xa3c1, 0xa281, 0x6240,
		0x6600, 0xa6c1, 0xa781, 0x6740, 0xa501, 0x65c0, 0x6480, 0xa441,
		0x6c00, 0xacc1, 0xad81, 0x6d40, 0xaf01, 0x6fc0, 0x6e80, 0xae41,
		0xaa01, 0x6ac0, 0x6b80, 0xab41, 0x6900, 0xa9c1, 0xa881, 0x6840,
		0x7800, 0xb8c1, 0xb981, 0x7940, 0xbb01, 0x7bc0, 0x7a80, 0xba41,
		0xbe01, 0x7ec0, 0x7f80, 0xbf41, 0x7d00, 0xbdc1, 0xbc81, 0x7c40,
		0xb401, 0x74c0, 0x7580, 0xb541, 0x7700, 0xb7c1, 0xb681, 0x7640,
		0x7200, 0xb2c1, 0xb381, 0x7340, 0xb101, 0x71c0, 0x7080, 0xb041,
		0x5000, 0x90c1, 0x9181, 0x5140, 0x9301, 0x53c0, 0x5280, 0x9241,
		0x9601, 0x56c0, 0x5780, 0x9741, 0x5500, 0x95c1, 0x9481, 0x5440,
		0x9c01, 0x5cc0, 0x5d80, 0x9d41, 0x5f00, 0x9fc1, 0x9e81, 0x5e40,
		0x5a00, 0x9ac1, 0x9b81, 0x5b40, 0x9901, 0x59c0, 0x5880, 0x9841,
		0x8801, 0x48c0, 0x4980, 0x8941, 0x4b00, 0x8bc1, 0x8a81, 0x4a40,
		0x4e00, 0x8ec1, 0x8f81, 0x4f40, 0x8d01, 0x4dc0, 0x4c80, 0x8c41,
		0x4400, 0x84c1, 0x8581, 0x4540, 0x8701, 0x47c0, 0x4680, 0x8641,
		0x8201, 0x42c0, 0x4380, 0x8341, 0x4100, 0x81c1, 0x8081, 0x4040,
	};
	size_t i;	
	for(i = 0; i < n; i++)
			m_crc16 = (m_crc16 >> 8) ^ crc16Tbl[*p++ ^ (m_crc16 & 0xFF)];
	return m_crc16;
}

- (NSMutableData*) swap:(NSData*)inData {
	size_t len = [inData length];
	size_t uneven = 0;
	if (len % 2 != 0) {
		uneven = 1; // prepare space for padding
	}
	NSMutableData* outData = [NSMutableData dataWithLength:len+uneven];
	swab([inData bytes], [outData mutableBytes], len-uneven);
	if (uneven) {
		// swab() requires an even length.  The last byte was not given to
		// swab() above, so copy it separately.  Also clear the byte just
		// before the last one, even though the dataWithLength: class method
		// should have already cleared it.
		unsigned char final[2] = { 0, 0 };
		[inData getBytes:&final[1] range:(NSRange){len-1,1}];
		[outData replaceBytesInRange:(NSRange){len-1,2} withBytes:final];
	}
	return outData;
}

- (NSData*) prepareCommand:(TopfieldUSBCmd)cmd withData:(NSData*)inData {
	NSMutableData* block = [NSMutableData dataWithLength:8];
	if (inData != nil)
		[block appendData:inData];
	size_t blockLength = [block length];
	if (blockLength > 0xFFFF)
		return nil; // wouldn't fit in UInt16

	UInt32 cmd_bigendian = EndianU32_NtoB(cmd);
	[block replaceBytesInRange:(NSRange){4,4} withBytes:&cmd_bigendian];

	UInt16 crc = [self findCRC:(const UInt8*)[block bytes]+4 length:blockLength-4];
	UInt16 crc_bigendian = EndianU16_NtoB(crc);
	[block replaceBytesInRange:(NSRange){2,2} withBytes:&crc_bigendian];

	UInt16 blockLength_bigendian = EndianU16_NtoB(blockLength);
	[block replaceBytesInRange:(NSRange){0,2} withBytes:&blockLength_bigendian];

	return [self swap:block];
}

// Check the size and CRC of a received communication block.
- (BOOL) isCommunicationBlockValid:(NSData*)block
	error:(NSError**)errorOut ecode:(TopfieldUSBEcode*)ecodeOut
{
	NSError* error = nil;
	TopfieldUSBEcode ecode = USB_OK;

	if (block == nil) {
		NSString* localizedDescription = NSLocalizedStringWithDefaultValue(
			@"ERR_COMMUNICATION_BLOCK_MISSING",
			nil, [NSBundle mainBundle],
			@"The PVR did not respond",
			@"A communication block was expected but not received");
		error = [NSError errorWithDomain:MacTFErrorDomain
			code:MACTF_ERR_COMMUNICATION_BLOCK_MISSING
			userInfo:[NSDictionary dictionaryWithObject:localizedDescription
				forKey:NSLocalizedDescriptionKey]];
		ecode = USB_Err_RunFail;
		goto ret;
	}

	size_t length = [block length];
	if (length < 8) {
		NSString* localizedDescription = NSLocalizedStringWithDefaultValue(
			@"ERR_COMMUNICATION_BLOCK_LT_8_BYTES",
			nil, [NSBundle mainBundle],
			@"Received only %u bytes; eight are required",
			@"Topfield's USB protocol requires an 8-byte header in every"
			 " communication block but the received block was too short"
			 " for that");
		localizedDescription = [NSString stringWithFormat:localizedDescription,
			(unsigned) length];
		error = [NSError errorWithDomain:MacTFErrorDomain
			code:MACTF_ERR_COMMUNICATION_BLOCK_LT_8_BYTES
			userInfo:[NSDictionary dictionaryWithObject:localizedDescription
				forKey:NSLocalizedDescriptionKey]];
		ecode = USB_Err_Size;
		goto ret;
	}

	UInt16 crc16_bigendian;
	[block getBytes:&crc16_bigendian range:(NSRange){2,2}];
	UInt16 crc = EndianU16_BtoN(crc16_bigendian);
	UInt16 calcCrc = [self findCRC:(const UInt8*)[block bytes]+4
		length:length-4];
	if (crc != calcCrc) {
		NSString* localizedDescription = NSLocalizedStringWithDefaultValue(
			@"ERR_COMMUNICATION_BLOCK_CRC",
			nil, [NSBundle mainBundle],
			@"CRC mismatch in received data.  The header claims %#.4x but"
			 " the data computes to %#.4x.",
			@"CRC mismatch in a received communication block");
		localizedDescription = [NSString stringWithFormat:localizedDescription,
			(unsigned) crc, (unsigned) calcCrc];
		error = [NSError errorWithDomain:MacTFErrorDomain
					code:MACTF_ERR_COMMUNICATION_BLOCK_CRC
					userInfo:[NSDictionary dictionaryWithObject:localizedDescription
						forKey:NSLocalizedDescriptionKey]];
		ecode = USB_Err_Crc;
		goto ret;
	}

ret:
	if (ecodeOut != NULL)
		*ecodeOut = ecode;
	if (errorOut != NULL)
		*errorOut = error;
	return error == nil;
}

// Get the cmd field from a communication block.  The caller must already have
// checked the length of the block.
- (TopfieldUSBCmd) cmdFromCommunicationBlock:(NSData*)block {
	UInt32 cmd_bigendian;
	[block getBytes:&cmd_bigendian range:(NSRange){4,4}];
	return EndianU32_BtoN(cmd_bigendian);
}

// ---------------------------------------------------------------------------
#pragma mark Common ingredients
// ---------------------------------------------------------------------------

- (BOOL) appendToData:(NSMutableData*)data fname:(NSString*)fname {
	NSData* fnameData = [fname dataUsingEncoding:NSISOLatin1StringEncoding];
	if (fnameData == nil) // fname contains characters unsupported by encoding
		return NO;
	const size_t terminatorSize = 1;
	[data appendData:fnameData];
	[data increaseLengthBy:terminatorSize]; // fills with 0x00
	return YES;
}

- (BOOL) appendToData:(NSMutableData*)data sizeAndFname:(NSString*)fname {
	NSData* fnameData = [fname dataUsingEncoding:NSISOLatin1StringEncoding];
	if (fnameData == nil) // fname contains characters unsupported by encoding
		return NO;
	const size_t terminatorSize = 1;
	const size_t nameSize = [fnameData length] + terminatorSize;
	if (nameSize > 0xFFFF) // size would not fit in UInt16
		return NO;
	const UInt16 nameSize_bigendian = EndianU16_NtoB(nameSize);
	[data appendBytes:&nameSize_bigendian length:2];
	[data appendData:fnameData];
	[data increaseLengthBy:terminatorSize]; // fills with 0x00
	return YES;
}

- (NSString*) fnameForFile:(NSString*)file atPath:(NSString*)path {
	return [NSString stringWithFormat:@"%@\\%@", path, file];
}

// Convert a Topfield-defined command code to a string for logging.
// The string is not localized.
- (NSString*) nameOfCmd:(TopfieldUSBCmd)cmd {
	switch (cmd) {
		case USB_CmdNone: return @"USB_CmdNone";
		case USB_Fail: return @"USB_Fail";
		case USB_Success: return @"USB_Success";
		case USB_Cancel: return @"USB_Cancel";
		case USB_CmdReady: return @"USB_CmdReady";
		case USB_CmdReset: return @"USB_CmdReset";
		case USB_CmdTurbo: return @"USB_CmdTurbo";
		case USB_CmdHddSize: return @"USB_CmdHddSize";
		case USB_DataHddSize: return @"USB_DataHddSize";
		case USB_CmdHddDir: return @"USB_CmdHddDir";
		case USB_DataHddDir: return @"USB_DataHddDir";
		case USB_DataHddDirEnd: return @"USB_DataHddDirEnd";
		case USB_CmdHddDel: return @"USB_CmdHddDel";
		case USB_CmdHddRename: return @"USB_CmdHddRename";
		case USB_CmdHddCreateDir: return @"USB_CmdHddCreateDir";
		case USB_CmdHddFileSend: return @"USB_CmdHddFileSend";
		case USB_DataHddFileStart: return @"USB_DataHddFileStart";
		case USB_DataHddFileData: return @"USB_DataHddFileData";
		case USB_DataHddFileEnd: return @"USB_DataHddFileEnd";
		default: return [NSString stringWithFormat:@"%#lx",
			(unsigned long) cmd];
	}
}

// Convert a Topfield-defined error code to a string for logging.
// The string is not localized.
// The ecode parameter would normally be one of the values listed
// in enum TopfieldUSBEcode, but other values are allowed too.
- (NSString*) nameOfEcode:(TopfieldUSBEcode)ecode {
	switch (ecode) {
		case USB_OK: return @"USB_OK";
		case USB_Err_Crc: return @"USB_Err_Crc";
		case USB_Err_UnknownCmd: return @"USB_Err_UnknownCmd";
		case USB_Err_InvalidCmd: return @"USB_Err_InvalidCmd";
		case USB_Err_Unknown: return @"USB_Err_Unknown";
		case USB_Err_Size: return @"USB_Err_Size";
		case USB_Err_RunFail: return @"USB_Err_RunFail";
		case USB_Err_Memory: return @"USB_Err_Memory";
		default: return [NSString stringWithFormat:@"%#lx",
			(unsigned long) ecode];
	}
}

// ---------------------------------------------------------------------------
#pragma mark Communication blocks for various commands
// ---------------------------------------------------------------------------

- (NSData*) prepareFailWithECode:(TopfieldUSBEcode)ecode {
	const UInt32 ecode_bigendian = EndianU32_NtoB(ecode);
	NSData* data = [NSData dataWithBytes:&ecode_bigendian length:4];
	if (data == nil) // out of memory presumably
		return nil;
	return [self prepareCommand:USB_Fail withData:data];
}

- (TopfieldUSBEcode) ecodeFromFail:(NSData*)block {
	if ([block length] < 12)
		return USB_Err_RunFail;
	UInt32 ecode_bigendian;
	[block getBytes:&ecode_bigendian range:(NSRange) {8,4}];
	return EndianU32_BtoN(ecode_bigendian);
}

- (NSData*) prepareSuccess {
	return [self prepareCommand:USB_Success withData:nil];
}

- (NSData*) prepareCancel {
	return [self prepareCommand:USB_Cancel withData:nil];
}

// Prepare a communication block that asks the Toppy to list the files
// in a directory.  Return the communication block, or nil on error.
- (NSData*) prepareCmdHddDirWithPath:(NSString*)path {
	NSMutableData* cmdData = [NSMutableData data];
	if (cmdData == nil) // out of memory presumably
		return nil;
	if (![self appendToData:cmdData fname:path])
		return nil;
	return [self prepareCommand:USB_CmdHddDir withData:cmdData];
}

- (NSData*) prepareCmdHddFileSendWithDirection:(UInt8)direction
		fname:(NSString*)fname offset:(UInt64)offset
{
	NSMutableData *cmdData = [NSMutableData dataWithBytes:&direction length:1];
	if (cmdData == nil) // out of memory presumably
		return nil;
	if (![self appendToData:cmdData sizeAndFname:fname])
		return nil;
	const UInt64 offset_bigendian = EndianU64_NtoB(offset);
	[cmdData appendBytes:&offset_bigendian length:8];
	return [self prepareCommand:USB_CmdHddFileSend withData:cmdData];
}

- (SInt64) offsetFromDataHddFileData:(NSData*)block {
	if ([block length] < 16)
		return -1;
	SInt64 offset_bigendian;
	[block getBytes:&offset_bigendian range:(NSRange) {8,8}];
	return EndianS64_BtoN(offset_bigendian);
}

- (NSData*) prepareDataHddFileEnd {
	return [self prepareCommand:USB_DataHddFileEnd withData:nil];
}

- (NSData*) prepareCmdTurboWithMode:(SInt32)mode {
	const SInt32 mode_bigendian = EndianS32_NtoB(mode);
	NSData* cmdData = [NSData dataWithBytes:&mode_bigendian length:4];
	if (cmdData == nil) // out of memory presumably
		return nil;
	return [self prepareCommand:USB_CmdTurbo withData:cmdData];
}

- (NSData*) prepareCmdHddDelWithFname:(NSString*)fname {
	NSMutableData* cmdData = [NSMutableData data];
	if (cmdData == nil) // out of memory presumably
		return nil;
	if (![self appendToData:cmdData fname:fname])
		return nil;
	return [self prepareCommand:USB_CmdHddDel withData:cmdData];
}

- (NSData*) prepareCmdHddRenameFromFname:(NSString*)oldFname toFname:(NSString*)newFname {
	NSMutableData* cmdData = [NSMutableData data];
	if (cmdData == nil) // out of memory presumably
		return nil;
	if (![self appendToData:cmdData sizeAndFname:oldFname])
		return nil;
	if (![self appendToData:cmdData sizeAndFname:newFname])
		return nil;
	return [self prepareCommand:USB_CmdHddRename withData:cmdData];
}

- (NSData*) prepareCmdHddCreateDirWithFname:(NSString*)fname {
	NSMutableData* cmdData = [NSMutableData data];
	if (cmdData == nil) // out of memory presumably
		return nil;
	if (![self appendToData:cmdData fname:fname])
		return nil;
	return [self prepareCommand:USB_CmdHddCreateDir withData:cmdData];
}

@end

//
//  TFUSBController.m
//  MacTF
//
//  Created by Nathan Oates on Sun Aug 01 2004.
//  Copyright (c) 2004-7 Nathan Oates nathan@noates.com All rights reserved.
//

/* This file was modified by Kalle Olavi Niemitalo on 2007-10-18.  */

// includes code based on uproar, license noted below:
//  uproar 0.1
//  Copyright (c) 2001 Kasima Tharnpipitchai <me@kasima.org>

/* 
 *
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

/* Much of the code that initializes and closes the device is from Apple
 * and is released under the license below.
 */

/*
 * © Copyright 2001 Apple Computer, Inc. All rights reserved.
 *
 * IMPORTANT:  This Apple software is supplied to you by Apple Computer, Inc. (“Apple”) in 
 * consideration of your agreement to the following terms, and your use, installation, 
 * modification or redistribution of this Apple software constitutes acceptance of these
 * terms.  If you do not agree with these terms, please do not use, install, modify or 
 * redistribute this Apple software.
 *
 * In consideration of your agreement to abide by the following terms, and subject to these 
 * terms, Apple grants you a personal, non exclusive license, under Apple’s copyrights in this 
 * original Apple software (the “Apple Software”), to use, reproduce, modify and redistribute 
 * the Apple Software, with or without modifications, in source and/or binary forms; provided 
 * that if you redistribute the Apple Software in its entirety and without modifications, you 
 * must retain this notice and the following text and disclaimers in all such redistributions 
 * of the Apple Software.  Neither the name, trademarks, service marks or logos of Apple 
 * Computer, Inc. may be used to endorse or promote products derived from the Apple Software 
 * without specific prior written permission from Apple. Except as expressly stated in this 
 * notice, no other rights or licenses, express or implied, are granted by Apple herein, 
 * including but not limited to any patent rights that may be infringed by your derivative 
 * works or by other works in which the Apple Software may be incorporated.
 * 
 * The Apple Software is provided by Apple on an "AS IS" basis.  APPLE MAKES NO WARRANTIES, 
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-
 * INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, REGARDING THE APPLE 
 * SOFTWARE OR ITS USE AND OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS. 
 *
 * IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS 
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, 
 * REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND 
 * WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR 
 * OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <mach/mach.h>
#include <IOKit/IOCFPlugIn.h>
#include <CoreFoundation/CFNumber.h>

#import "DataHandler.h"
#import "TFUSBController.h"
#import "TFDataFormat.h"
#import "UIElements.h"

static void hexDump(UInt8 *buf, int len);
static int doSend(IOUSBDeviceInterface197 **dev,
	IOUSBInterfaceInterface197 **intf, UInt8 *outBuf, UInt32 len, int type);
static int doRecv(IOUSBDeviceInterface197 **dev,
	IOUSBInterfaceInterface197 **intf, UInt8 *inBuf, UInt32 dataLen, int type);
static int dealWithInterface(io_service_t usbInterfaceRef,
	USBDeviceContext *device);
static int dealWithDevice(io_service_t usbDeviceRef, USBDeviceContext *device);
static int initDevice(USBDeviceContext *device);

static int debug;
static int rate;
static int kBlockSize;
static int connectedSpeed;

@interface TFUSBController (PrivateMethods)

// Low-level USB
- (NSData*) sendCommand:(NSData*)fullyPackagedCommand
	toDevice:(USBDeviceContext*)device expectResponse:(BOOL) getResponse
	careForReturn:(BOOL)careFactor;

// Protocol message sequences
- (id) getFileListForPath:(NSString*)path;
- (void) turnTurboOn:(BOOL)turnOn;
- (void) renameFile:(NSString*)oldName withName:(NSString*)newName atPath:(NSString*)currentPath;
- (void) makeFolder:(NSString*)newName atPath:(NSString*)currentPath;
- (void) checkUSB:(USBDeviceContext*)device;

// User interface
- (UIElements*) uiElements;
- (void) updateProgress:(NSDictionary*)inDict;
- (NSString*) elapsedTime:(NSTimeInterval)totalSeconds;

// Queue management
- (BOOL) hasPriorityTransfer;
- (void) getNextTransfer:(NSDictionary**)transfer queue:(NSMutableArray**)queue;
- (void) removeTransfer:(NSDictionary*)transfer fromQueue:(NSMutableArray*)queue;

@end

#define 	TopfieldVendorID	4571
   //1317 for HC
   //0x138c for humax
   //4571 for toppy
#define 	TF5kProdID 	4096
//42514 for HC
//0x02ad for humax
//4096 for toppy

// ---------------------------------------------------------------------------
#pragma mark Functions outside TFUSBController
// ---------------------------------------------------------------------------

static void
hexDump(UInt8 *buf, int len)
{
	int row, col, maxrows;

	maxrows = len/16;
	if (len % 16) maxrows++;
	for (row=0; row< maxrows; row++) {
		for (col=0; col<16; col++) {
			if (!(col%2)) printf(" ");
			printf("%02x", buf[row*16 + col] & 0xff);
		}
		printf("\t");	
		for (col=0; col<16; col++) {
			if ((buf[row*16 + col]>32) && (buf[row*16 + col]<126)) {
				printf("%c", buf[row*16 + col]);
			}
			else { printf("."); }
		}
		printf("\n");
	}
}

static int
doSend(IOUSBDeviceInterface197 **dev, IOUSBInterfaceInterface197 **intf,
	UInt8 *outBuf, UInt32 len, int type)
{
	IOReturn    err;
	UInt32      sendLen;

	if (debug == 1){
		printf(("sending:\n"));
		hexDump(outBuf, len);
	}
	sendLen = ((len/kBlockSize))*kBlockSize;
	if (len % kBlockSize)
		sendLen += kBlockSize;
	if ((sendLen % 0x200) == 0)  
		sendLen += kBlockSize;

	err = (*intf)->WritePipeTO(intf, 1, outBuf, sendLen, 1000, 20000);
	if (err) {
		printf("write err: %08x\n", err);
		return err;
	}
	return err;
}

static int
doRecv(IOUSBDeviceInterface197 **dev, IOUSBInterfaceInterface197 **intf,
	UInt8 *inBuf, UInt32 dataLen, int type)
{
	IOReturn    err;
	UInt32      len;

	if (dataLen > kMaxXferSize) return 1;

	len = (dataLen/kBlockSize) * kBlockSize;
	if (dataLen % kBlockSize)
		len += kBlockSize;

	err = (*intf)->ReadPipeTO(intf, 2, (void *)inBuf, &len,  1000, 20000);
	if (err) {
		printf("read err 2: %08x\n", err);
		printf("resetting\n");
		(*intf)->ClearPipeStallBothEnds(intf, 2); // ignore return value
		return err;
	}

	if (debug == 1) {
		printf(("receiving: \n")); 
		hexDump(inBuf, len);
	}
	return err;
}

static int
dealWithInterface(io_service_t usbInterfaceRef, USBDeviceContext *device)
{
	IOReturn                     err;
	IOCFPlugInInterface        **iodev;		// requires <IOKit/IOCFPlugIn.h>
	IOUSBInterfaceInterface197 **intf;
	IOUSBDeviceInterface197    **dev;
	SInt32                       score;
	UInt8                        numPipes, confNum, dSpeed;


	err = IOCreatePlugInInterfaceForService(usbInterfaceRef, kIOUSBInterfaceUserClientTypeID, kIOCFPlugInInterfaceID, &iodev, &score);
	if (err || !iodev)
	{
		printf("dealWithInterface: unable to create plugin. ret = %08x, iodev = %p\n", err, iodev);
		return kUproarDeviceErr;
	}
	err = (*iodev)->QueryInterface(iodev, CFUUIDGetUUIDBytes(kIOUSBInterfaceInterfaceID), (LPVOID)&(device->intf));
	(*iodev)->Release(iodev);				// done with this
	intf = device->intf;
	if (err || !intf)
	{
		printf("dealWithInterface: unable to create a device interface. ret = %08x, intf = %p\n", err, intf);
		return kUproarDeviceErr;
	}

	dev = device->dev;
	err = (*intf)->USBInterfaceOpen(intf);
	if (err)
	{
		printf("dealWithInterface: unable to open interface. ret = %08x\n", err);
		return kUproarDeviceErr;
	}
	err = (*intf)->GetNumEndpoints(intf, &numPipes);
	if (err)
	{
		printf("dealWithInterface: unable to get number of endpoints. ret = %08x\n", err);
		return kUproarDeviceErr;
	}

	printf("dealWithInterface: found %d pipes\n", numPipes);

	err = (*intf)->GetConfigurationValue(intf, &confNum);
	err = (*dev)->GetDeviceSpeed(dev, &dSpeed);
	if (dSpeed == 2) {
		kBlockSize = 0x40;
	} else {
		kBlockSize = 0x4;
	}
	printf("confnum: %08x, dspeed: %08x, blockS:%i\n", confNum, dSpeed, kBlockSize);
	connectedSpeed = dSpeed;
	return kUproarSuccess;
}


static int
dealWithDevice(io_service_t usbDeviceRef, USBDeviceContext *device)
{
	IOReturn                        err;
	IOCFPlugInInterface           **iodev;		// requires <IOKit/IOCFPlugIn.h>
	IOUSBDeviceInterface197       **dev;
	SInt32                          score;
	UInt8                           numConf;
	IOUSBConfigurationDescriptorPtr confDesc;
	IOUSBFindInterfaceRequest       interfaceRequest;
	io_iterator_t                   iterator;
	io_service_t                    usbInterfaceRef;
	int                             found=0;


	err = IOCreatePlugInInterfaceForService(usbDeviceRef, kIOUSBDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &iodev, &score);
	if (err || !iodev)
	{
		printf("dealWithDevice: unable to create plugin. ret = %08x, iodev = %p\n", err, iodev);
		return kUproarDeviceErr;
	}
	err = (*iodev)->QueryInterface(iodev, CFUUIDGetUUIDBytes(kIOUSBDeviceInterfaceID), (LPVOID)&(device->dev));
	(*iodev)->Release(iodev);				

	dev = device->dev;
	if (err || !dev)
	{
		printf("dealWithDevice: unable to create a device interface. ret = %08x, dev = %p\n", err, dev);
		return kUproarDeviceErr;
	}
	err = (*dev)->USBDeviceOpen(dev);
	if (err)
	{
		printf("dealWithDevice: unable to open device. ret = %08x\n", err);
		return kUproarDeviceErr;
	}
	err = (*dev)->GetNumberOfConfigurations(dev, &numConf);
	if (err || !numConf)
	{
		printf("dealWithDevice: unable to obtain the number of configurations. ret = %08x\n", err);
		return kUproarDeviceErr;
	}
	printf("dealWithDevice: found %d configurations\n", numConf);
	err = (*dev)->GetConfigurationDescriptorPtr(dev, 0, &confDesc);	// get the first config desc (index 0)
	if (err)
	{
		printf("dealWithDevice:unable to get config descriptor for index 0\n");
		return kUproarDeviceErr;
	}
	err = (*dev)->SetConfiguration(dev, confDesc->bConfigurationValue);
	if (err)
	{
		printf("dealWithDevice: unable to set the configuration\n");
		return kUproarDeviceErr;
	}

	interfaceRequest.bInterfaceClass = kIOUSBFindInterfaceDontCare;    // requested class
	interfaceRequest.bInterfaceSubClass = kIOUSBFindInterfaceDontCare; // requested subclass
	interfaceRequest.bInterfaceProtocol = kIOUSBFindInterfaceDontCare; // requested protocol
	interfaceRequest.bAlternateSetting = kIOUSBFindInterfaceDontCare;  // requested alt setting

	err = (*dev)->CreateInterfaceIterator(dev, &interfaceRequest, &iterator);
	if (err)
	{
		printf("dealWithDevice: unable to create interface iterator\n");
		return kUproarDeviceErr;
	}

	while (usbInterfaceRef = IOIteratorNext(iterator))
	{
		printf("found interface: %#jx\n", (uintmax_t)usbInterfaceRef);
		err = dealWithInterface(usbInterfaceRef, device);
		IOObjectRelease(usbInterfaceRef);	// no longer need this reference
		found = 1;
	}

	IOObjectRelease(iterator);
	iterator = 0;
	if ((!found) || (err))
		return kUproarDeviceErr;
	else
		return kUproarSuccess;
}

static int
initDevice(USBDeviceContext *device)
{
	mach_port_t            masterPort = 0;
	kern_return_t          err;
	CFMutableDictionaryRef matchingDictionary = 0;		// requires <IOKit/IOKitLib.h>
	short                  idVendor = TopfieldVendorID;
	short                  idProduct = TF5kProdID;
	CFNumberRef            numberRef;
	io_iterator_t          iterator = 0;
	io_service_t           usbDeviceRef;
	int                    found =0;

	err = IOMasterPort(bootstrap_port, &masterPort);			

	if (err)
	{
		printf("Anchortest: could not create master port, err = %08x\n", err);
		return err;
	}
	matchingDictionary = IOServiceMatching(kIOUSBDeviceClassName);	// requires <IOKit/usb/IOUSBLib.h>
	if (!matchingDictionary)
	{
		printf("Anchortest: could not create matching dictionary\n");
		return -1;
	}
	numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberShortType, &idVendor);
	if (!numberRef)
	{
		printf("Anchortest: could not create CFNumberRef for vendor\n");
		return -1;
	}
	CFDictionaryAddValue(matchingDictionary, CFSTR(kUSBVendorName), numberRef);
	CFRelease(numberRef);
	numberRef = 0;
	numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberShortType, &idProduct);
	if (!numberRef)
	{
		printf("Anchortest: could not create CFNumberRef for product\n");
		return -1;
	}
	CFDictionaryAddValue(matchingDictionary, CFSTR(kUSBProductName), numberRef);
	CFRelease(numberRef);
	numberRef = 0;

	err = IOServiceGetMatchingServices(masterPort, matchingDictionary, &iterator);
	matchingDictionary = 0;			// this was consumed by the above call

	while (usbDeviceRef = IOIteratorNext(iterator))
	{
		printf("Found device %#jx\n", (uintmax_t)usbDeviceRef);
		err = dealWithDevice(usbDeviceRef, device);
		IOObjectRelease(usbDeviceRef);			// no longer need this reference
		found = 1;
	}


	IOObjectRelease(iterator);
	iterator = 0;
	mach_port_deallocate(mach_task_self(), masterPort);
	if ((!found) || (err))
		return kUproarDeviceErr;
	else
		return kUproarSuccess;
}

@implementation TFUSBController (PrivateMethods)

// ---------------------------------------------------------------------------
#pragma mark Low-level USB (PrivateMethods)
// ---------------------------------------------------------------------------

- (NSData*)sendCommand:(NSData*)fullyPackagedCommand
	toDevice:(USBDeviceContext*)device expectResponse:(BOOL) getResponse
	careForReturn:(BOOL) careFactor
{
	IOReturn	err;
	int cmdLength = [fullyPackagedCommand length];
	unsigned char outBuffer[cmdLength];
	memset(outBuffer, 0, cmdLength);
//	NSLog(@"send: %@", [fullyPackagedCommand description]);
	[fullyPackagedCommand getBytes:outBuffer];
	
	err = doSend(device->dev, device->intf, outBuffer, cmdLength, 2);
	if (err)
	{
		NSLog(@"sendError: %08x\n");
		return nil;
	}
	
	if (! getResponse) return nil;
	
	int inLen = 0xFFFF; // i think this is biggest needed?
	unsigned char inBuf[inLen];
	memset(inBuf, 0, inLen);
	err = doRecv(device->dev, device->intf, inBuf, inLen, 2);
	if (err)
	{
		NSLog(@"inError: %08x\n", err);
		return nil;
	}

	if (! careFactor) return nil;
	NSMutableData* data = [NSMutableData dataWithBytes:inBuf length:inLen];
	data = [dataFormat swap:data];
	inLen = inBuf[1]*256 + inBuf[0]; // work out how long the response really is. NB data is flipped, but inBuf still isn't
	return [data subdataWithRange:(NSRange) {0,inLen}];
}

// ---------------------------------------------------------------------------
#pragma mark Protocol message sequences (PrivateMethods)
// ---------------------------------------------------------------------------

- (id) getFileListForPath:(NSString*)path {	
	if (myContext == nil)
		return nil;

	NSData* hddListCmd = [dataFormat prepareCmdHddDirWithPath:path];
	if (hddListCmd == nil)
		return nil;

	[self checkUSB:myContext]; // sends cancel and waits for response
	int contiguousErrors = 0;
	[[dh fileList] removeAllObjects];
	NSData* response = [self sendCommand:hddListCmd toDevice:myContext
		expectResponse:YES careForReturn:YES];
	while (response != nil && contiguousErrors++ < 5) {
		TopfieldUSBEcode ecode = USB_OK;
		if (![dataFormat isCommunicationBlockValid:response error:NULL ecode:&ecode]) {
			response = [self sendCommand:[dataFormat prepareFailWithECode:ecode]
				toDevice:myContext expectResponse:YES careForReturn:YES];
		} else switch ([dataFormat cmdFromCommunicationBlock:response]) {
		case USB_Fail:
//			[statusField setStringValue:NSLocalizedString(@"LAST_ERROR", @"Error on last command.")];
			[[dh fileList] removeAllObjects];
			response = [self sendCommand:hddListCmd toDevice:myContext
				expectResponse:YES careForReturn:YES];
			break;
		case USB_DataHddDir:
			contiguousErrors = 0;
			size_t i;
			// Swapping sometimes adds a byte of padding.  The following uses
			// only complete 144-byte structures and so ignores such padding.
			for (i=0; 8+(i+1)*114 <= [response length]; i++) {
				NSData* typeFile = [response subdataWithRange:(NSRange) {8+i*114,114}];
				NSMutableDictionary* tfFile = [dh newTFFileFromSwappedHexData:typeFile];
				if (![[tfFile objectForKey:@"name"] isEqualToString:@".."]) {
					[dh convertRawDataToUseful:tfFile];
					[[dh fileList] addObject:tfFile];
				}
				[tfFile release];
			}
			response = [self sendCommand:[dataFormat prepareSuccess]
				toDevice:myContext expectResponse:YES careForReturn:YES];
			break;
		case USB_DataHddDirEnd:
			contiguousErrors = 0;
			response = nil;
			break;
		default:
			[self checkUSB:myContext]; // cancel whatever is going on
			[[dh fileList] removeAllObjects];
			response = [self sendCommand:hddListCmd toDevice:myContext
				expectResponse:YES careForReturn:YES];
			break;
		}
	}

	[tableView reloadData];
	[[self uiElements] tableView:tableView didClickTableColumn:[[self uiElements]selectedColumn]];
	[[self uiElements] tableView:tableView didClickTableColumn:[[self uiElements]selectedColumn]]; //twice so get the same sort as before
	return nil;
}

- (void) turnTurboOn:(BOOL)turnOn {
	if (![[[self uiElements] isConnected] intValue]) return;
	[self checkUSB:myContext];
	NSData* turboCommand = [dataFormat prepareCmdTurboWithMode:(turnOn ? 1 : 0)];
	[self sendCommand:turboCommand toDevice:myContext expectResponse:YES careForReturn:NO];
}

- (void) renameFile:(NSString*)oldName withName:(NSString*)newName
	atPath:(NSString*)currentPath
{
	NSLog(@"%@,%@,%@", oldName, newName, currentPath);
	[self checkUSB:myContext];
	NSString* oldFname = [dataFormat fnameForFile:oldName atPath:currentPath];
	NSString* newFname = [dataFormat fnameForFile:newName atPath:currentPath];
	NSData* fileRenCmd = [dataFormat prepareCmdHddRenameFromFname:oldFname
		toFname:newFname];
	[self sendCommand:fileRenCmd toDevice:myContext expectResponse:YES careForReturn:NO];
}

- (void) makeFolder:(NSString*)newName atPath:(NSString*)currentPath {
	[self checkUSB:myContext];
	NSString* fname = [dataFormat fnameForFile:newName atPath:currentPath];
	NSData* newFoldCmd = [dataFormat prepareCmdHddCreateDirWithFname:fname];
	NSData* usbCancel = [dataFormat prepareCancel];
	[self sendCommand:usbCancel toDevice:myContext expectResponse:YES careForReturn:NO];
	[self sendCommand:newFoldCmd toDevice:myContext expectResponse:YES careForReturn:NO];
}

- (void) checkUSB:(USBDeviceContext*)device {
	NSData* usbCancel = [dataFormat prepareCancel];
	int retry;
	for (retry = 0; retry < 8; ++retry) {
		NSData* block = [self sendCommand:usbCancel toDevice:device
			expectResponse:YES careForReturn:YES];
		NSError* error = nil;
		if (![dataFormat isCommunicationBlockValid:block
				error:&error ecode:NULL]) {
			NSLog(@"bad response to cancel: %@", [error localizedDescription]);
			continue;
		}
		TopfieldUSBCmd cmd = [dataFormat cmdFromCommunicationBlock:block];
		if (cmd != USB_Success) {
			NSLog(@"response to cancel was %#x rather than success", (unsigned) cmd);
			continue;
		}
		return;
	}
	// tell someone that no longer connected here??
}

// ---------------------------------------------------------------------------
#pragma mark User interface (PrivateMethods)
// ---------------------------------------------------------------------------

- (UIElements*) uiElements {
	return [NSApp delegate];
}

- (void) updateProgress:(NSDictionary*)inDict {
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
	double offset = [[inDict objectForKey:@"offset"] doubleValue];
	double size = [[inDict objectForKey:@"size"] doubleValue];
	NSDate* startTime = [inDict objectForKey:@"startTime"]; 	
	[progressBar setDoubleValue:((double)offset/size*100)];
	[progressBar displayIfNeeded];
	[progressTime setStringValue:[self elapsedTime:[[NSDate date] timeIntervalSinceDate:startTime]]];
	[progressTime displayIfNeeded];
	[pool release];
}

- (NSString*) elapsedTime:(NSTimeInterval)totalSeconds {
	char hp = 20;
	char mp = 20;
	char sp = 20; //padding
	int hours = (totalSeconds / 3600);  // returns number of whole hours fitted in totalSecs
	if (hours < 10)
		hp = 48;
	int minutes = ((totalSeconds / 60) - hours*60);  // Whole minutes
	if (minutes < 10)
		mp = 48;
	int seconds = ((long) totalSeconds % 60);	// Here we can use modulo to get num secs NOT fitting in whole minutes (60 secs)
	if (seconds < 10)
		sp = 48;
	return [NSString stringWithFormat:@"%c%i:%c%i:%c%i", hp, hours, mp, minutes, sp, seconds];
}

// ---------------------------------------------------------------------------
#pragma mark Queue management (PrivateMethods)
// ---------------------------------------------------------------------------

- (BOOL) hasPriorityTransfer {
	BOOL ret;
	@synchronized (self) {
		ret = ([priorityTransferQueue count] > 0);
	}
	return ret;
}

- (void) getNextTransfer:(NSDictionary**)transferOut
	queue:(NSMutableArray**)queueOut
{
	NSDictionary* transfer = nil;
	NSMutableArray* queue = nil;

	@synchronized (self) {
		if ([priorityTransferQueue count] > 0) {
			queue = priorityTransferQueue;
			transfer = [queue objectAtIndex:0];
		} else if ([transferQueue count] > 0) {
			queue = transferQueue;
			transfer = [queue objectAtIndex:0];
		}

		// The queue exists as long as the TFUSBController does
		// so it need not be retained and autoreleased here.
		if (transfer != nil)
			[[transfer retain] autorelease];
	}

	if (transferOut != NULL)
		*transferOut = transfer;
	if (queueOut != NULL)
		*queueOut = queue;
}

- (void) removeTransfer:(NSDictionary*)transfer
	fromQueue:(NSMutableArray*)queue
{
	@synchronized (self) {
		[queue removeObjectIdenticalTo:transfer];
	}
}

@end

@implementation TFUSBController

// ---------------------------------------------------------------------------
#pragma mark Low-level USB
// ---------------------------------------------------------------------------

- (void) closeDevice:(USBDeviceContext *)device
{
	IOUSBInterfaceInterface197 **intf;
	IOUSBDeviceInterface197    **dev;
	IOReturn                     err;

	intf = device->intf;
	dev = device->dev;

	err = (*intf)->USBInterfaceClose(intf);
	if (err)
	{
		printf("dealWithInterface: unable to close interface. ret = %08x\n", err);
	}
	err = (*intf)->Release(intf);
	if (err)
	{
		printf("dealWithInterface: unable to release interface. ret = %08x\n", err);
	}

	err = (*dev)->USBDeviceClose(dev);
	if (err)
	{
		printf("dealWithDevice: error closing device - %08x\n", err);
		(*dev)->Release(dev);
	}
	err = (*dev)->Release(dev);
	if (err)
	{
		printf("dealWithDevice: error releasing device - %08x\n", err);
	}
}

- (USBDeviceContext*) initializeUSB {
	USBDeviceContext *device;
	int               err;
	kBlockSize = 0x08;
	device = malloc(sizeof(USBDeviceContext));
	err = initDevice(device);
	if (err) {
		printf("Could not connect to Topfield\n");
		return nil;
	}
	printf("\n\n");
	printf("Connected to Topfield\n\n");
	myContext = device;
	return device;
}

// ---------------------------------------------------------------------------
#pragma mark Protocol message sequences
// ---------------------------------------------------------------------------

- (int) getFile:(NSDictionary*)fileInfo forPath:(NSString*)currentPath
		toSaveTo:(NSString*)savePath beginAtOffset:(unsigned long long) offset
		withLooping:(BOOL)looping existingTime:(NSTimeInterval)existingTime {	
//	[progressBar setDoubleValue:0];
//	[progressTime setDoubleValue:0];
	NSString* nameOnToppy = [fileInfo objectForKey:@"name"];
	NSNumber* fileSize = [fileInfo objectForKey:@"fileSize"];

	[[[self uiElements] currentlyField] setStringValue:
		[NSLocalizedString(@"DOWNLOADING", @"Downloading: ")
			stringByAppendingString:nameOnToppy]];
	[[[self uiElements] connectLight] setImage:[NSImage imageNamed:@"blink.tiff"]];
	[[[self uiElements] currentlyField] displayIfNeeded];

	//prepackage commands to send
	NSData* fileSendCmd = [dataFormat
		prepareCmdHddFileSendWithDirection:USB_FileToHost
		fname:[dataFormat fnameForFile:nameOnToppy atPath:currentPath]
		offset:offset];
	NSData* usbSuccess = [dataFormat prepareSuccess];
	NSData* usbCancel = [dataFormat prepareCancel];
	
	// send commands
	if ([turboCB state]) 
		[self turnTurboOn:YES];
	else
		[self checkUSB:myContext]; //turbo has a check itself

	NSData* receivedBlock = [self sendCommand:fileSendCmd toDevice:myContext
		expectResponse:YES careForReturn:YES];
	NSError* error = nil;
	if (![dataFormat isCommunicationBlockValid:receivedBlock
			error:&error ecode:NULL]) {
		NSLog(@"Malformed response from Toppy after %@: %@",
			[dataFormat nameOfCmd:USB_CmdHddFileSend],
			[error localizedDescription]);
		[self turnTurboOn:NO];
		return 1;
	}
	TopfieldUSBCmd receivedCmd = [dataFormat
		cmdFromCommunicationBlock:receivedBlock];
	if (receivedCmd == USB_Fail) {
		TopfieldUSBEcode ecode = [dataFormat ecodeFromFail:receivedBlock];
		NSLog(@"Download fails.  Toppy reports error %@",
			[dataFormat nameOfEcode:ecode]);
		// TODO: retry if USB_Err_CRC?
		[self turnTurboOn:NO];
	} else if (receivedCmd != USB_DataHddFileStart) {
		NSLog(@"Unexpected response from Toppy during download.  "
		       "Expected %@ but received %@",
			[dataFormat nameOfCmd:USB_DataHddFileStart],
			[dataFormat nameOfCmd:receivedCmd]);
		[self turnTurboOn:NO];
		return 1;
	}

	// start timer
	NSDate* startTime = [NSDate date];
	startTime = [startTime addTimeInterval:(0-existingTime)];
	// acknowledge the USB_DataHddFileStart
	receivedBlock = [self sendCommand:usbSuccess toDevice:myContext
		expectResponse:YES careForReturn:YES];
	if (![dataFormat isCommunicationBlockValid:receivedBlock
			error:&error ecode:NULL]) {
		NSLog(@"Malformed block from Toppy after %@: %@",
			[dataFormat nameOfCmd:USB_Success],
			[error localizedDescription]);
		[self turnTurboOn:NO];
		return 1;
	}
	receivedCmd = [dataFormat cmdFromCommunicationBlock:receivedBlock];
	if (receivedCmd != USB_DataHddFileData) {
		NSLog(@"Unexpected response from Toppy during download.  "
		       "Expected %@ but received %@",
			[dataFormat nameOfCmd:USB_DataHddFileData],
			[dataFormat nameOfCmd:receivedCmd]);
		[self turnTurboOn:NO];
		return 1;
	}
	if ([receivedBlock length] < 16) {
		NSLog(@"Incorrect Data length in %@",
			[dataFormat nameOfCmd:receivedCmd]);
		[self turnTurboOn:NO];
		return 1;
	}
	SInt64 receivedOffset = [dataFormat offsetFromDataHddFileData:receivedBlock];
	if (receivedOffset != offset) {
		NSLog(@"File offset skipped from %lld to %lld!",
			offset, receivedOffset);
		[self turnTurboOn:NO];
		return 1;
	}

	// clean up data and prepare path to save it
	NSData* finalData = [receivedBlock subdataWithRange:(NSRange) {16,[receivedBlock length]-16}];
	
	// first initialize a file of the right name, as NSFileHandle requires an existing file to work on
	if (offset == 0)
		[[NSData dataWithBytes:"\0" length:1] writeToFile:savePath atomically:NO]; // write 0x00 to initialize (overwritten later)
	NSFileHandle* outFile = [NSFileHandle fileHandleForWritingAtPath:savePath];
	[outFile seekToFileOffset:offset];
	[outFile writeData:finalData];
	BOOL returnSuccess = YES;
	if (looping) {	// loop for multiple data sends (ie files > 64k)
		int updateRate = 8; 
		if ([fileSize isGreaterThan:[NSNumber numberWithDouble:1048576]]) {
			updateRate = 24;
			NSLog(@"large file detected - low GUI update rate");
		}

		// Because this loop usually runs for a long time, use a separate
		// NSAutoreleasePool for each iteration.  Keep the current pool
		// in a variable outside the loop so that it can be released when
		// the loop is left with a break statement.  (@finally is another
		// alternative, but it would require extra code in order to avoid
		// releasing exception objects too early.)
		NSAutoreleasePool* pool = nil;
		int dataBlocksReceived = 0;
		TopfieldUSBEcode sendEcode = USB_OK;

		for (;;) {
			if (pool != nil)
				[pool release];
			pool = [[NSAutoreleasePool alloc] init];

			if (![[[self uiElements] isConnected] boolValue]) {
				NSLog(@"Aborting the transfer because the Toppy has been disconnected.");
				returnSuccess = NO;
				break;
			}
			if ([self hasPriorityTransfer]) {
				NSLog(@"Suspending the transfer to make way for a priority transfer.");
				[self addTransfer:[NSDictionary dictionaryWithObjectsAndKeys:
						fileInfo,@"filename", currentPath,@"path", savePath,@"savePath",
						[NSNumber numberWithUnsignedLongLong:[outFile offsetInFile]],@"offset",
						@"download",@"transferType", [NSNumber numberWithBool:YES],@"looping",
						[NSNumber numberWithInt:[[NSDate date] timeIntervalSinceDate:startTime]],@"existingTime",
						nil]
					atIndex:1]; // nb adding to index 1 as the current transfer lies at 0 and will be deleted soon
				[self sendCommand:usbCancel toDevice:myContext
					expectResponse:YES careForReturn:NO];
				returnSuccess = NO;
				break;
			}

			NSData* sendBlock;
			if (sendEcode == USB_OK)
				sendBlock = usbSuccess;
			else
				sendBlock = [dataFormat prepareFailWithECode:sendEcode];
			receivedBlock = [self sendCommand:sendBlock toDevice:myContext
				expectResponse:YES careForReturn:YES];
			sendEcode = USB_OK;
			if (![dataFormat isCommunicationBlockValid:receivedBlock
					error:&error ecode:&sendEcode]) {
				NSLog(@"Malformed block from Toppy after %@: %@",
					[dataFormat nameOfCmd:USB_Success],
					[error localizedDescription]);
				continue;
			}
			receivedCmd = [dataFormat cmdFromCommunicationBlock:receivedBlock];
			if (receivedCmd == USB_DataHddFileData) {
				if ([receivedBlock length] < 16) {
					NSLog(@"Received %@ is too short, only %d bytes",
						[dataFormat nameOfCmd:receivedCmd],
						(int) [receivedBlock length]);
					sendEcode = USB_Err_Size;
					continue;
				}
				SInt64 receivedOffset = [dataFormat
					offsetFromDataHddFileData:receivedBlock];
				if (receivedOffset != [outFile offsetInFile]) {
					NSLog(@"File offset skipped from %lld to %lld!",
						[outFile offsetInFile], receivedOffset);
					returnSuccess = NO;
					break;
				}
				NSData* fileData = [receivedBlock
					subdataWithRange:(NSRange) {16, [receivedBlock length] - 16}];
				[outFile writeData:fileData];

				dataBlocksReceived++;
				if (dataBlocksReceived < 8 || dataBlocksReceived % updateRate == 0) {
					[self performSelectorOnMainThread:@selector(updateProgress:)
						withObject:[NSDictionary dictionaryWithObjectsAndKeys:
							[NSNumber numberWithDouble:(double)[outFile offsetInFile]], @"offset",
							fileSize, @"size", startTime, @"startTime", nil]
						waitUntilDone:NO
						modes:[NSArray arrayWithObject:NSDefaultRunLoopMode]];
				}
				continue;
			} else if (receivedCmd == USB_DataHddFileEnd) {
				[self sendCommand:usbSuccess toDevice:myContext
					expectResponse:NO careForReturn:NO];
				returnSuccess = YES;
				break;
			} else {
				NSLog(@"Unexpected response from Toppy during download.  "
					   "Expected %@ but received %@",
					[dataFormat nameOfCmd:USB_DataHddFileData],
					[dataFormat nameOfCmd:receivedCmd]);
				[self sendCommand:usbCancel toDevice:myContext
					expectResponse:YES careForReturn:NO];
				returnSuccess = NO;
				break;
			}
			// not reached
		} // for ever

		if (pool != nil)
			[pool release];
	} // if looping

	[outFile synchronizeFile];
	[outFile closeFile];
	//now add the right modification date to the file
	[[NSFileManager defaultManager]
		changeFileAttributes:[NSDictionary
			dictionaryWithObject:[fileInfo objectForKey:@"date"]
			forKey:@"NSFileModificationDate"]
		atPath:savePath];
	[self turnTurboOn:NO];
	if (looping) [[self uiElements] finishTransfer];
	return returnSuccess ? 1 : 0;
}

- (void) uploadFile:(NSString*)fileToUpload ofSize:(long long)size
	fromPath:(NSString*)curPath withAttributes:(NSData*)typeFile
	atOffset:(unsigned long long)offset
	existingTime:(NSTimeInterval)existingTime
{
	NSLog(@"upload: %@,%@,%qu", fileToUpload, curPath, offset);
	USBDeviceContext* dev = myContext;
	// prepare Send command
	NSMutableArray* array = [NSMutableArray arrayWithArray:[fileToUpload componentsSeparatedByString:@"/"]];
	NSMutableString* fname = [NSMutableString stringWithString:[array lastObject]];
	char dir = USB_FileToDevice;
	NSMutableData* build = [NSMutableData dataWithBytes:&dir length:1];
	short int nsize = [fname length]+[curPath length]+2;// one for slash, one for padding 0x00
	const UInt16 nsize_bigendian = EndianU16_NtoB(nsize);
	[build appendData:[NSData dataWithBytes:&nsize_bigendian length:2]];
	NSData* d = [curPath dataUsingEncoding:NSISOLatin1StringEncoding];
	[build appendData:d];
	dir = 0x5c; // 0x5c = "/"
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[fname dataUsingEncoding:NSISOLatin1StringEncoding]];// may need to pad to 95...
	if ([fname length] < 95)
		[build increaseLengthBy:95-[fname length]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	UInt64 offset_bigendian = EndianU64_NtoB(offset);
	[build appendData:[NSData dataWithBytes:&offset_bigendian length:8]];
	
	// prepackage commands to send
	NSData* fileSendCmd = [dataFormat prepareCommand:USB_CmdHddFileSend
		withData:build];
	NSData* fileStartCmd = [dataFormat prepareCommand:USB_DataHddFileStart
		withData:typeFile];
	NSData* fileEndCmd = [dataFormat prepareDataHddFileEnd];

	//start timer
	NSDate* startTime = [NSDate date];
	startTime = [startTime addTimeInterval:(0-existingTime)];
	//send commands
	if ([turboCB state]) 
		[self turnTurboOn:YES];
	else
		[self checkUSB:dev];
	// now the proper commands
	NSData *data = [self sendCommand:fileSendCmd toDevice:dev expectResponse:YES careForReturn:YES];
	data = [self sendCommand:fileStartCmd toDevice:dev expectResponse:YES careForReturn:YES];
	const UInt32 rW_bigendian = EndianU32_NtoB(USB_Success);
	NSData* responseWanted = [NSData dataWithBytes:&rW_bigendian length:4];
	NSData* responseToCheck = nil;

	NSFileHandle* fileHandle = [NSFileHandle fileHandleForReadingAtPath:fileToUpload];
	[fileHandle seekToFileOffset:offset];
	do {
		if ([self hasPriorityTransfer]) {
			//break out and create a new transfer to continue it
			NSLog(@"pausing upload");
			[self addTransfer:[NSDictionary dictionaryWithObjectsAndKeys:
					fileToUpload,@"filename",
					[NSNumber numberWithUnsignedLongLong:size],@"fileSize",
					curPath,@"path",typeFile,@"attributes",@"upload",@"transferType",
					[NSNumber numberWithUnsignedLongLong:offset],@"offset",
					[NSNumber numberWithInt:[[NSDate date] timeIntervalSinceDate:startTime]],@"existingTime",
					nil]
				atIndex:1]; //nb use index 1 as current transfer is at 0 and will be deleted
			[self turnTurboOn:NO];
			break;
		} else {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		offset_bigendian = EndianU64_NtoB(offset);
		NSMutableData* fileData = [NSMutableData dataWithBytes:&offset_bigendian length:8];
		[fileData appendData:[fileHandle readDataOfLength:rate]];
		offset += rate;
		NSData* fileDataCmd = [dataFormat prepareCommand:USB_DataHddFileData
			withData:fileData];
		data = [self sendCommand:fileDataCmd toDevice:dev expectResponse:YES careForReturn:YES];
		if ([data length] >= 8) 
			responseToCheck = [data subdataWithRange:(NSRange) {4,4}];
		else responseToCheck = nil;
		if (responseToCheck == nil || ![responseWanted isEqualToData:responseToCheck]) 
			break;
		[NSThread detachNewThreadSelector:@selector(updateProgress:) toTarget:self
			withObject:[NSDictionary dictionaryWithObjectsAndKeys:
				[NSNumber numberWithDouble:(double)offset], @"offset",
				[NSNumber numberWithDouble:(double)size], @"size",
				startTime, @"startTime", nil]];
		[pool release];
		}
	} while (offset < size && [[[self uiElements] isConnected] intValue]);
	if ([[[self uiElements] isConnected] intValue])
		data = [self sendCommand:fileEndCmd toDevice:dev expectResponse:YES careForReturn:YES];
	[fileHandle closeFile]; 
	[[self uiElements] goToPath:[[self uiElements]currentPath]];
	[[self uiElements] tableView:tableView didClickTableColumn:[[self uiElements]selectedColumn]];
	[[self uiElements] tableView:tableView didClickTableColumn:[[self uiElements]selectedColumn]]; //twice so get the same sort as before
	[self turnTurboOn:NO];
	[[self uiElements] finishTransfer];
}

- (void) deleteFile:(NSDictionary*)fileInfo fromPath:(NSString*)currentPath {
	[self checkUSB:myContext];
	NSString* fname = [dataFormat fnameForFile:[fileInfo objectForKey:@"name"]
		atPath:currentPath];
	NSData* fileDelCmd = [dataFormat prepareCmdHddDelWithFname:fname];
	[self sendCommand:fileDelCmd toDevice:myContext expectResponse:YES careForReturn:NO];
}

// ---------------------------------------------------------------------------
#pragma mark Queue management
// ---------------------------------------------------------------------------

- (void) addPriorityTransfer:(id)newTransfer {
	@synchronized (self) {
		[priorityTransferQueue addObject:newTransfer];
		NSLog(@"%i,p:%i-%@",[transferQueue count],[priorityTransferQueue count],[newTransfer objectForKey:@"transferType"]);
	}
}

- (void) addTransfer:(id)newTransfer atIndex:(int)front { //-1 for at end
	@synchronized (self) {
		if (front>=0) [transferQueue insertObject:newTransfer atIndex:front];
		else [transferQueue addObject:newTransfer];
		NSLog(@"%i,p:%i",[transferQueue count],[priorityTransferQueue count]);
	}
}

- (void) clearQueues {
	@synchronized (self) {
		[priorityTransferQueue removeAllObjects];
		[transferQueue removeAllObjects];
		[pausedQueue removeAllObjects];
	}
}

- (BOOL) hasCurrentTransfer {
	BOOL ret;
	@synchronized (self) {
		ret = ([transferQueue count] > 0);
	}
	return ret;
}

- (id) currentTransferInfo {
	id transfer = nil;
	@synchronized (self) {
		if ([transferQueue count] > 0) {
			transfer = [transferQueue objectAtIndex:0];
			// Make a copy of the object so that it can be safely manipulated
			// in a different thread from the original.
			transfer = [[transfer copy] autorelease];
		}
	}
	return transfer;
}

- (id) firstPausedTransferInfo {
	id transfer = nil;
	@synchronized (self) {
		if ([pausedQueue count] > 0) {
			transfer = [pausedQueue objectAtIndex:0];
			// Make a copy of the object so that it can be safely manipulated
			// in a different thread from the original.
			transfer = [[transfer copy] autorelease];
		}
	}
	return transfer;
}

- (void) transfer:(id)sender {
	while ([[[self uiElements] isConnected] intValue]) {
		NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
		NSDictionary* currentTransfer = nil;
		NSMutableArray* queue = nil;
		[self getNextTransfer:&currentTransfer queue:&queue];
		if (currentTransfer != nil) {
			NSString* transferType = [currentTransfer objectForKey:@"transferType"];
			if ([transferType isEqualToString:@"fileList"]) {
				[self getFileListForPath:[currentTransfer objectForKey:@"path"]];
			}
			else if ([transferType isEqualToString:@"turbo"]) {
		//		[self turnTurboOn:[[currentTransfer objectForKey:@"turboOn"] boolValue]];
			}
			else if ([transferType isEqualToString:@"rename"]) {
				[self renameFile:[currentTransfer objectForKey:@"oldName"]
					withName:[currentTransfer objectForKey:@"newName"]
					atPath:[currentTransfer objectForKey:@"path"]];
			}
			else if ([transferType isEqualToString:@"newFolder"]) {
				[self makeFolder:[currentTransfer objectForKey:@"newName"] atPath:[currentTransfer objectForKey:@"path"]];
			}
			else if ([transferType isEqualToString:@"delete"]) {
				[self deleteFile:[currentTransfer objectForKey:@"file"] fromPath:[currentTransfer objectForKey:@"path"]];
			}
			else if ([transferType isEqualToString:@"pause"]) {
				@synchronized (self) {
					if ([transferQueue count] != 0) {
						NSArray* toPause = [transferQueue filteredArrayUsingPredicate:
							[NSPredicate predicateWithFormat:@"(filename = %@)",
								[currentTransfer objectForKey:@"filename"]]];
						if ([toPause count] > 1) NSLog(@"multiple pauses?");
						else {
							[pausedQueue addObjectsFromArray:toPause];
							[transferQueue removeObjectsInArray:toPause];
						}
					}
				}
			}
			else if ([transferType isEqualToString:@"resume"]) {
				@synchronized (self) {
					if ([pausedQueue count] != 0) {
						NSArray* toResume = [pausedQueue filteredArrayUsingPredicate:
							[NSPredicate predicateWithFormat:@"(filename = %@)",
								[currentTransfer objectForKey:@"filename"]]];
						if ([toResume count] > 1) NSLog(@"multiple resumes?");
						else {
							[transferQueue addObjectsFromArray:toResume];
							[pausedQueue removeObjectsInArray:toResume];
						}
					}
				}
			}
			else if ([transferType isEqualToString:@"download"]) {
				[self getFile:[currentTransfer objectForKey:@"filename"]
					forPath:[currentTransfer objectForKey:@"path"]
					toSaveTo:[currentTransfer objectForKey:@"savePath"]
					beginAtOffset:[[currentTransfer objectForKey:@"offset"] unsignedLongLongValue]
					withLooping:[[currentTransfer objectForKey:@"looping"] boolValue]
					existingTime:[[currentTransfer objectForKey:@"existingTime"] intValue]];
			}
			else if ([transferType isEqualToString:@"upload"]) {
				[self uploadFile:[currentTransfer objectForKey:@"filename"]
					ofSize:[[currentTransfer objectForKey:@"fileSize"] unsignedLongLongValue]
					fromPath:[currentTransfer objectForKey:@"path"]
					withAttributes:[currentTransfer objectForKey:@"attributes"]
					atOffset:[[currentTransfer objectForKey:@"offset"] unsignedLongLongValue]
					existingTime:[[currentTransfer objectForKey:@"existingTime"] intValue]];
			}
			else {
				NSLog(@"Unrecognized transfer type: %@", transferType);
			}
			[self removeTransfer:currentTransfer fromQueue:queue];
		}
		else usleep(100);
		[pool release];
	}
}

// ---------------------------------------------------------------------------
#pragma mark User interface
// ---------------------------------------------------------------------------

- (void) setProgressBar:(NSProgressIndicator*)bar
	time:(NSTextField*)timeField
	turbo:(NSButton*)turbo
{
	progressBar = bar;
	progressTime = timeField;
	turboCB = turbo;
}

- (void) setDH:(id)newDH tableView:(id)tv {
	dh = newDH;
	tableView = tv;
}

// ---------------------------------------------------------------------------
#pragma mark Miscellaneous
// ---------------------------------------------------------------------------

- (id) init {
	if (self = [super init]) {
		debug = 0;
		rate = 0xfe00;
		transferQueue = [[NSMutableArray arrayWithCapacity:1] retain];
		pausedQueue = [[NSMutableArray arrayWithCapacity:1] retain];
		priorityTransferQueue = [[NSMutableArray arrayWithCapacity:1] retain];
		dataFormat = [TFDataFormat new];
	}
	return self;
}

- (void) dealloc {
	[dataFormat release];
	[super dealloc];
}

- (int) getSpeed {
	return connectedSpeed;
}

- (void) setDebug:(int)mode {
	debug = mode;
	NSLog(@"Debug level: %i", debug);
}

- (void) setRate:(int)newRate {
	rate = newRate;
	NSLog(@"New rate set: %i", rate);
}

@end

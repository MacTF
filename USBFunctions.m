//
//  USBFunctions.m
//  MacTF
//
//  Created by Nathan Oates on Sun Aug 01 2004.
//  Copyright (c) 2004 Nathan Oates nathan@noates.com All rights reserved.
//

// includes code based on uproar, license noted below:
//  uproar 0.1
//  Copyright (c) 2001 Kasima Tharnpipitchai <me@kasima.org>
#import "USBFunctions.h"

@implementation USBFunctions

/* 
 *
 * This source code is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Public License as published
 * by the Free Software Foundation; either version 2 of the License,
 * or (at your option) any later version.
 *
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * Please refer to the GNU Public License for more details.
 *
 * You should have received a copy of the GNU Public License along with
 * this source code; if not, write to:
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
 
 
 
/* Much of the code that initializes and closes the device is from Apple
 * and is released under the license below.
 */

/*
 * © Copyright 2001 Apple Computer, Inc. All rights reserved.
 *
 * IMPORTANT:  This Apple software is supplied to you by Apple Computer, Inc. (“Apple”) in 
 * consideration of your agreement to the following terms, and your use, installation, 
 * modification or redistribution of this Apple software constitutes acceptance of these
 * terms.  If you do not agree with these terms, please do not use, install, modify or 
 * redistribute this Apple software.
 *
 * In consideration of your agreement to abide by the following terms, and subject to these 
 * terms, Apple grants you a personal, non exclusive license, under Apple’s copyrights in this 
 * original Apple software (the “Apple Software”), to use, reproduce, modify and redistribute 
 * the Apple Software, with or without modifications, in source and/or binary forms; provided 
 * that if you redistribute the Apple Software in its entirety and without modifications, you 
 * must retain this notice and the following text and disclaimers in all such redistributions 
 * of the Apple Software.  Neither the name, trademarks, service marks or logos of Apple 
 * Computer, Inc. may be used to endorse or promote products derived from the Apple Software 
 * without specific prior written permission from Apple. Except as expressly stated in this 
 * notice, no other rights or licenses, express or implied, are granted by Apple herein, 
 * including but not limited to any patent rights that may be infringed by your derivative 
 * works or by other works in which the Apple Software may be incorporated.
 * 
 * The Apple Software is provided by Apple on an "AS IS" basis.  APPLE MAKES NO WARRANTIES, 
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-
 * INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE, REGARDING THE APPLE 
 * SOFTWARE OR ITS USE AND OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS. 
 *
 * IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS 
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, 
 * REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED AND 
 * WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR 
 * OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */         		


#define		kCmd			0
#define		kFin			1
#define		kDat			2

#define 	kTopfieldVendorID	4571
   //1317 for HC
   //0x138c for humax
   //4571 for toppy
#define 	kTF5kProdID 	4096
//42514 for HC
//0x02ad for humax
//4096 for toppy
-(id) init {
	id ret = [super init];
	debug = 0;
	rate = 0xfe00;
	return ret;
}


void hexDump(UInt8 *buf, int len) {
	int row, col, maxrows;
        
        maxrows = len/16;
        if (len % 16) maxrows++;
	for (row=0; row< maxrows; row++) {
		for (col=0; col<16; col++) {
			if (!(col%2)) printf(" ");
			printf("%02x", buf[row*16 + col] & 0xff);
		}
		printf("\t");	
		for (col=0; col<16; col++) {
			if ((buf[row*16 + col]>32) && (buf[row*16 + col]<126)) {
				printf("%c", buf[row*16 + col]);
			}
			else { printf("."); }
		}
		printf("\n");
	}
}



int doSend(IOUSBDeviceInterface197 **dev, IOUSBInterfaceInterface197 **intf,
		char *outBuf, UInt32 len, int type) 
{
	IOReturn 	err;
	UInt32		sendLen;
        
        
		if (debug == 1){
			printf(("sending:\n"));
            hexDump(outBuf, len);
		}
		sendLen = ((len/kBlockSize))*kBlockSize;
        if (len % kBlockSize)
            sendLen += kBlockSize;
        if ((sendLen % 0x200) == 0)  
        	sendLen += kBlockSize;
  //      if (type == kFin) //		err = beginInitiateRequest(dev); //  else  //    err = beginCmdSendRequest(dev);
	err = (*intf)->WritePipeTO(intf, 1, outBuf, sendLen, 1000, 20000);
	if (err) {
		printf("write err: %08x\n", err);
		return err;
	}
//	err = endRequest(dev);
	return err;
}


int doRecv(IOUSBDeviceInterface197 **dev, IOUSBInterfaceInterface197 **intf,
            UInt8 *inBuf, UInt32 dataLen, int type) {
        IOReturn	err;
        UInt32		len;
        
        if (dataLen > kMaxXferSize) return 1;
        
        len = (dataLen/kBlockSize) * kBlockSize;
        if (dataLen % kBlockSize)
	        len += kBlockSize;

		err = (*intf)->ReadPipeTO(intf, 2, (void *)inBuf, &len,  1000, 20000);
		if (err) {
                printf("read err 2: %08x\n", err);
				printf("resetting\n");
				err = (*intf)->ClearPipeStallBothEnds(intf, 2); 
		        return err;
        }

		
        if (debug == 1) {
          printf(("receiving: \n")); 
			hexDump(inBuf, len);
		}
        return err;
}





int dealWithInterface(io_service_t usbInterfaceRef, USBDeviceContext *device)
{
    IOReturn				err;
    IOCFPlugInInterface 		**iodev;		// requires <IOKit/IOCFPlugIn.h>
    IOUSBInterfaceInterface197 		**intf;
    IOUSBDeviceInterface197 		**dev;
    SInt32 				score;
	UInt8				numPipes, confNum, dSpeed;


    err = IOCreatePlugInInterfaceForService(usbInterfaceRef, kIOUSBInterfaceUserClientTypeID, kIOCFPlugInInterfaceID, &iodev, &score);
    if (err || !iodev)
    {
	printf("dealWithInterface: unable to create plugin. ret = %08x, iodev = %p\n", err, iodev);
	return kUproarDeviceErr;
    }
    err = (*iodev)->QueryInterface(iodev, CFUUIDGetUUIDBytes(kIOUSBInterfaceInterfaceID), (LPVOID)&(device->intf));
    (*iodev)->Release(iodev);				// done with this
    if (err || !intf)
    {
	printf("dealWithInterface: unable to create a device interface. ret = %08x, intf = %p\n", err, intf);
	return kUproarDeviceErr;
    }
    
    intf = device->intf;
    dev = device->dev;
    err = (*intf)->USBInterfaceOpen(intf);
    if (err)
    {
	printf("dealWithInterface: unable to open interface. ret = %08x\n", err);
	return kUproarDeviceErr;
    }
    err = (*intf)->GetNumEndpoints(intf, &numPipes);
    if (err)
    {
	printf("dealWithInterface: unable to get number of endpoints. ret = %08x\n", err);
	return kUproarDeviceErr;
    }
    
    printf("dealWithInterface: found %d pipes\n", numPipes);

    err = (*intf)->GetConfigurationValue(intf, &confNum);
    err = (*dev)->GetDeviceSpeed(dev, &dSpeed);
	if (dSpeed == 2) {
		kBlockSize = 0x40;
	} else {
		kBlockSize = 0x4;
	}
    printf("confnum: %08x, dspeed: %08x, blockS:%i\n", confNum, dSpeed, kBlockSize);
    connectedSpeed = dSpeed;
    return kUproarSuccess;
}


int dealWithDevice(io_service_t usbDeviceRef, USBDeviceContext *device)
{
    IOReturn				err;
    IOCFPlugInInterface 		**iodev;		// requires <IOKit/IOCFPlugIn.h>
    IOUSBDeviceInterface197		**dev;
    SInt32 				score;
    UInt8				numConf;
    IOUSBConfigurationDescriptorPtr	confDesc;
    IOUSBFindInterfaceRequest		interfaceRequest;
    io_iterator_t			iterator;
    io_service_t			usbInterfaceRef;
    int					found=0;
    
    
    err = IOCreatePlugInInterfaceForService(usbDeviceRef, kIOUSBDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &iodev, &score);
    if (err || !iodev)
    {
	printf("dealWithDevice: unable to create plugin. ret = %08x, iodev = %p\n", err, iodev);
	return kUproarDeviceErr;
    }
    err = (*iodev)->QueryInterface(iodev, CFUUIDGetUUIDBytes(kIOUSBDeviceInterfaceID), (LPVOID)&(device->dev));
    (*iodev)->Release(iodev);				
    
    dev = device->dev;
    if (err || !dev)
    {
	printf("dealWithDevice: unable to create a device interface. ret = %08x, dev = %p\n", err, dev);
	return kUproarDeviceErr;
    }
    err = (*dev)->USBDeviceOpen(dev);
    if (err)
    {
	printf("dealWithDevice: unable to open device. ret = %08x\n", err);
	return kUproarDeviceErr;
    }
    err = (*dev)->GetNumberOfConfigurations(dev, &numConf);
    if (err || !numConf)
    {
	printf("dealWithDevice: unable to obtain the number of configurations. ret = %08x\n", err);
	return kUproarDeviceErr;
    }
    printf("dealWithDevice: found %d configurations\n", numConf);
    err = (*dev)->GetConfigurationDescriptorPtr(dev, 0, &confDesc);	// get the first config desc (index 0)
    if (err)
    {
	printf("dealWithDevice:unable to get config descriptor for index 0\n");
	return kUproarDeviceErr;
    }
    err = (*dev)->SetConfiguration(dev, confDesc->bConfigurationValue);
    if (err)
    {
	printf("dealWithDevice: unable to set the configuration\n");
	return kUproarDeviceErr;
    }

    interfaceRequest.bInterfaceClass = kIOUSBFindInterfaceDontCare;		// requested class
    interfaceRequest.bInterfaceSubClass = kIOUSBFindInterfaceDontCare;		// requested subclass
    interfaceRequest.bInterfaceProtocol = kIOUSBFindInterfaceDontCare;		// requested protocol
    interfaceRequest.bAlternateSetting = kIOUSBFindInterfaceDontCare;		// requested alt setting
    
    err = (*dev)->CreateInterfaceIterator(dev, &interfaceRequest, &iterator);
    if (err)
    {
	printf("dealWithDevice: unable to create interface iterator\n");
	return kUproarDeviceErr;
    }
    
    while (usbInterfaceRef = IOIteratorNext(iterator))
    {
	printf("found interface: %p\n", (void*)usbInterfaceRef);
	err = dealWithInterface(usbInterfaceRef, device);
	IOObjectRelease(usbInterfaceRef);	// no longer need this reference
        found = 1;
    }
    
    IOObjectRelease(iterator);
    iterator = 0;
    if ((!found) || (err))
        return kUproarDeviceErr;
    else
        return kUproarSuccess;
}


int initDevice(USBDeviceContext *device){
    mach_port_t 	masterPort = 0;
    kern_return_t		err;
    CFMutableDictionaryRef 	matchingDictionary = 0;		// requires <IOKit/IOKitLib.h>
    short			idVendor = kTopfieldVendorID;
    short			idProduct = kTF5kProdID;
    CFNumberRef			numberRef;
    io_iterator_t 		iterator = 0;
    io_service_t		usbDeviceRef;
    int				found =0;
    
    err = IOMasterPort(bootstrap_port, &masterPort);			
    
    if (err)
    {
        printf("Anchortest: could not create master port, err = %08x\n", err);
        return err;
    }
    matchingDictionary = IOServiceMatching(kIOUSBDeviceClassName);	// requires <IOKit/usb/IOUSBLib.h>
    if (!matchingDictionary)
    {
        printf("Anchortest: could not create matching dictionary\n");
        return -1;
    }
    numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberShortType, &idVendor);
    if (!numberRef)
    {
        printf("Anchortest: could not create CFNumberRef for vendor\n");
        return -1;
    }
    CFDictionaryAddValue(matchingDictionary, CFSTR(kUSBVendorName), numberRef);
    CFRelease(numberRef);
    numberRef = 0;
    numberRef = CFNumberCreate(kCFAllocatorDefault, kCFNumberShortType, &idProduct);
    if (!numberRef)
    {
        printf("Anchortest: could not create CFNumberRef for product\n");
        return -1;
    }
    CFDictionaryAddValue(matchingDictionary, CFSTR(kUSBProductName), numberRef);
    CFRelease(numberRef);
    numberRef = 0;
    
    err = IOServiceGetMatchingServices(masterPort, matchingDictionary, &iterator);
    matchingDictionary = 0;			// this was consumed by the above call
    
    while (usbDeviceRef = IOIteratorNext(iterator))
    {
	printf("Found device %p\n", (void*)usbDeviceRef);
        err = dealWithDevice(usbDeviceRef, device);
	IOObjectRelease(usbDeviceRef);			// no longer need this reference
        found = 1;
    }
    
    
    IOObjectRelease(iterator);
    iterator = 0;
    mach_port_deallocate(mach_task_self(), masterPort);
    if ((!found) || (err))
        return kUproarDeviceErr;
    else
        return kUproarSuccess;
}


- (void) closeDevice:(USBDeviceContext *) device
{
    IOUSBInterfaceInterface197	**intf;
    IOUSBDeviceInterface197	**dev;
    IOReturn			err;
    
    intf = device->intf;
    dev = device->dev;
    
    err = (*intf)->USBInterfaceClose(intf);
    if (err)
    {
	printf("dealWithInterface: unable to close interface. ret = %08x\n", err);
    }
    err = (*intf)->Release(intf);
    if (err)
    {
	printf("dealWithInterface: unable to release interface. ret = %08x\n", err);
    }
    
    err = (*dev)->USBDeviceClose(dev);
    if (err)
    {
	printf("dealWithDevice: error closing device - %08x\n", err);
	(*dev)->Release(dev);
    }
    err = (*dev)->Release(dev);
    if (err)
    {
	printf("dealWithDevice: error releasing device - %08x\n", err);
    }
}

- (USBDeviceContext*) initializeUSB {
				USBDeviceContext	*device;
				int			err;
	kBlockSize = 0x08;
	device = malloc(sizeof(USBDeviceContext));
	err = initDevice(device);
	if (err) {
		printf("Could not connect to Topfield\n");
		return nil;
	}
	printf("\n\n");
	printf("Connected to Topfield\n\n");
	myContext = device;
	return device;
}

- (NSData*) getHDDSize:(USBDeviceContext*)device  {
	IOReturn	err;
	UInt8		outBuf[8], inBuf[64];
	UInt32		len;
	[self checkUSB:device];
	memset(outBuf, 0, 8);
	memset(inBuf, 0, 64);
	outBuf[0] = 0x08; //08000dc000000010 = request for HDD size info
	outBuf[1] = 0x00;
	outBuf[2] = 0x0d;
	outBuf[3] = 0xc0;
	outBuf[6] = 0x00;
	outBuf[7] = 0x10;
	
	err = doSend(device->dev, device->intf, outBuf, 8, kDat);
	len = 64;
	err = doRecv(device->dev, device->intf, inBuf, len, kDat);
			 
	NSData* data = [NSData dataWithBytes:inBuf length:16];
	data = [self swap:data];
	return data;
} 


- (NSData*) getFileList:(USBDeviceContext*)device forPath:(NSString*) path {	
	if (device == nil)
		return nil;
	
	// turn path into data
	NSMutableData *pathData = [NSMutableData dataWithData:[path dataUsingEncoding:NSISOLatin1StringEncoding]];
	unsigned int padding = 0x00;  // not sure if need to pad here?
	[pathData appendData:[NSData dataWithBytes:&padding length:1]];
	
	//prepackage any commands needed
	NSData* hddListCmd = [self prepareCommand:USB_CmdHddDir withData:pathData];
//	NSData* usbCancel = [self prepareCommand:USB_Cancel withData:nil];
	[self checkUSB:device];
	//send commands
	//reset first of all to make sure all is fine
/*	NSData* data = [self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:YES];
	unsigned int rW = 0x00000002;
	NSData* responseWanted = [NSData dataWithBytes:&rW length:4];
	while (data == nil || [data length] < 8) {
		NSLog (@"incorrect response");
		data = [self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:YES];
	}
	NSData* responseToCheck = [data subdataWithRange:(NSRange) {4,4}];
	if (![responseWanted isEqualToData:responseToCheck]) {
		[self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:NO]; // send reset again	
	}
*/	//now real one
	NSData* data = [self sendCommand:hddListCmd toDevice:device expectResponse:YES careForReturn:YES];	
	// deal with data recieved - in this case pass it back up for parsing
	return data;
}

-(word) findCRC:(byte*)p length:(dword) n
{
	word m_crc16 = 0x0000;
	const word crc16Tbl[256] = { 0x0000, 0xc0c1, 0xc181, 0x0140, 0xc301, 0x03c0, 0x0280, 0xc241, 0xc601, 0x06c0, 0x0780, 0xc741, 0x0500, 0xc5c1, 0xc481, 0x0440, 0xcc01, 0x0cc0, 0x0d80, 0xcd41, 0x0f00, 0xcfc1, 0xce81, 0x0e40, 0x0a00, 0xcac1, 0xcb81, 0x0b40, 0xc901, 0x09c0, 0x0880, 0xc841, 0xd801, 0x18c0, 0x1980, 0xd941, 0x1b00, 0xdbc1, 0xda81, 0x1a40, 0x1e00, 0xdec1, 0xdf81, 0x1f40, 0xdd01, 0x1dc0, 0x1c80, 0xdc41, 0x1400, 0xd4c1, 0xd581, 0x1540, 0xd701, 0x17c0, 0x1680, 0xd641, 0xd201, 0x12c0, 0x1380, 0xd341, 0x1100, 0xd1c1, 0xd081, 0x1040, 0xf001, 0x30c0, 0x3180, 0xf141, 0x3300, 0xf3c1, 0xf281, 0x3240, 0x3600, 0xf6c1, 0xf781, 0x3740, 0xf501, 0x35c0, 0x3480, 0xf441, 0x3c00, 0xfcc1, 0xfd81, 0x3d40, 0xff01, 0x3fc0, 0x3e80, 0xfe41, 0xfa01, 0x3ac0, 0x3b80, 0xfb41, 0x3900, 0xf9c1, 0xf881, 0x3840, 0x2800, 0xe8c1, 0xe981, 0x2940, 0xeb01, 0x2bc0, 0x2a80, 0xea41, 0xee01, 0x2ec0, 0x2f80, 0xef41, 0x2d00, 0xedc1, 0xec81, 0x2c40, 0xe401, 0x24c0, 0x2580, 0xe541, 0x2700, 0xe7c1, 0xe681, 0x2640, 0x2200, 0xe2c1, 0xe381, 0x2340, 0xe101, 0x21c0, 0x2080, 0xe041, 0xa001, 0x60c0, 0x6180, 0xa141, 0x6300, 0xa3c1, 0xa281, 0x6240, 0x6600, 0xa6c1, 0xa781, 0x6740, 0xa501, 0x65c0, 0x6480, 0xa441, 0x6c00, 0xacc1, 0xad81, 0x6d40, 0xaf01, 0x6fc0, 0x6e80, 0xae41, 0xaa01, 0x6ac0, 0x6b80, 0xab41, 0x6900, 0xa9c1, 0xa881, 0x6840, 0x7800, 0xb8c1, 0xb981, 0x7940, 0xbb01, 0x7bc0, 0x7a80, 0xba41, 0xbe01, 0x7ec0, 0x7f80, 0xbf41, 0x7d00, 0xbdc1, 0xbc81, 0x7c40, 0xb401, 0x74c0, 0x7580, 0xb541, 0x7700, 0xb7c1, 0xb681, 0x7640, 0x7200, 0xb2c1, 0xb381, 0x7340, 0xb101, 0x71c0, 0x7080, 0xb041, 0x5000, 0x90c1, 0x9181, 0x5140, 0x9301, 0x53c0, 0x5280, 0x9241, 0x9601, 0x56c0, 0x5780, 0x9741, 0x5500, 0x95c1, 0x9481, 0x5440, 0x9c01, 0x5cc0, 0x5d80, 0x9d41, 0x5f00, 0x9fc1, 0x9e81, 0x5e40, 0x5a00, 0x9ac1, 0x9b81, 0x5b40, 0x9901, 0x59c0, 0x5880, 0x9841, 0x8801, 0x48c0, 0x4980, 0x8941, 0x4b00, 0x8bc1, 0x8a81, 0x4a40, 0x4e00, 0x8ec1, 0x8f81, 0x4f40, 0x8d01, 0x4dc0, 0x4c80, 0x8c41, 0x4400, 0x84c1, 0x8581, 0x4540, 0x8701, 0x47c0, 0x4680, 0x8641, 0x8201, 0x42c0, 0x4380, 0x8341, 0x4100, 0x81c1, 0x8081, 0x4040, };
	unsigned long i;	
	for(i = 0; i < n; i++)
			m_crc16 = (m_crc16 >> 8) ^ crc16Tbl[*p++ ^ (m_crc16 & 0xFF)];
	return m_crc16;
}

- (NSData*) prepareCommand:(unsigned int)cmd withData:(NSData*) inData {	
		// work out length
		unsigned short int length = 0;
		if (inData == nil)
			length = 8;
		else {
			unsigned short int l = [inData length];
			length = l + 8;
		}
		NSMutableData* toSend = [NSMutableData dataWithBytes:&length length:2];
		NSMutableData* build = [NSMutableData dataWithBytes:&cmd length:4];
		if (inData != nil)
			[build appendData:inData];
		// work out crc
		byte test[length];
		memset(test, 0, length);
		[build getBytes:test length:[build length]];
		unsigned short int crc = [self findCRC:test length:[build length]];
		[toSend appendData:[NSMutableData dataWithBytes:&crc length:2]];
		[toSend appendData:build];
		// reverse and send
		toSend = [self swap:toSend];
		return toSend;
}

- (int) getFile:(NSDictionary*)fileInfo forPath:(NSString*) currentPath toSaveTo:(NSString*) savePath withLoop:(BOOL)looping {	
	//construct file send request
	NSMutableString* fname = [NSMutableString stringWithString:[fileInfo objectForKey:@"name"]];
	NSNumber* fileSize = [fileInfo objectForKey:@"fileSize"];
	char dir = USB_FileToHost;
	NSMutableData* build = [NSMutableData dataWithBytes:&dir length:1];
	short int nsize = [fname length]+[currentPath length]+2;// one for slash, one for padding 0x00
	[build appendData:[NSData dataWithBytes:&nsize length:2]];
	NSData* d = [currentPath dataUsingEncoding:NSISOLatin1StringEncoding];
	[build appendData:d];
	dir = 0x5c; // 0x5c = "/"
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[fname dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	
	//prepackage commands to send
	NSData* fileSendCmd = [self prepareCommand:USB_CmdHddFileSend withData:build];
	NSData* usbSuccess = [self prepareCommand:USB_Success withData:nil];
	NSData* usbCancel = [self prepareCommand:USB_Cancel withData:nil];
	
	// send commands
	[self checkUSB:myContext];
	[self sendCommand:fileSendCmd toDevice:myContext expectResponse:YES careForReturn:NO];
	//start timer
	NSDate* startTime = [NSDate date];
	// send start request and get response
	NSData *data = [self sendCommand:usbSuccess toDevice:myContext expectResponse:YES careForReturn:YES];
	unsigned int rW = 0x0000100a;
	NSData *responseWanted = [NSData dataWithBytes:&rW length:4];
	if ([data length] < 16) {
		NSLog(@"Incorrect Data length");
		return 1;
	}
	NSData *responseToCheck = [data subdataWithRange:(NSRange) {4,4}];
	if (![responseWanted isEqualToData:responseToCheck]) {
		NSLog(@"Unexpected response from Toppy during download");
		return 1;
	}
	
	// clean up data and prepare path to save it
	NSData* header = [data subdataWithRange:(NSRange) {4,4}]; //cmd data
	NSData* finalData = [data subdataWithRange:(NSRange) {16,[data length]-16}];
	
	// first initialize a file of the right name, as NSFileHandle requires an existing file to work on
	[[NSData dataWithBytes:&dir length:1] writeToFile:savePath atomically:NO]; // write 0x00 to initialize (overwritten later)
	NSFileHandle* outFile = [NSFileHandle fileHandleForWritingAtPath:savePath];
	[outFile writeData:finalData];
	if (looping) {	// loop for multiple data sends (ie files > 64k)
		int updateRate = 8; 
		if ([fileSize isGreaterThan:[NSNumber numberWithDouble:1048576]]) {
			updateRate = 24;
			NSLog(@"large file detected - low GUI update rate");
		}
		int timesAround = 0;
		int test = USB_DataHddFileData; 
		double amountReceived = 0xfe00;
		NSData* testData = [NSData dataWithBytes:&test length:4];
		while ([header isEqualToData:testData] && [[[NSApp delegate] isConnected] intValue]) {
			timesAround ++;
			NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];				
			data = [self sendCommand:usbSuccess toDevice:myContext expectResponse:YES careForReturn:YES];
			amountReceived += 0xfe00;
			if (timesAround < 8 || timesAround % updateRate == 0) {
					[NSThread detachNewThreadSelector:@selector(updateProgress:) toTarget:self withObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:(double)amountReceived], @"offset", fileSize, @"size", startTime, @"startTime", nil]];
			}
			if ([data length] > 16){ //there is something to read 
				header = [[data subdataWithRange:(NSRange) {4,4}] retain]; //cmd data
				NSData* finalData = [data subdataWithRange:(NSRange) {16,[data length]-16}];
				[outFile writeData:finalData];
				[outFile synchronizeFile];
			} 
			else break;
			[pool release];
		}
	}
	[self sendCommand:usbCancel toDevice:myContext expectResponse:YES careForReturn:NO]; // send reset again	just to make sure!
	[outFile closeFile];
    //now add the right modification date to the file
	[[NSFileManager defaultManager] changeFileAttributes:[NSDictionary dictionaryWithObject:[fileInfo objectForKey:@"date"] forKey:@"NSFileModificationDate"] atPath:savePath];
	return 0;
}

- (int) getFile:(NSDictionary*)fileInfo forPath:(NSString*) currentPath toSaveTo:(NSString*) savePath atOffset:(unsigned long long) offset withLoop:(BOOL)looping {	
	//construct file send request
	NSMutableString* fname = [NSMutableString stringWithString:[fileInfo objectForKey:@"name"]];
	NSNumber* fileSize = [fileInfo objectForKey:@"fileSize"];
	char dir = USB_FileToHost;
	NSMutableData* build = [NSMutableData dataWithBytes:&dir length:1];
	short int nsize = [fname length]+[currentPath length]+2;// one for slash, one for padding 0x00
	[build appendData:[NSData dataWithBytes:&nsize length:2]];
	NSData* d = [currentPath dataUsingEncoding:NSISOLatin1StringEncoding];
	[build appendData:d];	
	dir = 0x5c; // 0x5c = "/"
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[fname dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	//add 8 byte offset here
	NSLog(@"%qu", offset);
	[build appendData:[NSData dataWithBytes:&offset length:8]];
	NSLog([build description]);
	
	//prepackage commands to send
	NSData* fileSendCmd = [self prepareCommand:USB_CmdHddFileSend withData:build];
	NSData* usbSuccess = [self prepareCommand:USB_Success withData:nil];
	NSData* usbCancel = [self prepareCommand:USB_Cancel withData:nil];
	
	// send commands
	[self checkUSB:myContext];
	[self sendCommand:fileSendCmd toDevice:myContext expectResponse:YES careForReturn:NO];
	//start timer
	NSDate* startTime = [NSDate date];
	// send start request and get response
	NSData *data = [self sendCommand:usbSuccess toDevice:myContext expectResponse:YES careForReturn:YES];
	unsigned int rW = 0x0000100a;
	NSData *responseWanted = [NSData dataWithBytes:&rW length:4];
	if ([data length] < 16) {
		NSLog(@"Incorrect Data length");
		return 1;
	}
	NSData *responseToCheck = [data subdataWithRange:(NSRange) {4,4}];
	if (![responseWanted isEqualToData:responseToCheck]) {
		NSLog(@"Unexpected response from Toppy during download");
		return 1;
	}
	
	// clean up data and prepare path to save it
	NSData* header = [data subdataWithRange:(NSRange) {4,4}]; //cmd data
	NSData* finalData = [data subdataWithRange:(NSRange) {16,[data length]-16}];
	
	// first initialize a file of the right name, as NSFileHandle requires an existing file to work on
//	[[NSData dataWithBytes:&dir length:1] writeToFile:savePath atomically:NO]; // write 0x00 to initialize (overwritten later)
	NSFileHandle* outFile = [NSFileHandle fileHandleForWritingAtPath:savePath];
	[outFile seekToFileOffset:offset];
	[outFile writeData:finalData];
	if (looping) {	// loop for multiple data sends (ie files > 64k)
		int updateRate = 8; 
		if ([fileSize isGreaterThan:[NSNumber numberWithDouble:1048576]]) {
			updateRate = 24;
			NSLog(@"large file detected - low GUI update rate");
		}
		int timesAround = 0;
		int test = USB_DataHddFileData; 
		double amountReceived = 0xfe00;
		NSData* testData = [NSData dataWithBytes:&test length:4];
		while ([header isEqualToData:testData] && [[[NSApp delegate] isConnected] intValue]) {
			timesAround ++;
			NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];				
			data = [self sendCommand:usbSuccess toDevice:myContext expectResponse:YES careForReturn:YES];
			amountReceived += 0xfe00;
			if (timesAround < 8 || timesAround % updateRate == 0) {
					[NSThread detachNewThreadSelector:@selector(updateProgress:) toTarget:self withObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:(double)amountReceived], @"offset", fileSize, @"size", startTime, @"startTime", nil]];
			}
			if ([data length] > 16){ //there is something to read 
				header = [[data subdataWithRange:(NSRange) {4,4}] retain]; //cmd data
				NSData* finalData = [data subdataWithRange:(NSRange) {16,[data length]-16}];
				[outFile writeData:finalData];
				[outFile synchronizeFile];
			} 
			else break;
			[pool release];
		}
	}
	[self sendCommand:usbCancel toDevice:myContext expectResponse:YES careForReturn:NO]; // send reset again	just to make sure!
	[outFile closeFile];
    //now add the right modification date to the file
	[[NSFileManager defaultManager] changeFileAttributes:[NSDictionary dictionaryWithObject:[fileInfo objectForKey:@"date"] forKey:@"NSFileModificationDate"] atPath:savePath];
	return 0;
}

- (NSData*) sendCommand:(NSData*) fullyPackagedCommand toDevice:(USBDeviceContext*)device expectResponse:(BOOL) getResponse careForReturn:(BOOL) careFactor{
	IOReturn	err;
	int cmdLength = [fullyPackagedCommand length];
	unsigned char outBuffer[cmdLength];
	memset(outBuffer, 0, cmdLength);
//	NSLog(@"send: %@", [fullyPackagedCommand description]);
	[fullyPackagedCommand getBytes:outBuffer];
	
	err = doSend(device->dev, device->intf, outBuffer, cmdLength, kDat);
	if (err)
		NSLog(@"sendError: %08x\n");
	
	if (! getResponse) return nil;
	
	int inLen = 0xFFFF; // i think this is biggest needed?
	unsigned char inBuf[inLen];
	memset(inBuf, 0, inLen);
	err = doRecv(device->dev, device->intf, inBuf, inLen, kDat);
	if (err)
		NSLog(@"inError: %08x\n", err);
	
	if (! careFactor) return nil;
	NSMutableData* data = [NSMutableData dataWithBytes:inBuf length:inLen];
	data = [self swap:data];
	inLen = inBuf[1]*256 + inBuf[0]; // work out how long the response really is. NB data is flipped, but inBuf still isn't
	return [data subdataWithRange:(NSRange) {0,inLen}];
}

-(NSMutableData*) swap: (NSData*) inData {
//	NSLog(@"Entering swap");
//	if ([inData length] > 500)
//		NSLog(@"cut:%@",[[inData subdataWithRange:(NSRange){0,500}] description]);
//	else 
//		NSLog([inData description]);
	int len = [inData length];
	int uneven = 0;
	if (len % 2 != 0) {
		uneven = 1; // prepare space for padding
	}
//	NSLog(@"Uneven: %i", uneven);
//	char array[len + uneven];
//	char outarray[len + uneven];
//	memset(array, 0 , len+uneven);
//	memset(outarray, 0 , len+uneven);
	char *array;
	char *outarray;
	if ((array = calloc((len + uneven), sizeof(char))) == NULL) {
		NSLog(@"ERROR: Malloc failed");
		return [NSMutableData dataWithData:inData];
	}
	if ((outarray = calloc((len + uneven), sizeof(char))) == NULL) {
		NSLog(@"ERROR: Malloc failed");
		return [NSMutableData dataWithData:inData];
	}
//	NSLog(@"memset complete");
	[inData getBytes:array length:len];
	swab(array, outarray, len+uneven);
//	NSLog(@"swab over");
	NSMutableData* ret = [NSMutableData dataWithCapacity:len+uneven];
	[ret setData:[NSData dataWithBytes:outarray length:len+uneven]];
//	NSLog(@"outdata prepared");
	free(array);
	free(outarray);
	return ret; 
}

- (void) uploadFile:(NSString*) fileToUpload ofSize:(long long) size fromPath:(NSString*)curPath withAttributes:(NSData*) typeFile toDevice:(USBDeviceContext*)dev {
	// prepare Send command
	NSMutableArray* array = [NSMutableArray arrayWithArray:[fileToUpload componentsSeparatedByString:@"/"]];
	NSMutableString* fname = [NSMutableString stringWithString:[array lastObject]];
	char dir = USB_FileToDevice;
	NSMutableData* build = [NSMutableData dataWithBytes:&dir length:1];
	short int nsize = [fname length]+[curPath length]+2;// one for slash, one for padding 0x00
	[build appendData:[NSData dataWithBytes:&nsize length:2]];
	NSData* d = [curPath dataUsingEncoding:NSISOLatin1StringEncoding];
	[build appendData:d];
	dir = 0x5c; // 0x5c = "/"
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[fname dataUsingEncoding:NSISOLatin1StringEncoding]];// may need to pad to 95...
		int f = 0x0000000000;
		int i = 0;
		while (i < 95 -[fname length]){
			[build appendData:[NSData dataWithBytes:&f length:1]];
			i++;
		}
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	
	// prepackage commands to send
	NSData* fileSendCmd = [self prepareCommand:USB_CmdHddFileSend withData:build];
//	NSData* usbSuccess = [self prepareCommand:USB_Success withData:nil];
//	NSData* usbCancel = [self prepareCommand:USB_Cancel withData:nil];
	NSData* fileStartCmd = [self prepareCommand:USB_DataHddFileStart withData:typeFile];
	NSData* fileEndCmd = [self prepareCommand:USB_DataHddFileEnd withData:nil];

	//start timer
	NSDate* startTime = [NSDate date];
	
	//send commands
	[self checkUSB:dev];
	// now the proper commands
	NSData *data = [self sendCommand:fileSendCmd toDevice:dev expectResponse:YES careForReturn:YES];
	data = [self sendCommand:fileStartCmd toDevice:dev expectResponse:YES careForReturn:YES];
	unsigned int rW = 0x00000002;
	NSData* responseWanted = [NSData dataWithBytes:&rW length:4];
	NSData* responseToCheck = nil;

	unsigned long long offset = 0x0000000000000000;
	NSFileHandle* fileHandle = [NSFileHandle fileHandleForReadingAtPath:fileToUpload];
	do {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		NSMutableData* fileData = [NSMutableData dataWithBytes:&offset length:8];
		[fileData appendData:[fileHandle readDataOfLength:rate]];
		offset += rate;
		NSData* fileDataCmd = [self prepareCommand:USB_DataHddFileData withData:fileData];
		data = [self sendCommand:fileDataCmd toDevice:dev expectResponse:YES careForReturn:YES];
		if ([data length] >= 8) 
			responseToCheck = [data subdataWithRange:(NSRange) {4,4}];
		else responseToCheck = nil;
		if (responseToCheck == nil || ![responseWanted isEqualToData:responseToCheck]) 
			break;
		[NSThread detachNewThreadSelector:@selector(updateProgress:) toTarget:self withObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithDouble:(double)offset], @"offset", [NSNumber numberWithDouble:(double)size], @"size", startTime, @"startTime", nil]];
		[pool release];
	} while (offset < size && [[[NSApp delegate] isConnected] intValue]);
	if ([[[NSApp delegate] isConnected] intValue])
		data = [self sendCommand:fileEndCmd toDevice:dev expectResponse:YES careForReturn:YES];
	[fileHandle closeFile]; 	
}


- (void) turnTurboOn:(BOOL) turnOn forDevice:(USBDeviceContext*)device withReset:(BOOL)reset {
	if (![[[NSApp delegate] isConnected] intValue]) return;
	if (reset) [self checkUSB:device];
	int mode = 0;
	if (turnOn) 
		mode = 1;
	NSData* modeData = [NSData dataWithBytes:&mode length:4];
	NSLog([modeData description]); //debugging
	NSData* turboCommand  = [self prepareCommand:USB_CmdTurbo withData:modeData];
	[self sendCommand:turboCommand toDevice:device expectResponse:YES careForReturn:NO];
}

- (int) getSpeed {
	return connectedSpeed;
}

-(void) setDebug:(int)mode {
	debug = mode;
	NSLog(@"Debug level: %i", debug);
}

-(void) setRate:(int) newRate {
	rate = newRate;
	NSLog(@"New rate set: %i", rate);
}

-(void) setProgressBar:(NSProgressIndicator*)bar time:(NSTextField*)timeField {
	progressBar = bar;
	progressTime = timeField;
}

-(void) updateProgress:(NSDictionary*) inDict {
	NSAutoreleasePool *pool=[[NSAutoreleasePool alloc] init];
	double offset = [[inDict objectForKey:@"offset"] doubleValue];
	double size = [[inDict objectForKey:@"size"] doubleValue];
	NSDate* startTime = [inDict objectForKey:@"startTime"]; 	
	[progressBar setDoubleValue:((double)offset/size*100)];
	[progressBar displayIfNeeded];
	[progressTime setStringValue:[self elapsedTime:[[NSDate date] timeIntervalSinceDate:startTime]]];
	[progressTime displayIfNeeded];
	[pool release];
}


- (NSString*) elapsedTime:(NSTimeInterval) totalSeconds {
	char hp = 20;
	char mp = 20;
	char sp = 20; //padding
	int hours = (totalSeconds / 3600);  // returns number of whole hours fitted in totalSecs
	if (hours < 10)
		hp = 48;
	int minutes = ((totalSeconds / 60) - hours*60);  // Whole minutes
	if (minutes < 10)
		mp = 48;
	int seconds = ((long) totalSeconds % 60);	// Here we can use modulo to get num secs NOT fitting in whole minutes (60 secs)
	if (seconds < 10)
		sp = 48;
	return [NSString stringWithFormat:@"%c%i:%c%i:%c%i", hp, hours, mp, minutes, sp, seconds];
}

- (void) deleteFile:(NSDictionary*)fileInfo fromPath:(NSString*)currentPath onDevice:(USBDeviceContext*)device {
	[self checkUSB:device];
	NSMutableString* fname = [NSMutableString stringWithString:[fileInfo objectForKey:@"name"]];
	NSMutableData* build = [NSMutableData dataWithData:[currentPath dataUsingEncoding:NSISOLatin1StringEncoding]];
	char dir = 0x5c; // 0x5c = "\"
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[fname dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	NSData* fileDelCmd = [self prepareCommand:USB_CmdHddDel withData:build];
	[self sendCommand:fileDelCmd toDevice:device expectResponse:YES careForReturn:NO];
}

- (void) renameFile:(NSString*) oldName withName:(NSString*)newName atPath:(NSString*)currentPath onDevice:(USBDeviceContext*)device {
	[self checkUSB:device];
	unsigned short int i = [oldName length]+[currentPath length]+2;
	NSMutableData* build = [NSMutableData dataWithBytes:&i length:2];
	[build appendData:[currentPath dataUsingEncoding:NSISOLatin1StringEncoding]];
	char dir = 0x5c;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[oldName dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	i = [newName length]+[currentPath length]+2;
	[build appendData:[NSData dataWithBytes:&i length:2]];
	[build appendData:[currentPath dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x5c;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[newName dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	NSData* fileRenCmd = [self prepareCommand:USB_CmdHddRename withData:build];
	[self sendCommand:fileRenCmd toDevice:device expectResponse:YES careForReturn:NO];
}

- (void) makeFolder:(NSString*)newName atPath:(NSString*)currentPath onDevice:(USBDeviceContext*)device {
	[self checkUSB:device];
	unsigned short int i = [newName length]+[currentPath length]+2;
	NSMutableData* build = [NSMutableData dataWithBytes:&i length:2];
	[build appendData:[currentPath dataUsingEncoding:NSISOLatin1StringEncoding]];
	char dir = 0x5c;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	[build appendData:[newName dataUsingEncoding:NSISOLatin1StringEncoding]];
	dir = 0x00;
	[build appendData:[NSData dataWithBytes:&dir length:1]];
	NSData* newFoldCmd = [self prepareCommand:USB_CmdHddCreateDir withData:build];
	NSData* usbCancel = [self prepareCommand:USB_Cancel withData:nil];
	[self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:NO];
	[self sendCommand:newFoldCmd toDevice:device expectResponse:YES careForReturn:NO];
}

- (void) checkUSB:(USBDeviceContext*)device {
	NSData* usbCancel = [self prepareCommand:USB_Cancel withData:nil];
	NSData* data = [self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:YES];
	unsigned int rW = 0x00000002;
	int i = 0;
	NSData* responseWanted = [NSData dataWithBytes:&rW length:4];
	while ((data == nil || [data length] < 8) && i < 8) {
		NSLog (@"incorrect response");
		i++;
		data = [self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:YES];
	}
	if (!i<=8) {
		// tell someone that no longer connected here??
		return;
		}
	NSData* responseToCheck = [data subdataWithRange:(NSRange) {4,4}];
	if (![responseWanted isEqualToData:responseToCheck]) {
		[self sendCommand:usbCancel toDevice:device expectResponse:YES careForReturn:NO]; // send reset again	
	}
}

@end

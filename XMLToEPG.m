//
//  XMLToEPG.m
//  MacTF
//
//  Created by Nathan Oates on Thu Dec 16 2004.
//  Copyright (c) 2004 Nathan Oates. All rights reserved.
//

#import "XMLToEPG.h"


@implementation XMLToEPG

- (NSMutableArray*) importXML: (NSURL*) xmlFile {
	NSData *xmlData = [NSData dataWithContentsOfURL:xmlFile];
	XMLTree *tree = [[XMLTree alloc] initWithData:xmlData];
	NSMutableArray *importedSeqs = [NSMutableArray arrayWithCapacity:1];
	if (!tree) {
		NSLog(@"tree is null");
	} else {
		XMLTree *tvTree = [tree descendentNamed:@"tv"];
		int numOfChildren = [tvTree count];
		int i = 0;
		while (i < numOfChildren) {
			XMLTree *currSeq = [tvTree childAtIndex:i];
			if ([[currSeq name] isEqualToString:@"channel"]) {
				//handle channel
			} else { // it's a programme
				NSString *title = [[currSeq descendentNamed:@"title"] description];
				NSString *rating = [[currSeq descendentNamed:@"rating"] description];
				NSString *cat = [[currSeq descendentNamed:@"category"] description];
				NSString *desc = [[currSeq descendentNamed:@"desc"] description];
				NSString *rawChannel = [currSeq attributeNamed:@"channel"];
				NSString *rawStart = [currSeq attributeNamed:@"start"];
				NSString *rawStop = [currSeq attributeNamed:@"stop"];
				NSString *subt = [[currSeq descendentNamed:@"sub-title"] description];
				NSMutableString *sub;
				if (cat == nil) cat = @"";
				if (subt == nil)
					sub = [NSMutableString stringWithString:@" ["];
				else {
					sub = [NSMutableString stringWithString:subt];
					[sub appendString:@" ["];
				}
				[sub appendString:cat];
				[sub appendString:@"]"];
				if (desc == nil) desc = @"";
				if (title == nil) title = @"";
				if (rating == nil) rating = @"";
				NSString *record = @"N";
				NSMutableDictionary *total =  [NSMutableDictionary dictionaryWithObjectsAndKeys:
					title, @"title",
					rating, @"rating",
					cat, @"category",
					sub, @"subtitle",
					desc, @"description",
					record, @"record",
					rawChannel, @"rawChannel",
					rawStart, @"rawStart",
					rawStop, @"rawStop", nil];
				[importedSeqs addObject:total];
			}
				i++;			
		}
	}
	return importedSeqs;
}

- (void) fixData: (NSMutableArray*) rawData {
	NSEnumerator* e = [rawData objectEnumerator];
	id currentSelected;
	while (currentSelected = [e nextObject]) {
		NSString* dateString = [currentSelected objectForKey:@"rawStart"];
		NSCalendarDate *startDate = [self stringToDate:dateString];
		[currentSelected setObject:startDate forKey:@"startDate"];
		dateString = [currentSelected objectForKey:@"rawStop"];
		NSCalendarDate *stopDate = [self stringToDate:dateString];
		[currentSelected setObject:stopDate forKey:@"stopDate"];
		int duration;
		[stopDate years:NULL months:NULL days:NULL hours:NULL minutes:&duration seconds:NULL sinceDate:startDate];
		[currentSelected setObject:[NSNumber numberWithInt:duration] forKey:@"duration"];
		// now fix channel
		NSString *rawChannel = [currentSelected objectForKey:@"rawChannel"];
		NSString *chanName = @"";
		NSArray *tempName  = [[rawChannel componentsSeparatedByString:@"."] subarrayWithRange:(NSRange) {1,2}];
		if ([[tempName objectAtIndex:1] isEqualToString:@"2"]) chanName = @"ABC";
		else if ([[tempName objectAtIndex:1] isEqualToString:@"7"]) chanName = @"Seven";
		else if ([[tempName objectAtIndex:1] isEqualToString:@"9"]) chanName = @"Nine";
		else if ([[tempName objectAtIndex:1] isEqualToString:@"10"]) chanName = @"Ten";
		else if ([[tempName objectAtIndex:1] isEqualToString:@"SBS"]) chanName = @"SBS";
		[currentSelected setObject:chanName forKey:@"chanName"];
		NSString *locationChannel = [tempName componentsJoinedByString:@"."];
		[currentSelected setObject:locationChannel forKey:@"locChan"];
	//	if ([rawChannel ]) serviceID = 545;
	//	else if ([rawChannel isEqualTo:@"freesd.Sydney.7.d1.com.au"]) serviceID = 1313;
	//	else if ([rawChannel isEqualTo:@"freesd.Sydney.10.d1.com.au"]) serviceID = 1569;
	//	else if ([rawChannel isEqualTo:@"freesd.Sydney.9.d1.com.au"]) serviceID = 1;
	//	else if ([rawChannel isEqualTo:@"freesd.Sydney.SBS.d1.com.au"]) serviceID = 769;
	//	[currentSelected setObject:[NSNumber numberWithInt:serviceID] forKey:@"serviceID"];
	
	//	NSLog([currentSelected description]);
	}
}

- (void) exportFixedXML:(NSMutableArray*) fixedData toFile:(NSString*) path {
	char dir = 0x00;
	[[NSData dataWithBytes:&dir length:1] writeToFile:path atomically:NO]; // write 0x00 to initialize (overwritten later)
	NSFileHandle* outFile = [NSFileHandle fileHandleForWritingAtPath:path];
	NSEnumerator* e = [fixedData objectEnumerator];
	id currentSelected;
	while (currentSelected = [e nextObject]) { //construct a line with the right info
		NSMutableString *buildString = [NSMutableString stringWithString:[currentSelected objectForKey:@"chanName"]];
		[buildString appendString:@"\t"];
		NSCalendarDate *startDate = [currentSelected objectForKey:@"startDate"];
		[buildString appendString:[startDate descriptionWithCalendarFormat:@"%Y-%m-%d %H:%M" timeZone:[NSTimeZone timeZoneWithName:@"GMT"] locale:nil]];
		[buildString appendString:@"\t"];
		[buildString appendString:[[currentSelected objectForKey:@"duration"] description]];
		[buildString appendString:@"\t"];
		[buildString appendString:[currentSelected objectForKey:@"title"]];
		[buildString appendString:@"\t"];
		[buildString appendString:[currentSelected objectForKey:@"subtitle"]];
		[buildString appendString:@"\t"];
		[buildString appendString:[currentSelected objectForKey:@"description"]];
		[buildString appendString:@"\t"];
		[buildString appendString:[currentSelected objectForKey:@"rating"]];
		[buildString appendString:@"\t"];
		[buildString appendString:[currentSelected objectForKey:@"record"]];
		[buildString appendString:@"\n"];
		[outFile writeData:[buildString dataUsingEncoding:NSASCIIStringEncoding]];
	}
	[outFile closeFile];
}



- (NSCalendarDate*) stringToDate:(NSString*) inString {
	int year = [[inString substringWithRange:(NSRange) {0,4}] intValue];
	int month = [[inString substringWithRange:(NSRange) {4,2}] intValue];
	int day = [[inString substringWithRange:(NSRange) {6,2}] intValue];
	int hour = [[inString substringWithRange:(NSRange) {8,2}] intValue];
	int min = [[inString substringWithRange:(NSRange) {10,2}] intValue];
	int tzsecs = [[inString substringWithRange:(NSRange) {16,2}] intValue] * 60 * 60 + [[inString substringWithRange:(NSRange) {18,2}] intValue] * 60;
	return [NSCalendarDate dateWithYear:year month:month day:day hour:hour minute:min second:0 timeZone:[NSTimeZone timeZoneForSecondsFromGMT:tzsecs]];
}

@end

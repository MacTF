//
//  Scripting_Additions.m
//  MacTF
//
//  Created by Nathan Oates on Fri Dec 31 2004.
//  Copyright (c) 2004 __MyCompanyName__. All rights reserved.
//

#import "Scripting_Additions.h"
#import "UIElements.h"


@implementation NSApplication(Scripting_Additions) 

// Applescript commands
- (id) handleUploadFileCommand:(NSScriptCommand *)command {
	
	// ask the command to evaluate its arguments
	NSDictionary *args = [command evaluatedArguments];
	NSString *file = [args objectForKey:@"FileName"];
	NSString *dest = [args objectForKey:@"Destination"];
	
	// if we don't have a file argument, we're doomed!
	if (!file || [file isEqualToString:@""]) return [NSNumber numberWithBool:NO];
	if (!dest || [dest isEqualToString:@""]) return [NSNumber numberWithBool:NO];
	NSLog(@"Applescript upload: %@ to %@", file, dest);
	[[self delegate]uploadPath:file toPath:dest];
	return [NSNumber numberWithBool:YES];
}

- (id)handleDownloadFileCommand:(NSScriptCommand *)command {
	// ask the command to evaluate its arguments
	NSDictionary *args = [command evaluatedArguments];
	NSString *file = [args objectForKey:@"FileName"];
	NSString *dest = [args objectForKey:@"Destination"];
	
	// if we don't have a file argument, we're doomed!
	if (!file || [file isEqualToString:@""]) return [NSNumber numberWithBool:NO];
	if (!dest || [dest isEqualToString:@""]) return [NSNumber numberWithBool:NO];
	
	UIElements *UI = [self delegate];
	if (![UI isConnected])
		return [NSNumber numberWithBool:NO];
	else {
		NSLog(@"Applescript download: %@ to %@", file, dest);
		NSDictionary* fileInfo = [NSDictionary dictionaryWithObjectsAndKeys:[file lastPathComponent], @"name", [NSNumber numberWithInt:0], @"fileSize", [NSCalendarDate calendarDate], @"date", nil];
		[[UI getTfusbc] getFile:fileInfo forPath:[file stringByDeletingLastPathComponent] toSaveTo:dest beginAtOffset:0 withLooping:YES existingTime:0];
		return [NSNumber numberWithBool:YES];
	}
}

- (id)handleDeleteFileCommand:(NSScriptCommand *)command {
	// ask the command to evaluate its arguments
	NSDictionary *args = [command evaluatedArguments];
	NSString *file = [args objectForKey:@"FileName"];
	
	// if we don't have a file argument, we're doomed!
	if (!file || [file isEqualToString:@""]) return [NSNumber numberWithBool:NO];
	
	UIElements *UI = [self delegate];
	if (![UI isConnected])
		return [NSNumber numberWithBool:NO];
	else {
		NSLog(@"Applescript delete: %@", file);
		NSDictionary* fileInfo = [NSDictionary dictionaryWithObjectsAndKeys:[file lastPathComponent], @"name", [NSNumber numberWithInt:0], @"fileSize", [NSCalendarDate calendarDate], @"date", nil];
	//	NSLog([fileInfo description]);
		[[UI getTfusbc] deleteFile:fileInfo fromPath:[file stringByDeletingLastPathComponent]];
		return [NSNumber numberWithBool:YES];
	}
}

- (id)handleConnectCommand:(NSScriptCommand *)command {
	NSLog(@"Applescript app connect");
	UIElements *uiEl = [self delegate];
	[uiEl connect:nil];
	return [uiEl isConnected];
}

- (id)handleDisconnectCommand:(NSScriptCommand *)command {
	NSLog(@"Applescript disconnect");
	UIElements *uiEl = [self delegate];
	[uiEl connect:nil];
	if ([uiEl isConnected])
		return [NSNumber numberWithBool:NO];
	else
		return [NSNumber numberWithBool:YES];
}

@end
